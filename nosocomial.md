# 20211124, https://www.bbc.com/news/uk-wales-59402295

Covid in Wales: 2,000 patients die after catching virus in hospital

More than 2,000 people in Wales, who probably caught Covid in hospital, went on to die within 28 days, data shows.

There have been at least 8,243 cases of patients probably or definitely catching Covid in Welsh hospitals since the start of the pandemic.

About 27% (2,216) died within 28 days, though not necessarily of Covid, having been admitted for other reasons.

PHW data shows that during the first wave, 33% of patients who probably or definitely caught Covid in hospital died within 28 days of a positive test.

That figure fell to 27% in the second wave and currently stands at 14% in the third wave.

The data however does not set out how many of the deaths were primarily caused by the virus, in how many Covid was an aggravating factor, or how many deaths were caused by other factors.

People are defined as having "probably" caught Covid in hospital if they test positive between day eight and 14 of their hospital stay, and "definitely" if the positive test comes after that.

More than 1,000 patients have probably or definitely caught the virus in hospital in Wales since the beginning of August.



# 20200518, https://www.theguardian.com/world/2020/may/18/agency-staff-were-spreading-covid-19-between-care-homes-phe-found-in-april

Agency staff were spreading Covid-19 between care homes, PHE found in April
This article is more than 1 year

Exclusive: results of unpublished study were only shared with care home providers last week

Temporary care workers transmitted Covid-19 between care homes as cases surged, according to an unpublished government study which used genome tracking to investigate outbreaks.

In evidence that raises further questions about ministers’ claims to have “thrown a protective ring around care homes”, it emerged that agency workers – often employed on zero-hours contracts – unwittingly spread the infection as the pandemic grew, according to the study by Public Health England (PHE).

The genome tracking research into the behaviour of the virus in six care homes in London found that, in some cases, workers who transmitted coronavirus had been drafted in to cover for care home staff who were self-isolating expressly to prevent the vulnerable people they look after from becoming infected.

...

During flu pandemic planning in 2018, a report from social care directors warned ministers that frontline care workers would need advice on “controlling cross-infection”. A 2019 PHE document about flu pandemic preparations called “Infection prevention and control: an outbreak information pack for care homes” urged operators to “try to avoid moving staff between homes and floors”.

But the DHSC’s social care plan, published on 16 April, mentions nothing about restricting staff movements between homes in its chapter on “controlling the spread of infection in care homes”.

...

Care operators have been increasingly reliant on flexible workers to fill shifts, with absence rates caused by self-isolation among permanent staff running as high as 25% at the peak of outbreaks.

Until recent weeks, shortages of PPE exacerbated concerns that care staff risked spreading the illness and family members have complained of loved ones put at risk by care workers travelling to and from work without changing clothes.

...

“The challenge here is they are looking for restrictions on movement when we are already running with a high level of vacancies in the sector, people self-isolating with symptoms and now the roll out of testing of asymptomatic staff which will increase the numbers self-isolating again.”


# 20210104, https://nymag.com/intelligencer/article/coronavirus-lab-escape-theory.html

long and substantial read on lableak theory ,
By Nicholson Baker


# 20201113, https://www.msf.org/msf-supports-coronavirus-covid-19-response-czech-republic

In its first ever operations in the Czech Republic, M�decins Sans Fronti�res (MSF) is supporting the COVID-19 response in nursing homes around the country, in cooperation with the Ministry of Labour and Social Affairs. Since early November, MSF teams have been providing training in infection prevention and control measures to nursing home staff, who are on the frontline of the COVID-19 response.

�Our teams were looking for a way to support the most vulnerable communities in response to the deteriorating pandemic situation in the Czech Republic,� says MSF project coordinator Pavel Gruber. �We discovered that providing support with infection prevention and control in nursing homes was the most efficient course of action.�

MSF staff providing nursing home staff with support

In early November, the Czech Republic became one of the three countries globally with the highest number of COVID-19 cases per million people. The disease is spreading particularly fast among elderly people. Nursing home staff, who do not necessarily have medical training, often find themselves in the position of having to put in place infection prevention and control measures with little to no guidance or previous experience.

MSF has two mobile teams, each consisting of a nurse and a logistician. They are visiting nursing homes around the country to give feedback and advice, based on MSF�s expertise in dealing with infectious diseases around the world, as well as on our experience supporting homes during COVID-19 peaks in Spain, Belgium and elsewhere earlier this year.

�For the nursing home staff, this is often their first opportunity to discuss the measures they are introducing,� says Pavel Dacko, an MSF logistician with experience working in Kenya, Democratic Republic of Congo, Niger and Chad. �They can speak with someone who has an external view. It�s also an opportunity to be appraised.�

�We walk through the care home and plan with the staff how to introduce �clean� and �dirty� zones, whether they have isolation rooms, where staff will put on their protective gear and where they will dispose of it afterwards,� says Tereza Pokorn�, an MSF nurse on one of the teams. �We need to see all of this physically because that is how the best ideas arise.�

Onsite support, online resources and psychological support

In the first two weeks of MSF�s response, the two mobile teams visited 23 care homes, mainly in the Pilsner and South Moravian regions. Now, they are expanding their support to other regions and plan to visit a further 12 care homes in the coming week.

As well as on-site support through our mobile teams, we have also set up a website providing resources for medical and non-medical professionals responding to COVID-19. In partnership with other organisations, MSF is also promoting psychological support for first-line responders.

While MSF�s operations in the Czech Republic are currently small and focused on specific activities, we are closely monitoring the situation of those people most vulnerable to COVID-19 and our teams are ready to respond when needed.



# 20210304, https://www.derstandard.at/story/2000124595249/medizinanthropologin-kann-auf-die-schnelel

STANDARD: Zu Beginn der Covid-19-Pandemie waren Sie in der letzten Phase Ihres Projekts zu "Medizin in Zeiten der Austerität", also bei Knappheit und Sparpolitik. Sie haben die Lage in Spanien analysiert, das Projekt wurde vom Schweizerischen Nationalfonds gefördert. Was war die wichtigste Erkenntnis?

Janina Kehr: Es geht nie nur um ein Sparen, sondern es gibt immer ein Nebeneinander von Knappheit und Verschwendung, von Mangel und Überfluss. Klar: In Spanien hat die Regierung viel gespart, vor allem seit der Finanzkrise. Nun wurde in Madrid ein neues Pandemiekrankenhaus für über 100 Millionen Euro errichtet – wo die Betten aufgebaut werden, die in den zehn Jahren zuvor abgebaut wurden. Das Problem mit dieser Kurzfristigkeit: Es wird teils zu viel an profitträchtigen Stellen ausgegeben. Bei längerfristigen Investitionen zur Aufrechterhaltung von bestehenden Infrastrukturen oder Personal wird gespart. Mit mehr Krankenhäusern ist es aber nicht getan, auf die Schnelle kann man keine Krankenschwestern "produzieren".

STANDARD: Frauen übernehmen durchschnittlich häufiger Pflegeaufgaben. Wie wirkt sich die aktuelle Krise auf Geschlechterrollen aus?

Kehr: Man sieht deutlich, dass Frauen in der Krise mehr aufgebürdet wird, sei es Kinderbetreuung, Homeschooling oder die Pflege von Eltern. Das hat sich auch bei meiner Forschung in Spanien gezeigt: Wenn sich der Staat von Fürsorgeaufgaben zurückzieht oder sie auslagert, werden diese Aufgaben buchstäblich von Frauen geschultert. Sie tragen noch mehr als sonst und unbezahlt für ihre Familien Sorge.

...

Kehr: Öffentliche Gesundheitsversorgung ist ein wertvolles und hart erkämpftes Gut sozialer Sicherung, das es bei uns so erst seit Ende des Zweiten Weltkriegs gibt. In Pandemiezeiten merkt man besonders, dass mit Gesundheit und Krankheit nicht gehaushaltet werden kann wie mit Waren. In Krankenhäusern zum Beispiel fährt man im Namen von Effizienz und Kosteneinsparung permanent am Limit. Das funktioniert in dem Moment, wo man mehr Kapazitäten bräuchte, einfach nicht. Das ist keine gute Rechnung und es gibt auch keine einfachen, schnellen Lösungen. Es braucht viel längerfristige Planung.




# 20160908, https://www1.wdr.de/daserste/monitor/sendungen/intensiv-pflege-100.html

MONITOR vom 08.09.2016
Geschäfte mit Todkranken: Der boomende Markt der Intensiv-Pflege



# 20210228, https://www.zeit.de/2021/09/altenpflege-pflegeheim-intensivstation-corona-patienten

�rzte weisen betagte Covid-Patienten oft vorschnell auf Intensivstationen ein. Dabei ist es für viele alte Menschen sinnvoller, im Pflegeheim zu bleiben. Das aber verlangt den Medizinern Mut ab.
Ein Gastbeitrag von Gian Domenico Borasio 

Berichte aus Italien, insbesondere aus der Lombardei, legen nahe, dass auf dem Höhepunkt der ersten Welle die unreflektierte Verlegung von dementen Covid-19-Patienten aus den Pflegeheimen in die Krankenhäuser entscheidend zum Kollaps der medizinischen Versorgung in einigen Gemeinden beigetragen hat. 

Solche Verlegungen sind für Betroffene oft nicht nur unnütz, sondern auch schädlich. Denn Demenzpatienten werden durch den Umgebungswechsel destabilisiert, ihre Verwirrtheit nimmt zu, und sie sind dadurch stärker gefährdet, als wenn sie in ihrer vertrauten Umgebung geblieben wären. Hinzu kommen Krankenhauskeime, die für ältere Menschen lebensbedrohlich sein können. Die Zahlen aus der Zeit vor Covid-19 zeigen, dass die Wahrscheinlichkeit, eine Lungenentzündung zu überleben, für Pflegeheimpatienten deutlich größer ist, wenn sie in ihrem Heim bleiben. Die Entscheidung, Pflegeheimpatienten mit fortgeschrittener Demenz beim Auftreten einer Covid-19-Erkrankung nicht ins Krankenhaus einzuweisen, ist also schlichtweg das Gebot einer guten medizinischen Versorgung. 

Besonders wichtig: Diese Entscheidungen haben nichts mit Triage zu tun. Triage, ein Begriff, der ursprünglich aus der Kriegsmedizin stammt, ist definiert als die Priorisierung medizinischer Maßnahmen in Situationen, in denen aus Mangel an Ressourcen nicht allen Patienten, welche von diesen Maßnahmen profitieren könnten, gleichzeitig geholfen werden kann. In den oben beschriebenen Fällen geht es aber um Patienten, für welche die infrage kommenden medizinischen Maßnahmen mehr Schaden als Nutzen bringen würden. 


# 2013, https://www.berliner-zeitung.de/daenemark-jedem-sein-zuhause-li.34597

Dänemark hat die Seniorenbetreuung in den letzten 25 Jahren revolutioniert. Lebten damals noch acht von zehn Pflegebedürftigen in einem Zimmer mit gemeinsamer Küche und Bad in einem Altenheim, so ist jetzt nur noch jeder Achte an eine traditionelle Institution gebunden. Seit einem im Jahr 1987 verabschiedeten Reformgesetz ist Schluss mit den klassischen Heimen. Alle Neubauten seither sind Seniorenwohnungen mit Pflege- und Betreuungseinrichtungen und angeschlossenem Personal. Wie das „Højmarken“ in Gram: In jedem der vier Pavillons sind zehn Zwei-Zimmer-Wohnungen mit eigener Küchennische und geräumigem Bad um den gemeinsamen Speise- und Aufenthaltsraum angeordnet. Jede Wohnung hat einen kleinen Vorgarten. In der offenen Küche bereitet das Personal die Mahlzeiten zu, aber wer möchte, kann auch selbst kochen.


# 20201216, https://www.youtube.com/watch?v=9tdo2NOtsow

Hilfe gesucht: In einem Altersheim in Moischt müssen sich grauenvolle Dinge abgespielt haben. Weil alle Pfleger in Quarantäne sind, bleiben Bewohner unversorgt. 
(Video: Nadine Weigel)

# 20201005, https://www.washingtonpost.com/world/2020/10/15/long-term-elder-care-coronavirus-nursing-homes-research-lessons/

Many tactics have a significant financial cost. In South Korea or Hong Kong, where the state is heavily involved in long-term care facilities, the government may cover them. But in nations where private ownership of elder-care homes is common, such as Britain and the United States, companies must keep expenses down.

“They have to provide a lot more staff, they have to provide a lot more equipment, they have to have more space,” said Shereen Hussein, a professor of care and health policy at the University of Kent. “This means profit margins are reduced.”

Elder-care facilities may see significant changes — and not just in the short term. The International Long-Term Care Policy Network predicts higher costs and lower demand for elder-care services may not be a blip but could last for “many years to come.”

# 20201109, https://ltccovid.org/2020/11/19/what-went-wrong-and-what-could-be-learned-from-the-institutional-and-organizational-management-of-care-homes-during-the-covid-19-pandemic-in-spain/

According to all available data, Spain has been one of the European countries most seriously hit by the pandemic, experiencing dismaying figures regarding the number of deaths in care homes for older people. Between March and August 2020, at least half of the deceased due to the pandemic resided in care homes.

Moreover, distant threats should not be underestimated, while domestic capacities should not be overrated (especially after having gone through a huge economic crisis during the last decade that seriously affected the healthcare and Long-Term Care sectors).

Inter-sectoral coordination between healthcare and social services (which concerns ministries and Regional departments), as well as between care homes and healthcare centres, must be immediately activated in a pandemic. The relationship between the NHS and long term care has always been susceptible to improvement. The lack of coordination between these two sectors can be extremely damaging, as this research project confirmed in the case of Spain during the Covid-19.

Beyond the current crisis, it is necessary to rethink the future of the LTC sector in Spain. Fourteen years after the definition of the Spanish dependency system, and after the budget cuts introduced in the aftermath of the 2008 financial crisis, it seems crucial:

    To secure quality LTC services for the older-age adults and for those who care for them. The entire society will benefit from the investment returns made to achieve this objective.
    To avoid ‘medicalization’ of the care homes sector, exploring alternatives such as improving the coordination mechanisms between the healthcare and social services sectors.
    To be attentive to proposals related to the future of the Social Services and LTC and its de-institutionalization.

see https://www.mc-covid.csic.es/english-version



# 20200720, https://www.bbc.com/news/uk-scotland-53492929

Coronavirus: What went wrong in Scotland's care homes?

During the coronavirus pandemic more people died with the virus in Scotland's care homes than in its hospitals. The latest figures show almost 1,900 deaths in care homes where Covid-19 is on the death certificate. A BBC Disclosure programme, The Care Home Scandal, looked at what went wrong.

At the height of the pandemic, 39 staff members at Whitehills care home in East Kilbride were off work.

In April, the BBC understands, the home had issued a red staffing alert - meaning it did not have enough staff to properly care for its residents.

Figures released to the BBC under Freedom of Information suggest absence rates like this were not unusual.

During lockdown, the Care Inspectorate received 30 red warnings that homes did not have enough staff to properly care for their residents, and 149 amber warnings that staffing was stretched.

Louise McKechnie believes the high level of absence at Whitehills affected her grandmother's care.

Bridget Snakenburg had been at Whitehills for four years when she contracted Covid, then had a stroke.

Louise - who herself works in the care sector - got into the home and was shocked by what she saw.

She said she found Bridget soiled and wet, in a dirty room, with an open bag of used PPE in her bathroom.

"That's how bad it was," Louise said.

"It was meant to be discarded right away, to prevent any more infection getting about.

"She'd been in her bed for a long time, and her room hadn't been touched for a long time. The debris had built up. I've never seen a room like that.

"There weren't enough staff to care for their needs."

# 20200608, https://www.regensburg-digital.de/sterben-unter-verschwiegenheitspflicht/08062020/

Annähernd die Hälfte aller Verstorbenen in der Covid 19-Pandemie stammt – nicht nur in Bayern – aus Alten- und Pflegeheimen. In vielen Orten sind diese Einrichtungen Hotspots. Zehn bayerische Städte und Kreise dominieren die bundesweite Statistik der relativen Todeszahlen. Dabei überragen einige das vielgescholtene Schweden bei diesen Zahlen deutlich. Doch anders als in Schweden findet in Bayern und Deutschland darüber so gut wie keine Debatte statt.

...

Nach einer fortlaufenden Studie eines englischen Forscherteams (hier zu den Zwischenergebnissen) zeichnet sich aber ab, dass der Anteil der Toten aus Alten- und Pflegeheime in vielen europäischen Ländern zwischen 40 und 60 Prozent liegt. So beträgt ihr Anteil in Österreich etwa 41 Prozent, 49 Prozent in Schweden, 51 Prozent in Frankreich und Belgien und 62 Prozent in Irland (Stand Ende Mai 2020)

...

Bayern: Mehr als die Hälfte der Toten aus “Massenunterkünften”

Der alleinige Blick auf die stets steigenden Infektionszahlen, der seit Monaten tagtäglich und oft ohne Relationen oder Einordnung überall angeboten wird, kann die gesundheitlichen und tödlichen Auswirkungen einer Corona-Infektion nicht erfassen. Zumal laut einer Auskunft der Regierung der Oberpfalz „ca. 80 Prozent der Infektionen symptomfrei“ verlaufen. Der detaillierte Blick auf die amtlich erfassten COVID-19-Todesfälle hingegen bringt etwas mehr Licht ins Katastrophenfall-Dunkel.

Die Zahl der vormals in bayerischen Altenheimen und Flüchtlingsunterkünften untergebrachten und an/mit einer COVID-19-Infektion Verstorbenen beträgt laut bayerischem Landesamt LGL 998 Menschen (Stand 1. Juni). Wenn man nun auch für Bayern die unvollständigen Meldungen an das RKI gemäß §36 IfSG überschlägig hinzurechnet, stammen mehr als die Hälfte der bayernweit gemeldeten COVID-19-Toten aus Massen- und Sammelunterkünften.

...

Untersucht man bayerische Landkreise mit hohen relativen Todeszahlen genauer, zeigen sich wiederum Cluster, in denen sich der Sars-Cov2-Virus massenhaft verbreiten konnte und in der Folge auch viele Todesopfer zu beklagen waren. So stammen von den 47 COVID-19-Toten, die etwa der gesamte Landkreis Amberg-Sulzbach zu beklagen hat, laut Auskunft des Landratsamts sage und schreibe 31 Tote (bzw. 66 Prozent) aus Altersheimen. An Leib und Leben betroffen waren und sind bislang aber nur die Bewohner (und Mitarbeiter) von drei Heimen. Eines davon, ein Altenheim des Bayerischen Roten Kreuzes (BRK) in Hirschau, sticht mit seinen 23 COVID-19-Toten besonders ins Auge. (Hier zum Bericht des Onetz von heute.)

Die Verhältnisse in Straubing erscheinen noch drastischer. In der niederbayerischen kreisfreien Stadt stammen 69 Prozent der COVID-19-Toten (33 von 48 insgesamt, so die Auskunft der städtischen Pressesetelle) aus Altersheimen, die an einer Hand abzuzählen sind. Wer die hohen relativen Todeszahlen der Stadt Straubing analysieren will, muss also vor allem die Verhältnisse in den fünf Altenheimen untersuchen, in denen laut offizieller Statistik circa 600 Personen untergebracht sind.

Im nordoberpfälzischen Landkreis Tirschenreuth untersucht zurzeit eine Arbeitsgruppe des RKI die dort zu beklagenden 134 Toten, die an/mit COVID-19 verstorben sind. Wie viele davon aus Alten- und Pflegeheimen stammen, diese und weitere detaillierte Fragen werden vom dortigen Landratsamt nicht beantwortet. Eine der wenigen konkreten Auskünfte, die das Landratsamt nach der dritten oder vierten Presseanfrage unserer Redaktion erteilte, bezog sich auf die Frage, in wie vielen Alten- und Pflegeeinrichtung COVID-19-Infektionen festgestellt und amtlich gemäß § 36 IfSG gemeldet wurden. Die Antwort:

    „In 14 Einrichtungen im Landkreis TIR wurden COVID-19-Infektionen (bei Bewohnern und Mitarbeiter oder nur bei Bewohnern) festgestellt. In 2 weiteren Einrichtungen waren nur Mitarbeiter (darunter auch Verwaltungspersonal) betroffen.“

Differenzierte Zahlen werden verweigert

Laut amtlicher Statistik gibt es im Landkreis Tirschenreuth 13 Alten- und Pflegeheime, in denen etwa 1.100 Personen untergebracht und betreut werden. Da offenbar in all diesen Einrichtungen COVID-19-Infektionen aufgetreten sind, ist anzunehmen, dass ein sehr hoher Anteil der an/mit COVID-19 Verstorbenen aus Tirschenreuther Altenheimen stammt. Dass es dabei auch gravierende Missstände gegeben haben könnte, legt ein Spendenaufruf des Tirschenreuther Landrats vom 25. März nahe. Aufgrund der Knappheit von Schutzausrüstung, insbesondere von Schutzmasken appellierte er darin an alle Unternehmen oder auch Privathaushalte zu spenden, „damit sich z. B. das Personal in den Kliniken, Heimen, Pflegedienste und die Einsatzkräfte ausreichend schützen können.“

...

Quasi-Nachrichtensperre in Tirschenreuth

Der bundesweit mit den höchsten relativen COVID-19-Todesfallzahlen berühmt-berüchtigt gewordene Landkreis Tirschenreuth nutzt offenbar die Gunst des Corona-Katastrophenfalls und ermächtigt sich selber zur Verhängung einer Quasi-Nachrichtensperre. Zur Abwehr von Presseanfragen über amtlich erhobene Daten bemüht man eine „Verschwiegenheitspflicht“, die man wie einen dichten Schleier über den ganzen Landkreis gelegt hat.

...

Der Skandal des Totschweigens

Dass den in Altenheimen untergebrachten Menschen – alleine in Bayern über 100.000 an der Zahl – immer noch Grundrechte und Freiheiten genommen werden, um sie angeblich vor einer Infektion zu schützen, obwohl dieser Schutz offenbar gescheitert ist, ist ein Skandal im Skandal. In jenem Skandal, der im Zuge des Corona-Katastrophenfalls als neue gesellschaftliche Normalität hingenommen und in Tirschenreuth mit einer selbstgeschaffenen „Verschwiegenheitspflicht“ verteidigt und aktiv beschwiegen wird.

# 20200705, https://www.tagesspiegel.de/wissen/corona-tote-in-schweden-jeder-zweite-hat-zuvor-in-einem-seniorenheim-gelebt/25811204.html

Die Zahl der Toten in Schweden klettert auf mehr als 3000. Rund die Hälfte der Verstorbenen war älter als 70 und pflegebedürftig. 

Schweden hat in der Coronavirus-Krise eine neue traurige Marke erreicht: Nach Angaben der staatlichen Gesundheitsbehörde stieg die Zahl der Patienten, die in Verbindung mit einer Covid-19-Erkrankung gestorben sind, am Donnerstag um 99 auf 3040. „Dies ist eine schrecklich große Zahl“, hatte der Staatsepidemiologe Anders Tegnell, der die schwedische Regierung berät, bereits am Vortag gesagt.



# 20200521, https://de.euronews.com/2020/05/21/schwedens-coronapolitik-senioren-kommen-nicht-ins-krankenhaus



# 20200723, https://www.deutschlandfunkkultur.de/coronavirus-in-schweden-toedlicher-sonderweg-fuer-die-alten.976.de.html?dram:article_id=481127

Doch es gab keine besonderen Auflagen für die Heime und ihre Bewohner, nur die gängigen Verhaltensregeln: Soziale Kontakte sollten sie vermeiden, nicht einkaufen gehen. „Erst als die ersten Todesfälle auch in Altersheimen auftragen, wurde ein Besuchsverbot eingeführt“, so Szebehely. Das war Ende März.

Doch zu diesem Zeitpunkt grassierte das Virus in mehreren stationären Einrichtungen – auch im Heim von Oscarsson. Innerhalb kurzer Zeit starben acht Bewohner an Covid-19. Etwas Ähnliches hat die Schwedin zuvor nicht erlebt. „Alles ging sehr schnell und wir haben es kaum geschafft, alle Bewohner zu begleiten“, erinnert sie sich an diese schwierige Zeit. Viele der alten Menschen hätten schon lange in dem Heim gelebt, entsprechend eng war die Bindung zum Pflegepersonal. „Es war sehr traurig, sie zu verlieren.“

...

In den ersten Wochen der Pandemie zeigten sich dabei viele Probleme, auf die Kritiker bereits lange vorher aufmerksam gemacht hatten. In Schweden sind viele Heime in den vergangenen Jahren privatisiert worden. Die Stammbelegschaft ist kleiner geworden, viele Mitarbeiterinnen arbeiten auf Stundenbasis in unterschiedlichen Abteilungen.

Szebehely sagt, dass jede vierte Pflegekraft in einem unsicheren Beschäftigungsverhältnis steht. Sie bekommen kein Geld, wenn sie nicht zur Arbeit kommen. Jetzt hätten viele Kommunen dieses Personal für zwei Monate fest angestellt. „Um nicht zu riskieren, dass sie auch mit milden Symptomen zur Arbeit gehen und dann andere anstecken“, so die Wissenschaftlerin. 

Sozialarbeitsforscherin Szebehely ist gespannt auf die Ergebnisse, in einem aber ist sie skeptisch. Nämlich, dass „geschlossene Grenzen oder Schulen etwas geändert hätten“. Sie glaubt viel mehr, dass die Ausbreitung an den Strukturen der Pflegeeinrichtungen liegt: zu wenig und zu unerfahrenes Personal etwa. „Und infizierte Mitarbeiter, die trotz Symptomen zur Arbeit gegangen sind.“ Ein Lockdown hätte keinen großen Unterschied gemacht.

# 20201206, https://www.welt.de/debatte/kommentare/article221906282/Deutschland-wie-Schweden-im-Umgang-mit-Corona-Kranken-in-Pflegeheimen.html

Dazu passt ein furchtbarer, fast zynischer Begriff, den ein bekannter Epidemiologe in die Welt gesetzt hat. Er beschreibt eine Politik, die nur steigende Infektionszahlen anerkennt, aber die Alten nicht schützt und die Sterbenden und die Toten nicht sieht. Von „kalter Herdenimmunität“ ist die Rede. Denn wer tot ist, steckt niemanden mehr an.


# 20201109, https://pubmed.ncbi.nlm.nih.gov/33165560/

file: ref/brown20.pdf
citation: @Brown2020

Association Between Nursing Home Crowding and COVID-19 Infection and Mortality in Ontario, Canada

Conclusions and relevance: In this cohort of Canadian nursing homes, crowding was common and crowded homes were more likely to experience larger and deadlier COVID-19 outbreaks. 

# 20200817, https://pubmed.ncbi.nlm.nih.gov/32699006/

file: ref/stall20.pdf
citation: @Stall2020

For-profit long-term care homes and the risk of COVID-19 outbreaks and resident deaths 

For-profit status is associated with the extent of an outbreak of COVID-19 in LTC homes and the number of resident deaths, but not the likelihood of outbreaks. Differences between for-profit and nonprofit homes are largely explained by older design standards and chain ownership, which should be a focus of infection control efforts and future policy. 

# 20201224, https://steiermark.orf.at/stories/3082188/

Steiermark: Meiste CoV-Tote – aber warum?

Obwohl die Steiermark bei den CoV-Infektionszahlen bundesweit nur an vierter Stelle steht, ist sie das Bundesland mit den meisten CoV-Todesfällen – bislang 1.200. Liegt das an den vielen steirischen Pflegeheimen? 

Während Wien und Oberösterreich bei etwas mehr als 1.000 CoV-Toten liegen, sind es in der Steiermark über 1.200. Klaus Vander, Hygieniker der Krankenanstaltengesellschaft KAGes, nennt als mögliche Gründe, „dass die Steiermark nach den Bundesländern Burgenland und Kärnten das höchste Durchschnittsalter und nach Wien ebenso den höchsten Anteil an Pflegebetten aufweist“.

Das vermutet auch Gesundheitslandesrätin Juliane Bogner-Strauß (ÖVP) – knapp mehr als die Hälfte der Corona-Toten in der Steiermark hatten ihren Hauptwohnsitz in einem Pflegeheim. „Wir haben mit der zweiten Welle sehr hohe Infektionszahlen in der Bevölkerung. Wir können keine Glaskuppel über die Pflegeheime stülpen, und ich möchte das auch gar nicht: Die Menschen, die dort wohnen, brauchen Besuche, menschliche Nähe. Aber das bringt immer ein Risiko mit sich. Die Steiermark hat daher allein in den letzten Wochen 80.000 Testungen in den Pflegeheimen gemacht“, so Bogner-Strauß

Fast 230 Senioren- und Pflegeheime

In der Steiermark gibt es fast 230 Senioren- und Pflegeheime, auf Platz zwei unter den Bundesländern liegt Oberösterreich mit 130. Das bedeutet auch: In der Steiermark gibt es besonders viele kleine und mehr gewinnorientierte Heime, die also nicht vom Land oder von gemeinnützigen Trägern wie Caritas oder Volkshilfe betrieben werden. Kann das ein Grund sein für mehr Todesfälle? Bogner-Strauß: „Was derzeit die Cluster und Fälle angeht, scheint das keine Rolle zu spielen. Wir haben leider sowohl in privaten Pflegeheimen als auch in drei von vier Landespflegezentren große Cluster.“

Lernen für die Zukunft

Aber große Träger tun sich leichter, Pflegekräfte zu ersetzen, die in Quarantäne müssen, sagt Hygieniker Vander, und Jakob Kabas, Sprecher der von Land und Gemeinden betriebenen Heime, meint, dass man für die Zukunft lernen könne.

„Wir haben in Lassing ein Wohngruppenmodell, das sogenannte Pflegeheim der vierten Generation, wo ich Wohngruppen habe mit maximal 14, 15 Bewohnern und Bewohnerinnen. Dort tue ich mich in der Isolierung leichter, weil ich dann einfach die Wohngruppe dicht mache. Die Herausforderung wird sicher auch sein: Wie sieht eine pandemietaugliche Struktur eines Pflegeheims aus“, so Kabas.

Mehrheit überlebte

Von rund 18.000 Pflegeheimbewohnern in der Steiermark ist bisher laut Zahlen des Landes jeder/jede Fünfte mit Sars-CoV-2 infiziert worden. Von den Infizierten in Pflegeheimen ist jeder Sechste verstorben – die große Mehrheit der infizierten Pflegeheimbewohner hat das Coronavirus also überlebt.



# 20201214, https://www.lr-online.de/lausitz/weisswasser/corona-in-sachsen-26-pflegeheime-im-kreis-goerlitz-in-quarantaene-53825457.html

26 Pflegeheime im Kreis Görlitz in Quarantäne

Sieben neue Ansteckungen mit dem Coronavirus meldet der Kreis Görlitz am Montag, fünf weitere Todesfälle im Zuge der Pandemie werden gezählt.

Gegenwärtig sind im Landkreis Görlitz 26 Pflegeeinrichtungen von Corona-Fällen betroffen und unter Quarantäne gestellt worden. Wie das Gesundheitsamt des Kreises informiert, zählen dazu fünf Einrichtungen der Behindertenhilfe in Weißwasser, Görlitz, Reichenbach und Herrnhut sowie 21 Seniorenheime in Bad Muskau, Bernstadt, Ebersbach-Neugersdorf Görlitz, Jonsdorf, Löbau, Mittelherwigsdorf, Oybin, Olbersdorf, Ostritz, Seifhennersdorf und Zittau. Bei weiteren fünf Einrichtungen bestünden darüber hinaus noch Verdachtsfälle, die bislang aber noch nicht bestätigt sind.


# 20201212, https://www.aargauerzeitung.ch/schweiz/trotz-steigender-fallzahlen-altersheime-sehen-sich-diesmal-gegen-corona-gewappnet-139465332

Trotz steigender Fallzahlen: Altersheime sehen sich diesmal gegen Corona gewappnet

Im Frühling traf das Virus die Altersheime stark. In manchen starben mehrere Bewohner, die Gesunden litten unter Besuchsverboten. Aktuell sei die Lage aber ganz anders, sagen Experten.

Die Ältesten litten besonders unter der ersten Welle der Coronakrise. Gut die Hälfte der Covid-Toten in der Schweiz lebte zuvor in einem Alters- oder Pflegeheim, wie im Sommer bekannt wurde. Nun, da die Infektionen wieder stark steigen, vereinzelt auch Infektionen in Altersheimen gemeldet werden, stellt sich manch einer die bange Frage: Was bedeutet das für die Bewohner? Werden sie diesmal besser geschützt als im Frühling? Ja, versichern Experten.



# 20200806, https://www.br.de/nachrichten/bayern/coronavirus-fordert-viele-opfer-in-bayerns-pflegeheimen,S6pRux1

* Pflegekräfte in der Altenpflege besonders häufig an Corona erkran
* Würzburg: Gleich zwei Seniorenheime von Infektionen betroffe
* Langenzenn: Mitarbeiter beklagen Hygienemänge
* Aichach: Polizei durchsucht Heim nach Todesfälle
* Schliersee: Bundeswehr hält Betrieb in Seniorenresidenz aufrech
* Bad Steben: 17 Menschen sterben nach Corona-Infektion
* Arzberg: Pflegeheim muss geräumt werde

# 20201015, https://www.tagesschau.de/ausland/coronavirus-belgien-101.html

Positiv getestete Krankenpfleger müssen weiterarbeiten

Die zweite Welle war absehbar - warum hat man sich also nicht besser
vorbereitet und mehr Intensivbetten geschaffen? Das Problem sind
weniger die Bettenkapazitäten als das Personal: Ärzte, Schwestern und
Pfleger gab es in Belgien schon vor Corona zu wenige, nun müssen sie
noch mehr leisten. Man könne sie ja schlecht klonen, meint Virologin
Erika Vlieghe.

Hinzu kommt, dass auch das Krankenhauspersonal nicht immun gegen das
Virus ist. Fast jeder fünfte ist krank. Und der Rest soll nun die
zweite Welle auffangen. So auch Gaëtan Mestag, er ist Krankenpfleger
in Brüssel. Bei ihm gehen in der Woche 200 Menschen ein und aus, um
sich von ihm auf Covid-19 testen zu lassen. Vor kurzem wurde er selbst
positiv getestet. Trotzdem arbeitete er weiter. "Ich kann meine
Kollegen nicht im Stich lassen und in Quarantäne gehen: Ich müsste
meine Behandlungen absagen - weil ich keinen finde, der auch nur die
Hälfte meiner Arbeitstage übernehmen könnte", sagt er.

Und Mestag ist keine Ausnahme. Dass Covid-infiziertes Medizinpersonal
möglicherweise nicht-infizierte Patienten behandelt, ist in Belgien
nicht nur erlaubt, sondern wohl auch an der Tagesordnung. Das System
würde sonst kollabieren.

[...]

Jahrelange Sparmaßnahmen rächen sich

Der neue Gesundheitsminister Frank Vandenbroucke, der die Krise jetzt
lösen muss, beschönigt nichts. Auf die Frage ob er eine Implosion des
Gesundheitssystem für möglich hält, sagt er schlicht: "Absolut!"

Nun rächten sich die Sparmaßnahmen der Vergangenheit, kritisiert
Intensivmediziner Philippe Devos. 2020 sei das erste Jahr, in dem die
Regierung das Gesundheitsbudget um 1,2 Milliarden Euro
aufstocke. Davor seien über die letzten zehn Jahre Hunderte Millionen
Euro abgezogen worden. Man versuche also nun auszugleichen, was man
vorher eingespart habe. Das sei zwar sehr gut, komme aber zu spät.


# https://www.thelancet.com/journals/lanpub/article/PIIS2468-2667(20)30073-6/fulltext
The effect of control strategies to reduce social mixing on outcomes of the COVID-19 epidemic in Wuhan, China: a modelling study

-> population mixing matrix

# https://www.sciencedirect.com/science/article/abs/pii/S0022519315005366
Media coverage and hospital notifications: Correlation analysis and optimal media impact duration to manage a pandemic

-> media publicity should be focused on how to guide people׳s
   behavioral changes.

# https://www.sciencedirect.com/science/article/pii/S1755436514000607
Synthesizing data and models for the spread of MERS-CoV, 2013: Key role of index cases and hospital transmission

# https://onlinelibrary.wiley.com/doi/full/10.1111/j.1348-0421.2007.tb03978.x
Mathematical Modeling of Severe Acute Respiratory Syndrome Nosocomial Transmission in Japan: The Dynamics of Incident Cases and Prevalent Cases

 Severe Acute Respiratory Syndrome (SARS) occurred in Hong Kong in
 late February 2003, resulting in 8,096 cumulative cases with 774
 deaths.  The simulation results revealed that it was important to
 recognize and isolate SARS patients as early as possible and also to
 prevent the transmission spreading outside the hospital to control an
 epidemic.

# https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4318875/
A Three-Scale Network Model for the Early Growth Dynamics of 2014 West Africa Ebola Epidemic

We combine a discrete, stochastic SEIR model with a three-scale
community network model to demonstrate that the different regional
trends may be explained by different community mixing
rates. Heuristically, the effect of different community mixing rates
may be understood as the observation that two individuals infected by
the same chain of transmission are more likely to share the same
contacts in a less-mixed community.

# https://www.cambridge.org/core/journals/infection-control-and-hospital-epidemiology/article/combining-highresolution-contact-data-with-virological-data-to-investigate-influenza-transmission-in-a-tertiary-care-hospital/AF37EA2BC6D7C2DDC8400479B645003D

Combining High-Resolution Contact Data with Virological Data to Investigate Influenza Transmission in a Tertiary Care Hospital

A total of 18,765 contacts were recorded among 37 patients, 32 nurses,
and 15 medical doctors. Most contacts occurred between nurses or
between a nurse and a patient. Fifteen individuals had influenza A
(H3N2). Among these, 11 study participants were positive at the
beginning of the study or at admission, and 3 patients and 1 nurse
acquired laboratory-confirmed influenza during the study. Infectious
medical doctors and nurses were identified as potential sources of
hospital-acquired influenza (HA-Flu) for patients, and infectious
patients were identified as likely sources for nurses. Only 1
potential transmission between nurses was observed.

# McMichael2020, https://www.nejm.org/doi/full/10.1056/NEJMoa2005412

Epidemiology of Covid-19 in a Long-Term Care Facility in King County,
Washington

As of March 18, a total of 167 confirmed cases of Covid-19 affecting
101 residents, 50 health care personnel, and 16 visitors were found to
be epidemiologically linked to the facility. Most cases among
residents included respiratory illness consistent with Covid-19;
however, in 7 residents no symptoms were documented. Hospitalization
rates for facility residents, visitors, and staff were 54.5%, 50.0%,
and 6.0%, respectively. The case fatality rate for residents was 33.7%
(34 of 101). As of March 18, a total of 30 long-term care facilities
with at least one confirmed case of Covid-19 had been identified in
King County.

# 20200422, https://www.aerzteblatt.de/nachrichten/112145

Etwa 4.600 Menschen sind in Deutschland bislang infolge des
Coronavirus SARS-CoV-2 gestorben. Davon lebten mindestens 1.491 in
Alten- und Pflegeheimen sowie an­deren Betreuungseinrichtungen. Das
entspricht etwa einem Drittel aller gemeldeten To­desfälle, wie das
Robert-Koch-Institut (RKI) auf Anfrage von NDR Info mitteilte. 17
Pro­zent aller infizierten Bewohner sind gestorben, also fast jeder
fünfte.

Die Zahlen dürften allerdings erheblich höher liegen, da bei 41
Prozent aller Todesfall-Meldungen Angaben fehlten, in welchen
Einrichtungen die Infektionen festgestellt wur­den. 966 der 4.600
Todesfälle können nicht zugeordnet werden.

# rki_daily_report_2020-04-22-en.pdf

Thus far, 7,862 cases with a SARS-CoV-2 infection have been notified
among staff working in medical facilities as defined by Section 23
IfSG (Table 3). Among the cases reportedly working in medical
facilities, 72% were female and 28% male. The median age was 41
years. Hospitalisation was reported for 333 of 7,365 cases among staff
working in medical facilities with information available (4,5%). There
were 18 COVID-19 related deaths among staff working in medical
facilities.The proportion of cases reported as working in medical
facilities among all cases increased over time from at least 3.6% in
Week 12, 4.8% in Week 13, 5.5% in Week 14, 6.8% in Week 15, to 7.0% in
Week 16, 2020. Due to missing data on occupation over 40% of cases,
the true proportion of cases working in medical facilities may be
higher.


# 20200424, https://www.bild.de/news/2020/news/coronavirus-in-pflegeheimen-schon-20-pflegekraefte-gestorben-70241946.bild.html

Etwa in Niedersachsen gab es seit März Corona-Ausbrüchen in mehreren
Heimen. Im Hanns-Lilje-Heim in Wolfsburg starben 42 von 74
Infizierten. Inzwischen seien laut Diakonie nur noch wenige Bewohner
Corona-positiv. Keiner befinde sich mehr zur stationären Behandlung im
Krankenhaus.

# 20200421, https://www.tagesschau.de/investigativ/ndr/corona-zahlen-heime-101.html

Robert Koch-Institut: Mindestens 14.000 Coronafälle in Heimen

Laut Zahlen des Robert Koch-Instituts gibt es in Heimen und
Massenunterkünften mindestens 14.000 Coronafälle. Tatsächlich dürfte
die Zahl viel höher liegen.

Mindestens 14.228 Coronafälle haben Alten- und Pflegeheime sowie
andere Betreuungseinrichtungen und Massenunterkünfte bisher
gemeldet. Nach einer Anfrage von NDR Info am Wochenende
veröffentlichte das Robert Koch-Institut am Montagabend erstmals
Zahlen zu diesem Bereich. Das RKI führt die "hohen Fallzahlen bei
Betreuten und Tätigen in Pflegeeinrichtungen" auf die Ausbrüche dort
in den vergangenen Wochen zurück. Die Zahlen dürften allerdings
erheblich höher liegen, da bei 41 Prozent aller Meldungen Angaben dazu
fehlten, in welchen Einrichtungen die Infektionen aufgetreten sind.

Nur 83.000 der insgesamt 142.000 offiziellen Meldungen enthielten
Informationen darüber, ob die Coronafälle den Gesundheitsbereich,
Betreuungseinrichtungen oder die Lebensmittelherstellung
betreffen. Diese Einrichtungen sind laut Infektionsschutzgesetz
relevant für das Infektionsgeschehen und müssen deshalb gemeldet
werden. Rund ein Drittel der detaillierten Infektionsmeldungen
entfällt auf diese relevanten Bereiche.

Betreuungseinrichtungen am stärksten betroffen

Mit 17 Prozent machen Betreuungseinrichtungen den größten Anteil
aus. Dazu zählen neben Alten- und Pflegeheimen etwa auch Gefängnisse,
Obdachlosen- und Flüchtlingunterkünfte. Dort gibt es 8592 Fälle unter
den Betreuten und 5636 Fälle unter den Beschäftigten. Auf das
Gesundheitswesen entfallen 11 Prozent der detaillierten Meldungen. Die
Mehrheit stellen 7413 Mitarbeiter, von denen bisher 13 gestorben
sind. Angaben über Todesfälle in den anderen relevanten Bereichen
veröffentlichte das RKI zunächst nicht.

Kinder- und Jugendeinrichtungen wie Schulen, Kitas und Heime machen
lediglich 4 Prozent (3649 Fälle) aus. Das Institut führt das auf die
Schulschließungen sowie auf "das bislang niedrigere Erkrankungsrisiko
bei Kindern" zurück. Nur knapp 1 Prozent (739 Fälle) entfällt auf
Küchen, Lebensmittelproduktion und -handel gemeldet.

Die Berichte über Infektionen in Alten- und Pflegeheimen hatten sich
in den vergangenen Wochen gehäuft. Allein in Niedersachsen wurde das
Virus in rund 80 dieser Einrichtungen nachgewiesen, hatte das
Gesundheitsministerium in Hannover am Wochenende mitgeteilt. Die
Gewerkschaft Verdi kritisierte, vielerorts fehle es in Alten- und
Pflegeheimen noch an Schutzkleidung.

# 20200417, Comas-Herrera et al. Mortality associated with COVID-19
outbreaks in care homes: early international evidence

https://ltccovid.org/2020/04/12/mortality-associated-with-covid-19-outbreaks-in-care-homes-early-international-evidence/

Data from 3 epidemiological studies in the United States shows that as
many as half of people with COVID-19 infections in care homes were
asymptomatic (or pre-symptomatic) at the time of testing. New data
from Belgium shows that 73% of staff and 69% of residents who tested
positive were asymptomatic.

Official data from 7 countries suggests that the share of care home
residents whose deaths are linked to COVID-19 is much lower in 2
countries where there have been fewer deaths in total (14% in
Australia, where there have been 63 deaths, and 20% in Singapore,
where there have been 10 deaths).

In the remaining 5 countries for which we have official data (Belgium,
Canada, France, Ireland and Norway), and where the number of total
deaths ranges from 136 to 17,167, the % of COVID-related deaths in
care homes ranges from 49% to 64%).  Data reported by media as coming
from official sources for Portugal and Spain suggests rates of 33% and
53% respectively.

