
## scrape German Kreise Population from Wikipedia
library("rvest")
library("xml2")
library("lubridate")

options(stringsAsFactors=FALSE)
## english time
Sys.setlocale("LC_TIME", "C")

if ( interactive() ) setwd("~/ref/corona")
source(file.path("scripts","utils.R"))

store <- TRUE#!interactive()

tm <- format(Sys.time(), "%Y%m%d_%H%M")
## Landkreise
url <- "https://de.wikipedia.org/wiki/Liste_der_Landkreise_in_Deutschland"

htm <- xml2::read_html(url)
htmt <-  rvest::html_table(htm,fill=TRUE)
kreise <- htmt[[1]]

kreise <- data.frame(kreise)

kreise$KrS <- sprintf("%05d", kreise$KrS)
kreise$BL <- gsub(" .*","",kreise$BL) # note: special symbol?
kreise$Kreis <- paste("LK",gsub("\\[FN .*\\]","",kreise[,"Landkreis.Kreis"]))
kreise$Kreissitz <- gsub("\\[FN .*\\]","",kreise$Kreissitz)

## filter
kreise <- kreise[,c("KrS","Kreis","BL","Kreissitz","Einw.","Fl.in.km.")]
colnames(kreise) <- c("KrS","Kreis","BL","Kreissitz","population","area.km2")

## process numbers
kreise$population <- as.numeric(gsub("\\.","",kreise$population))
kreise$area.km2 <- as.numeric(gsub(",",".",gsub("\\.","",kreise$area.km2)))

## kreisfreie Staedte
## TODO: get list of PLZ for each
url <- "https://de.wikipedia.org/wiki/Liste_der_kreisfreien_St%C3%A4dte_in_Deutschland"

htm <- xml2::read_html(url)
htmt <-  rvest::html_table(htm)
skreise <- htmt[[2]]

skreise <- data.frame(skreise)

skreise$Kreis <-  paste("SK",gsub("\\[.*","",
                                  gsub("(\\S)\\(.*","\\1",skreise$Stadt)))
bidx <- grep("Regierungs.bezirk",colnames(skreise))
skreise[,"Regierungs­bezirk[4]"] <- gsub("0–","",skreise[,bidx])
skreise$EWjetzt <-gsub("\\.","", gsub(" \\(.*","",skreise$EWjetzt))
skreise$KrS <- NA # TODO: find PLZ

## filter
skreise <- skreise[,c("KrS","Kreis","BL",colnames(skreise)[bidx],
                      "EWjetzt","Flächeinkm.")]
colnames(skreise) <- c("KrS","Kreis","BL","Kreissitz","population","area.km2")

## process numbers
skreise$population <- as.numeric(gsub("\\.","",skreise$population))
skreise$area.km2 <- as.numeric(gsub(",",".",gsub("\\.","",skreise$area.km2)))

kreise <- rbind(kreise,skreise)

## fix some names

file.name <- file.path("downloadedData","population_germany_kreise.csv")
write.table(kreise, file=file.name, quote=FALSE, na="",
            row.names=FALSE, sep="\t")
  
## BUNDESLÄNDER
url <- "https://de.wikipedia.org/wiki/Liste_der_deutschen_Bundesl%C3%A4nder_nach_Bev%C3%B6lkerung"

htm <- xml2::read_html(url)
htmt <-  rvest::html_table(htm, dec=",",fill=TRUE)
bl <- htmt[[1]]

bl <- data.frame(bl)

bl <- cbind.data.frame(state=sub(" .*","",bl$Bundesland),
                       population=as.numeric(gsub("\\.","",bl[,"Einwohner"])))

bl <- bl[-grep("Bundesland",bl[,1]),]
file.name <- file.path("downloadedData","population_germany_bundeslaender.csv")
write.table(bl, file=file.name, quote=FALSE, na="",
            row.names=FALSE, sep="\t")
