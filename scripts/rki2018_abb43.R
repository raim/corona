library(lubridate)
setwd("~/ref/corona")

## nice date axis
dateax <- function(dw,dm,dy,mgp=par("mgp")) {
    axis.Date(1, at=dw, labels=NA, tcl=-.15)
    axis.Date(1, at=dm, tcl=-.3, labels=NA, mgp=mgp)
    mgp[2] <- 2*mgp[2]
    axis.Date(1, at=dy, tcl=-.5, format="%Y", mgp=mgp)
}

## TODO: why is last point too low? cut!
## TODO: check x-axis and convert to dates
## TODO: interpolate to actual weeks,

## Mortality in Berlin, 1. KW 2015 to 20. KW 2019
dat <- read.csv("originalData/rki2018_abb43.csv")
dat <- dat[1:(nrow(dat)-1),]

## convert to dates
dy <- seq(from=as.Date("2015-01-01"), to=as.Date("2019-01-01"),by="year")
dm <- seq(from=as.Date("2015-01-01"), to=as.Date("2019-04-15"),by="month")
dw <- seq(from=as.Date("2015-01-01"), to=as.Date("2019-04-15"),by="week")
dt <- seq(from=as.Date("2015-01-01"), to=as.Date("2019-04-15"), length.out=nrow(dat))


cls <- c("red","gray","black")
png(file.path("figures","rki2018_abb43_digitized.png"), units="in",
    width=7, height=3.5, res=200)
par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
matplot(dt, dat[,2:4], type="l", ylim=c(400,1000), lty=1,lwd=c(1,3,1),
        xlab="", ylab="deaths per week", col=cls,
        main="mortality in Berlin, 1. KW 2015 to 20. KW 2019", axes=FALSE)
axis(2)
dateax(dw,dm,dy)
legend("topleft", rev(c("raw","trend","CI")), col=cls, lty=1,lwd=c(1,3,1))
dev.off()

## interpolate to weeks
raw <- approx(x=dt, y=dat$raw, xout=dw)
## extrapolate from berlin to germany
raw$y <- raw$y*20

## interpolated
png(file.path("figures","rki2018_abb43_interpolated.png"), units="in",
    width=7, height=3.5, res=200)
par(mai=c(.5,.5,.1,.1), mgp=c(1.3,.4,0))
plot(raw$x, raw$y, type="l", ylim=c(0,max(raw$y)))
dev.off()
