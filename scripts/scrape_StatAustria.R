library(lubridate)
options(stringsAsFactors=FALSE)
Sys.setlocale("LC_TIME", "C")
## NOTE: library "prophet" required for forecasting

if ( interactive() ) setwd("~/ref/corona")
source(file.path("scripts","utils.R"))

## total mortality data from
## https://data.statistik.gv.at/web/catalog.jsp#collapse7
adat <- read.csv(url("https://data.statistik.gv.at/data/OGD_gest_kalwo_GEST_KALWOCHE_100.csv"),sep=";")

adat$date <- as.Date(paste0(adat$C.KALWOCHE.0,"-1"), format="KALW-%Y%U-%u")

## list of countries, add AUSTRIA as B00
countries <- c("B00",sort(unique(adat$C.B00.0)))

## load country codes
cnn <- read.csv(url("https://data.statistik.gv.at/data/OGD_gest_kalwo_GEST_KALWOCHE_100_C-B00-0.csv"), sep=";", row.names=1)[,"en_name",drop=FALSE]
cnn[,1] <- sub(" ","_",sub(" <.*","",cnn[,1]))
cnn <- rbind(cnn, B00="Austria")

## covid19 deaths austria
## TODO: get from https://www.data.gv.at/covid-19/ instead of ECDC
## TODO: use OWID data instead of ECDC
## https://info.gesundheitsministerium.at/data/TodesfaelleTimeline.csv
#cases <- read.delim(file.path("downloadedData","ecdc_cases_2.csv"))
#cases <- cases[which(cases$country_code=="AUT"),]

cases <- read.delim(file.path("downloadedData","owid.csv"))
cases <- cases[which(cases$iso_code=="AUT"),]
cases$date <- as.Date(cases$date)
cases <- cases[order(cases$date),]

DR <- caserate(cases$date, cases$total_deaths)
DR <- rbind(DR$CM,DR$ECM)
DR$cases.week[is.na(DR$cases.week)] <- 0 # add zeros
DR <- DR[which(DR$cases.week>0),]


## data processing params
mov.avg <- 6 # moving average
fourier.cut <- as.Date(c("2019-06-01","2011-06-01")) # full years for Fourier
annual.component <- TRUE # use annual instead of main fourier component?

## sums over age groups and Bundeslaender
## TODO: analyze per age group and Bundesland
dir.create(file.path("figures","Austria"))
##
cn="B00"
for ( cn in countries ) {

    cname <- cnn[cn,1]
    dat <- adat
    if ( cname!="Austria" )
        dat <- dat[dat$C.B00.0==cn,]

    dat <- split(dat, f=dat$date)
    dat <- cbind.data.frame(date=as.Date(names(dat)),
                            deaths.week=unlist(lapply(dat,
                                                      function(x)
                                                          sum(x$F.ANZ.1))))


    ## merge corona deaths per week into main data 
    dat <- merge(x=dat, y=DR, by="date", all=TRUE)

    ## TODO: get corona data per bundesland, or population size to scale
    ## until available, just set to NA
    if ( cname != "Austria" ) dat$cases.week <- NA

    ## add latest corona deaths to last w/o corona value
    lst <- tail(dat$deaths.week,1)
    ymn <- mean(dat$deaths.week,na.rm=TRUE)
    if ( is.na(lst) )
        lst <- tail(dat$cases.week,1)
            
    dat$deaths.wo.corona <- dat$deaths.week - dat$cases.week

    
    ## MOVING AVERAGE
    ddr.full <- filter(dat$deaths.week, rep(1/mov.avg,mov.avg), sides = 2) 
    ## FOURIER
    annual.component <- FALSE
    idx <- dat$date < fourier.cut[1] & dat$date >= fourier.cut[2] # take full years
    cdate <- dat$date[idx]
    ddr <- ddr.full[idx] # subset of times
    ddf <- fft(ddr)[1:(length(ddr)/2)]
    if ( annual.component ) {
        mxa <- 1+as.numeric(round(difftime(fourier.cut[1],fourier.cut[2],
                                           units="weeks")/52))
    } else {
        mxa <- 1+which.max(abs(ddf[2:length(ddf)])) # maximal component: yearly
    }
    phase <- complex2degree(ddf[mxa])
    amp <- abs(ddf[mxa])/length(ddr)
    mn <- abs(ddf[1])/length(ddr)
    rng <- as.POSIXct(range(cdate))
    period <- as.numeric(difftime(rng[2],rng[1],unit="weeks")/(mxa-1))
    ddtime <- seq(from=as.Date(cdate[1]), to=as.Date(today()), by="week") 
    ddtrend <- mn+amp*cos(2*pi/period*(1:length(ddtime)-1) - 2*pi*phase/360)
    
    
    ## PLOT
    ylm <- c(0,max(dat$deaths.week,na.rm=TRUE))
    ## date axis
    XLM <- list(all=c(as.POSIXct(min(dat$date,na.rm=TRUE)),
                      as.POSIXct(today())),
                zoom=c(as.POSIXct(today()-4.5*365), as.POSIXct(today())))
    date <- as.POSIXct(dat$date)
    last.date <- tail(date[!is.na(dat$deaths.week)],1)
    for ( xlm in names(XLM) ) {
        width <- 12
        if ( xlm=="zoom" ) width <- 7.5
        file.name <- file.path("figures","Austria",
                               paste0(cname,"_context_",xlm,".png"))
        png(file.name, units="in", width=width, height=3.5, res=200)
        par(mfcol=c(1,1),mai=c(.25,.5,.1,.5), mgp=c(1.4,.3,0),tcl=-.25)
        plot(date, dat$deaths.week, type="l", axes=FALSE,
             ylab="deaths/week", xlab="", xlim=XLM[[xlm]],ylim=ylm)
        dateax(1,date)
        axis(2)
        #if ( xlm=="zoom" ) {
            axis.POSIXct(1, at=last.date, tcl=1.5*par("tcl"), col="blue",
                         col.axis="blue", mgp=-3*par("mgp"), format="%m/%d")
        #}
        abline(v=as.POSIXct(last.date),col="blue", lty=2)
        abline(v=as.POSIXct(last.date - 3600*24*365),col="gray", lty=2)
	##abline(v=as.POSIXct(today()),col="blue", lty=2)
        ##lines(date,ddr.full, col="#00aa0099",lwd=3)
        lines(as.POSIXct(ddtime),ddtrend, col="#0000ff77",lwd=3)
        ## total deaths
        lines(date, dat$deaths.week)
        ## deaths w/o corona
        if ( cname == "Austria" ) {
            lines(date, dat$deaths.wo.corona, col=2)
            y1 <- dat$deaths.week
            x1 <- date[!is.na(y1)]
            y1 <- y1[!is.na(y1)]
            y2 <- dat$deaths.wo.corona
            x2 <- date[!is.na(y2)]
            y2 <- y2[!is.na(y2)]
            cut1 <- x1>=min(x2)
            polygon(x=c(x1[cut1],rev(x2)), y=c(y1[cut1], rev(y2)),
                    col="#ff000055",
                    border=NA)
            ## per 1e5 axis
            axe5 <- pretty(c(0,dat$deaths.week*1e5/cases$population[1]))
            axis(4, at=axe5/(1e5/cases$population[1]), labels=axe5)
            mtext("deaths/1e5/week", 4, par("mgp")[1])

            ## last point predicted if not available
            if ( is.na(tail(dat$deaths.week,1)) & xlm=="zoom" ) {
                ##arrows(x0=tail(date,1),y0=ymn,y1=lst,col=2,lwd=1.5,length=.1)
                y2l <- tail(y2,1)
                points(tail(date,1),y2l+lst,col=2,pch=4)
                arrows(x0=tail(date,1),y1=y2l,y0=y2l+lst,col=2,lwd=1,
                       length=.05,lty=2,angle=90)
            }
 
        }
        legend("bottomleft",cname,bty="n",cex=1.5)
        if ( cname=="Austria" )
            legend("bottom",
                   c("total deaths/week",
                     ##"moving average",
                     "main fourier component",
                     "deaths w/o corona"),
                   col=c("#000000","#0000ff77","#ff0000aa"),
                   lwd=c(1,3,2),pch=NA, 
                   y.intersp=.7,bg="#ffffffaa")
        dev.off()                
    }
    
    
### USE PROPHET PACKAGE FOR FORECASTING
    if ( !require(prophet) ) next
    
    ## train death/week data
    df <- dat[,c("date","deaths.week")]
    colnames(df) <- c("ds","y")
    ## cut before covid19
    df <- df[df$ds<as.Date("2020-01-01"),]
    m <- prophet(df)
    
    if ( interactive() )
        plot(m,predict(m, df))
    
    ## forecast
    ftime <- seq(min(dat$date), today()+1.5*365, by="day")
    ##if ( !exists("prd") ) # don't redo in interactive mode
    prd <- predict(m,data.frame(ds=ftime))

    ## plot forecast
    ddtime <- seq(from=as.Date(cdate[1]), to=max(ftime), by="week") 
    ddtrend <- mn+amp*cos(2*pi/period*(1:length(ddtime)-1) - 2*pi*phase/360)
    
    XLM <- list(all=c(as.POSIXct(min(dat$date,na.rm=TRUE)),
                      as.POSIXct(max(as.POSIXct(prd$ds)))),
                zoom=c(as.POSIXct(today()-4.5*365),max(as.POSIXct(prd$ds))))
    date <- as.POSIXct(dat$date) 
    last.date <- tail(date[!is.na(dat$deaths.week)],1)
    
    for ( xlm in names(XLM) ) {
        width <- 12
        if ( xlm=="zoom" ) width <- 7.5
        
        file.name <- paste0(cname,"_context_prediction_",xlm,".png")
        file.name <- file.path("figures","Austria",file.name)
        png(file.name, units="in", width=width, height=3.5, res=200)
        par(mfcol=c(1,1),mai=c(.25,.5,.1,.5), mgp=c(1.4,.3,0),tcl=-.25)
        plot(date, dat$deaths.week, type="l", axes=FALSE, col=NA,
             ylab="deaths/week", xlab="", xlim=XLM[[xlm]],ylim=ylm)
        lines(as.POSIXct(ddtime),ddtrend, col="#0000ff77",lwd=3)
        dateax(1,XLM[[xlm]])
        axis(2);
        #if ( xlm=="zoom" )
        axis.POSIXct(1, at=last.date, tcl=1.5*par("tcl"), col="blue",
                     col.axis="blue", mgp=-3*par("mgp"), format="%m/%d")
        ##abline(v=as.POSIXct(today()),col="blue", lty=2)
        abline(v=last.date,col="blue", lty=2)
        abline(v=as.POSIXct(last.date - 3600*24*365),col="gray", lty=2)
	## prophet prediction
        polygon(x=as.POSIXct(c(prd$ds, rev(prd$ds))),
                y=c(prd$yhat_lower, rev(prd$yhat_upper)), border=NA,
                col="#00aa0025")
        lines(as.POSIXct(prd$ds), prd$yhat, col="#00aa00", lwd=2)
        ## deaths with and w/o corona
        lines(date, dat$deaths.week)
        if ( cname == "Austria" ) {
            lines(date, dat$deaths.wo.corona, col=2)
            y1 <- dat$deaths.week
            x1 <- date[!is.na(y1)]
            y1 <- y1[!is.na(y1)]
            y2 <- dat$deaths.wo.corona
            x2 <- date[!is.na(y2)]
            y2 <- y2[!is.na(y2)]
            cut1 <- x1>=min(x2)
            polygon(x=c(x1[cut1],rev(x2)), y=c(y1[cut1], rev(y2)),
                    col="#ff000055",
                    border=NA)
            ## per 1e5 axis
            axe5 <- pretty(c(0,dat$deaths.week*1e5/cases$population[1]))
            axis(4, at=axe5/(1e5/cases$population[1]), labels=axe5)
            mtext("deaths/1e5/week", 4, par("mgp")[1])

            ## last point predicted if not available
            if ( is.na(tail(dat$deaths.week,1)) & xlm=="zoom" ) {
                y2l <- tail(y2,1)
                points(tail(date,1),y2l+lst,col=2,pch=4)
                arrows(x0=tail(date,1),y1=y2l,y0=y2l+lst,col=2,lwd=1,
                       length=.05,lty=2,angle=90)
            }
            
        }
        legend("bottomleft",cname,bty="n",cex=1.5)
        if ( cname=="Austria" )
            legend("bottom",
                   c("total deaths/week",
                     "main fourier component",
                     "deaths w/o corona",
                     "'prophet' forecast"),
                   col=c("#000000","#0000ff77","#ff0000aa","#00aa00"),
                   lwd=c(1,3,2,2),pch=NA, 
                   y.intersp=.7,bg="#ffffffaa")
        dev.off()
    }
}
    
