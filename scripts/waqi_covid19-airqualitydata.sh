#!/bin/bash

#The CSV data sets can be downloaded programatically: The url is
#https://aqicn.org/data-platform/covid19/report/11410-1960d830/period
#Where period is any of 2019Q1, 2019Q2, 2019Q3, 2019Q4, 2018H1, 2017H1, 2016H1, 2015H1.

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
DLP=$SCRIPTPATH/../bigData/

curl --compressed -o $DLP/waqi-covid-2020.csv   https://aqicn.org/data-platform/covid19/report/11410-1960d830/2020
curl --compressed -o $DLP/waqi-covid-2019Q1.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2019Q1
curl --compressed -o $DLP/waqi-covid-2019Q2.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2019Q2
curl --compressed -o $DLP/waqi-covid-2019Q3.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2019Q3
curl --compressed -o $DLP/waqi-covid-2019Q4.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2019Q4
curl --compressed -o $DLP/waqi-covid-2018H1.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2018H1
curl --compressed -o $DLP/waqi-covid-2017H1.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2017H1
curl --compressed -o $DLP/waqi-covid-2016H1.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2016H1
curl --compressed -o $DLP/waqi-covid-2015H1.csv https://aqicn.org/data-platform/covid19/report/11410-1960d830/2015H1

#To download the list of stations with latitude, longitude and EPA feed: 
curl --compressed -o $DLP/airquality-covid19-cities.json https://aqicn.org/data-platform/covid19/airquality-covid19-cities.json
