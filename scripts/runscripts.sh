#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

cd $SCRIPTPATH/..

R --vanilla < scripts/scrape_NPGEO.R &> log/npgeo.log
#R --vanilla < scripts/scrape_RKI.R  &> log/rki.log
#R --vanilla < scripts/scrape_worldometer.R &> log/worldometer.log
#R --vanilla < scripts/scrape_ECDC.R &> log/ecdc.log
R --vanilla < scripts/scrape_ECDC_weekly.R &> log/ecdc_weekly.log
R --vanilla < scripts/scrape_OWID.R &> log/owid.log
# run AFTER OWID
R --vanilla < scripts/scrape_StatAustria.R &> log/stataustria.log
R --vanilla < scripts/scrape_NYTimes.R &> log/nyt.log
R --vanilla < scripts/scrape_eurostat.R &> log/eurostat.log
R --vanilla < scripts/scrape_coronaDAT_2.R &> log/coronaDAT_2.log
## requires manual update for latest data in all plots
## see README.md
#R --vanilla < scripts/scrape_coronaDAT.R &> log/coronaDAT.log
R --vanilla < scripts/scrape_CDC_fluview.R &> log/cdc.log


pandoc corona_slides.md  --filter pandoc-citeproc -t beamer -o slides.pdf  &> log/slides.log

pandoc -s monitor.md -t slidy --self-contained -o monitor.html; sed -i 's/ü/\&uuml;/g;s/Ü/\&Uuml;/g;s/ä/\&auml;/g;s/Ö/\&Ouml;/g' monitor.html 
