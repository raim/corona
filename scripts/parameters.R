
## global y-axis settings
## loaded from scripts/scrape_*.R
mxc <- 2*4*5000 # cases/1e5
mxd <- mxc *.05/2 # deaths/1e5
mxcw <- 2*mxc/4/5# cases per week
mxdw <- mxcw *.05/2/2 # deaths  per week
