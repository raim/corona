#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cd $SCRIPTPATH/..
pandoc corona_slides.md  --filter pandoc-citeproc -t beamer -o slides.pdf #-s -o slides.tex
