---
title: "The Nosocomial Catastrophe"
subtitle: "Putative Feedbacks during the SARS-CoV-2 Pandemic."
author: Rainer Machne

---

# The Nosocomial Catastrophe: Putative Feedbacks during the SARS-CoV-2 Pandemic.


## The SEIR model 
    
* McKendrick/vFoerster!

## The SEIR model vs. SARS-CoV2/Covid19 
    
* unprecedented data set, wide variety of dynamics, 
* factors: age, pollution, health care spending, seasonality, etc.
* each affects all parameters of the SEIR model!

## Virus Seasonality: social vs. biological, 

* bio:
    - vulnerable lungs, 
    - energy limitation in cold/wet weather,
    - virus interference
* social:
    - complex seasonal dynamics of social behaviour, 
    summer: outdoor/procreation, winter: indoor/family,
    - various implicit measures: stay home & drink tea etc., 
    - sneeze in tissue -> hand -> armpit

-> waves, synchronized with social and biological dynamics

## Local Catastrophes

* Pollution
* Cuts in Health Care spending
    - optimizing the very buffer that was build in on purpose - hybris
    - indices don't reflect this (belgium, UK)


## Nosocomial transmission lines 
    
* flu, SARS-CoV-1, SARS-CoV-2

## The Nosocomial Catastrophe:
    
* "waiting-in-line": feedback? Bergamo, New York, Neukoelln, 
-> two effects: virus-boost in infected, infection of vulnerable
* psychogenic mortality: death in hospitals and care homes, giving-up?

## Virus Evolution: The Virulence-Transmission Trade-Off

* Spread among young during summer: measured first time, is this the
same for Influenza et al.?
* Does the virus evolve in summer to higher transmission and lower virulence?

@Froissart2010: plant viruses,
@BenShachar2018: dengue virus, 
@Blanquart2016: HIV-1 in Uganda, 
@Hector2019: little evidence?

## Self-fulfilling Prophecy?

* Overtreatment: early intubation caused severe lung damage,
* Mildly ill persons travel through city to get tested,
exposed to cold weather and rain, waiting in line instead of
self-treatment at home: virus can proliferate in weakened body,
people get sicker and more infectious,
* Mildly ill persons are treated in the hospital (to isolate),
contributing to infection of staff and other patients,
* Hospital staff is over-worked, short of sleep; weakened immune
system makes them more vulnerable,
* Stress -> increased cortisol -> supressed immune system,
* Old people die from social isolation and panik ("psychogenic
mortality", "giving up"),
* Distancing over summer supresses virus spread in healthy and
less vulnerable (than winter) population, which would allow
development of immunity and evolution of the virus (virulence-transmission
trade-off),
* Full parties and shopping malls prior to hard lock-downs.

* Earlier hospitalization, still at peak of transmission?
(cf. figure in online lecture by drosten)
