downloaded from
https://www.wiko-berlin.de/en/fellows/fellowfinder/detail/2008-moelling-karin/

Karin Mölling, Dr. rer. nat.

Professor of Medical Virology

University of Zurich

Born in 1943 in Meldorf, Dithmarschen, Germany
Studied Physics, Biology, and Biophysics at the University of Kiel, the University of California, Berkeley, the University of Tübingen, and the University of Gießen 
