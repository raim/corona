downloaded from https://www.health.ny.gov/statistics/diseases/covid-19/fatalities_nursing_home_acf.pdf
date: 20200420

via https://www.nytimes.com/2020/04/17/nyregion/new-york-nursing-homes-coronavirus-deaths.html

The data did not include facilities with fewer than five deaths, and it did not include residents who died in hospitals.

At least 14 nursing homes in New York City and its suburbs have recorded more than 25 coronavirus-related deaths, according to new data from the state Health Department that shows the virus’s impact on individual facilities, and five had 40 or more.

The homes that have been hit hardest are the Cobble Hill Health Center, a 364-bed nonprofit facility in Brooklyn, which reported 55 deaths, and the Kings Harbor Multicare Center, a for-profit, 720-bed facility in the Bronx, which had 45.

The 10 homes with the most deaths were all in the city.


