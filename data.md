# GENERAL DATA

* flu data
    - vaccination rates: https://data.oecd.org/healthcare/influenza-vaccination-rates.htm
    
* worl-wide mortality, in deaths/1e3/year

https://data.worldbank.org/indicator/sp.dyn.cdrt.in?end=2018&start=1960&view=chart

NOTE, current global mortality:
7.542 deaths/1e3/year =  (7.542/(1000/1e5))/52 = 14.50385 deaths/1e5/week

* population data
    - USA, 2019: https://www2.census.gov/programs-surveys/popest/datasets/2010-2019/counties/totals/co-est2019-alldata.csv
    - Germany, 2017: https://opendata.arcgis.com/datasets/5dc2fc92850241c3be3d704aa0945d9c_2.geojson
    - Germany, 2018: https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/04-kreise.xlsx?__blob=publicationFile
    - Germany, 2011: https://www.regionalstatistik.de/genesisws/downloader/00/12111-01-01-4_00.csv

* UK mortality
    - https://ukpublichealthdata.shinyapps.io/weekly_deaths/

* UK Lung Deaths
    - part of R package, https://rstudio.github.io/dygraphs/

* Belgium mortality
    - https://epistat.wiv-isp.be/momo/

# COVID-19 DATA COLLECTIONS

* spatially resolved data
    - https://www.google.com/maps/d/viewer?mid=1AKT1bRIRWygoGMZ-lTqWWESm02yQmIdM&ll=23.047021801946904%2C72.68173652895985&z=10

* government measures
    - http://epidemicforecasting.org/containment
    - https://data.humdata.org/dataset/acaps-covid19-government-measures-dataset
    - https://www.notion.so/977d5e5be0434bf996704ec361ad621d
    - https://www.bsg.ox.ac.uk/research/research-projects/coronavirus-government-response-tracker - API or csv, used in graphs at http://inproportion2.talkigy.com/
    

* general COVID-19 dataset clearinghouse

https://asone.ai/polymath/index.php?title=COVID-19_dataset_clearinghouse#Data_sets

* Johns Hopkins git

https://github.com/CSSEGISandData/COVID-19

2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns
Hopkins CSSE

* Austria: https://www.data.gv.at/covid-19/

* NYC data: https://www1.nyc.gov/site/doh/covid/covid-19-data.page#download

* Testing data
    - USA: https://covidtracking.com/data

* Virus genome analysis for the SARS-CoV-2 pandemic

eg. amino acid replacements
http://cov-glue.cvr.gla.ac.uk/#/replacement

* EMBL-EBI sequence data:

https://www.covid19dataportal.org/

* USA County-level Socioeconomic Data

https://github.com/JieYingWu/COVID-19_US_County-level_Summaries

* by age corona data
    - Germany: https://npgeo-corona-npgeo-de.hub.arcgis.com/datasets/dd4580c810204019a7b8eb3e0b329dd6_0/data
    - Canada: https://www150.statcan.gc.ca/t1/tbl1/en/tv.action?pid=1310076701
    - Korea: https://github.com/ThisIsIsaac/Data-Science-for-COVID-19
    - France: https://github.com/opencovid19-fr/data
    - Germany: https://www.kaggle.com/headsortails/covid19-tracking-germany

* health care data
    - Germany: https://npgeo-corona-npgeo-de.hub.arcgis.com/search?groupIds=b686728f969a418c8930cab186dcd7ce
    - USA, Hospital Beds: https://coronavirus-disasterresponse.hub.arcgis.com/datasets/definitivehc::definitive-healthcare-usa-hospital-beds?geometry=162.949%2C-16.820%2C172.090%2C72.123
    - USA, provider practice locations: https://www.arcgis.com/home/webmap/viewer.html?webmap=6afcaeb7549f4390b07224a0be01b3a6

* general demographic info
    - https://www.kaggle.com/headsortails/covid19-tracking-germany

* travel routes
    - Korea: https://github.com/ThisIsIsaac/Data-Science-for-COVID-19

* geo data
    - https://www.kaggle.com/headsortails/covid19-tracking-germany

* mortality
    - shown for several countries, but data not provided: https://www.economist.com/graphic-detail/2020/04/16/tracking-covid-19-excess-deaths-across-countries


## Forums

* https://discourse.data-against-covid.org/c/i-have-data/15


# CLIMATE DATA

* @Ogen2020: Assessing nitrogen dioxide ({NO2}) levels as a
  contributing factor to coronavirus ({COVID}-19) fatality.

The spatial analysis has been conducted on a regional scale and
combined with the number of death cases taken from 66 administrative
regions in Italy, Spain, France and Germany. Results show that out of
the 4443 fatality cases, 3487 (78\%) were in five regions located in
north Italy and central Spain.  Additionally, the same five regions
show the highest NO2 concentrations combined with downwards airflow
which prevent an efficient dispersion of air pollution.

* @Cui2017: Air pollution and case fatality of SARS in the People's
  Republic of China: an ecologic study.

positive association between air pollution and SARS case fatality in
Chinese population by utilizing publicly accessible data on SARS
statistics and air pollution indices. Although ecologic fallacy and
uncontrolled confounding effect might have biased the results, the
possibility of a detrimental effect of air pollution on the prognosis
of SARS patients deserves further investigation.

* @Wu2020bpre: Exposure to air pollution and COVID-19 mortality in the United States

data and code at https://github.com/wxwx1993/PM_COVID

* http://chelsa-climate.org/timeseries/

The CHELSA timeseries data is a monthly dataset of precipitation,
maximum-, minimum-, and mean temperatures at 30 arc sec resolution for
the earths land surface areas. There are seperate files for each month
starting January 1979 – December 2013. For temperatures also air
temperature over the oceans is included.

* http://climexp.knmi.nl/start.cgi?id=someone@somewhere

Please enter the KNMI Climate Explorer, a research tool to investigate
the climate. This web site collects a lot of climate data and analysis
tools. Please verify yourself that the data you use is good enough for
your purpose, and report errors back. In publications the original
data source should be cited, a link to a web page describing the data
is always provided.

Start by selecting a class of climate data from the right-hand menu. After you have selected the time series or fields of interest, you will be able to investigate it, correlate it to other data, and generate derived data from it.

* http://worldweather.wmo.int/en/dataguide.html

The forecast and climatological information of each city are available for downloading at:

https://worldweather.wmo.int/en/json/[City ID]_en.json
where [City ID]is city id of the city

Examples:
Singapore: https://worldweather.wmo.int/en/json/234_en.json
New York City: https://worldweather.wmo.int/en/json/278_en.json


* https://aqicn.org/data-platform/register/

Air Quality Historical Data Platform

eg. Sweden: https://aqicn.org/map/sweden/

* https://aqicn.org/data-platform/covid19/

