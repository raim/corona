---
title: "SARS-CoV-2 Monitor"
subtitle: "A Genetic & Memetic Pandemic."
author: Rainer Machne
date: \today
output:
    beamer_presentation:
    keep_tex:  true
header-includes:
  - '\setlength\itemsep{0em}'
  - \usepackage{caption}
  - \captionsetup[figure]{labelformat=empty}
  - \widowpenalties 1 150
bibliography: corona.bib
link-citations: true
---

# The Unknown.

\scriptsize

\vspace{1cm}

\strut\hfill ![](originalData/RKI_EM_Covid_19_koloriert_Krankheitsseite.jpg){width=40%}

\vspace{-2cm}

Reports that say that something hasn't happened\newline
are always interesting to me,\newline\vspace{-1ex}

because as we know, there are known knowns;\newline
there are things we know we know.\newline\vspace{-1ex}

We also know there are known unknowns;\newline
that is to say we know there are some things we do not know.\newline\vspace{-1ex}

But there are also unknown unknowns -\newline
the ones we don't know we don't know.\newline\vspace{-1ex}

And if one looks throughout the history of our country and other free countries,\newline
it is the latter category that tend
to be the difficult ones.\newline

\vspace{.5cm}

Donald Rumsfeld, Feb 12, 2002, about **the lack of evidence** linking the
government of Iraq with the supply of weapons of mass destruction to
terrorist groups.\newline
\url{en.wikipedia.org/wiki/There_are_known_knowns}


# Different Data Sources: Germany, Johns Hopkins

![](figures/world/Germany_owid.png){height=60%}

\scriptsize

Johns Hopkins Univ. via \url{ourworldindata.org}

\tiny

\url{https://covid.ourworldindata.org/data/owid-covid-data.csv}

# Different Data Sources: Germany, ECDC

![](figures/world/Germany_ecdc_2.png){height=60%}

\scriptsize

European Center for Disease Prevention and Control: time-series data

\tiny

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/}


# Different Data Sources: Germany, cleaned RKI data

![](figures/Germany/Germany_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}, **CORRECT(ish) DATES**

# Pandemic & Isolation: Local Events

![](figures/Germany/Berlin_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}



# Pandemic & Isolation: Local Events

![](figures/Germany/Bayern_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Pandemic & Isolation: Local Events

![](figures/Austria/Bezirke/Landeck_2.png){height=60%}

\scriptsize

European Hub: skiing resort Ischgl

\tiny

\url{github.com/statistikat/coronaAT/}

# Pandemic & Isolation: Local Events

![](figures/world/Iceland_owid.png){height=60%}

\scriptsize

Johns Hopkins Univ. via \url{ourworldindata.org}

\tiny

\url{https://covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic & Isolation: Local Events

![](figures/Germany/Nordrhein.Westfalen/LK.Heinsberg_npgeo.png){height=60%}

\scriptsize

First cluster in Germany: Heinsberg

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# Pandemic & Isolation: Local Events

![](figures/Germany/top_1.png){height=60%}

\scriptsize

Most Affected "Kreis" in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# Pandemic & Isolation: Local Events

![](figures/Germany/Nordrhein.Westfalen/LK.Guetersloh_npgeo.png){height=60%}

\scriptsize

A summer cluster in Germany: slaughterhouse and meat factory Tönnies

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 



# Pandemic & Isolation: Local Events

![](figures/USA/New York/New York_New York City_nyt.png){height=60%}

\scriptsize

New York Times data collection by US counties

\tiny

\url{github.com/nytimes/covid-19-data}

# In Context: Berlin

![](figures/npgeo_berlin_context.png){height=60%}

\scriptsize

Total mortality in Berlin.

\tiny

Figure 43, RKI Influenza Bericht 2018/2019
(doi:http://dx.doi.org/10.25646/6232), digitized with `engauge`,\newline
RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 


# In Context: Germany

![](figures/npgeo_germany_context.png){height=60%}

\scriptsize

Extrapolated from Berlin (3.7e6) to Germany (83.2e6)

\tiny

Figure 43, RKI Influenza Bericht 2018/2019
(doi:http://dx.doi.org/10.25646/6232), digitized with `engauge`,\newline
RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# In Context: European Countries

![](figures/Europe/Europe_mortality_19991231.png){height=60%}

\scriptsize

Total mortality in European countries, normalized to median 2016-2019.

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: Austria

![](figures/Austria/Austria_context_all.png){height=60%}

\scriptsize

Total mortality in Austria.

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Austria

![](figures/Austria/Austria_context_zoom.png){height=60%}

\scriptsize

Total mortality in Austria.

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Austria

![](figures/Austria/Austria_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Tyrol

![](figures/Austria/Tyrol_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Vienna

![](figures/Austria/Vienna_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Belgium

![](figures/Europe/BE_mortality.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Belgium

![](figures/Europe/age/BE_mortality_age.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Belgium

![](figures/Europe/age/BE_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline



# In Context: Spain

![](figures/Europe/age/ES_mortality_age.png){height=60%}

\scriptsize

Total mortality in Spain (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Spain

![](figures/Europe/age/ES_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Spain (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: Sweden

![](figures/Europe/age/SE_mortality_age.png){height=60%}

\scriptsize

Total mortality in Sweden (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Sweden

![](figures/Europe/age/SE_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Sweden (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: UK

![](figures/Europe/age/UK_mortality_age.png){height=60%}

\scriptsize

Total mortality in United Kingdom (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: UK

![](figures/Europe/age/UK_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in United Kingdom (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: California

![](figures/USA/California_cdc_fluview_all.png){height=60%}


\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}

 
# In Context: USA

![](figures/USA/California_cdc_fluview_zoom.png){width=50%} California\newline
![](figures/USA/Arizona_cdc_fluview_zoom.png){width=50%} Arizona\newline
![](figures/USA/New_York_City_cdc_fluview_zoom.png){width=50%} New York City

\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}


# Pandemic: world-wide data 

![](figures/world/North_America_owid.png){height=25%}
![](figures/world/South_America_owid.png){height=25%}

![](figures/world/Europe_owid.png){height=25%}
![](figures/world/Africa_owid.png){height=25%}

![](figures/world/Asia_owid.png){height=25%}
![](figures/world/Oceania_owid.png){height=25%}


\tiny

ECDC: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} 


# Pandemic: Europe - South

![](figures/world/Italy_owid.png){height=25%}
![](figures/world/Greece_owid.png){height=25%}

![](figures/world/Malta_owid.png){height=25%}
![](figures/world/Cyprus_owid.png){height=25%}

![](figures/world/Spain_owid.png){height=25%}
![](figures/world/Portugal_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - North

![](figures/world/United_Kingdom_owid.png){height=25%}
![](figures/world/Ireland_owid.png){height=25%}

![](figures/world/Netherlands_owid.png){height=25%}
![](figures/world/Denmark_owid.png){height=25%}

![](figures/world/Sweden_owid.png){height=25%}
![](figures/world/Norway_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Central

![](figures/world/France_owid.png){height=25%}
![](figures/world/Belgium_owid.png){height=25%}

![](figures/world/Germany_owid.png){height=25%}
![](figures/world/Luxembourg_owid.png){height=25%}

![](figures/world/Austria_owid.png){height=25%}
![](figures/world/Switzerland_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: Europe - Germany - Northwest

![](figures/Germany/Hamburg_npgeo.png){height=25%}
![](figures/Germany/Schleswig.Holstein_npgeo.png){height=25%}

![](figures/Germany/Bremen_npgeo.png){height=25%}
![](figures/Germany/Niedersachsen_npgeo.png){height=25%}

![](figures/Germany/Nordrhein.Westfalen_npgeo.png){height=25%}
![](figures/Germany/Hessen_npgeo.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Pandemic: Europe - Germany - Southwest

![](figures/Germany/Rheinland.Pfalz_npgeo.png){height=25%}
![](figures/Germany/Saarland_npgeo.png){height=25%}

![](figures/Germany/Bayern_npgeo.png){height=25%}
![](figures/Germany/Baden.Wuerttemberg_npgeo.png){height=25%}


\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Pandemic: Europe - Germany - East

![](figures/Germany/Mecklenburg.Vorpommern_npgeo.png){height=25%}
![](figures/Germany/Brandenburg_npgeo.png){height=25%}

![](figures/Germany/Sachsen.Anhalt_npgeo.png){height=25%}
![](figures/Germany/Berlin_npgeo.png){height=25%}

![](figures/Germany/Sachsen_npgeo.png){height=25%}
![](figures/Germany/Thueringen_npgeo.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Pandemic: Europe - Austria - East

![](figures/Austria/Wien.png){height=25%}
![](figures/Austria/Niederoesterreich.png){height=25%}

![](figures/Austria/Burgenland.png){height=25%}
![](figures/Austria/Oberoesterreich.png){height=25%}

![](figures/Austria/Steiermark.png){height=25%}
![](figures/Austria/Salzburg.png){height=25%}

\tiny

Statistik Austria: \url{github.com/statistikat/coronaDAT}

# Pandemic: Europe - Austria - West/South

![](figures/Austria/Kaernten.png){height=25%}\newline
![](figures/Austria/Tirol.png){height=25%}\newline
![](figures/Austria/Vorarlberg.png){height=25%}

\tiny

Statistik Austria: \url{github.com/statistikat/coronaDAT}


# Pandemic: Europe - East

![](figures/world/Poland_owid.png){height=25%}
![](figures/world/Czechia_owid.png){height=25%}

![](figures/world/Slovakia_owid.png){height=25%}
![](figures/world/Hungary_owid.png){height=25%}

![](figures/world/Ukraine_owid.png){height=25%}
![](figures/world/Russia_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - South-East


![](figures/world/Slovenia_owid.png){height=25%}
![](figures/world/Croatia_owid.png){height=25%}

![](figures/world/Serbia_owid.png){height=25%}
![](figures/world/Bosnia_and_Herzegovina_owid.png){height=25%}

![](figures/world/Romania_owid.png){height=25%}
![](figures/world/Bulgaria_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - East, Small Countries


![](figures/world/Estonia_owid.png){height=25%}
![](figures/world/Lithuania_owid.png){height=25%}

![](figures/world/Latvia_owid.png){height=25%}
![](figures/world/Moldova_owid.png){height=25%}

![](figures/world/North_Macedonia_owid.png){height=25%}
![](figures/world/Kosovo_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Small Countries


![](figures/world/Liechtenstein_owid.png){height=25%}
![](figures/world/Monaco_owid.png){height=25%}

![](figures/world/San_Marino_owid.png){height=25%}
![](figures/world/Andorra_owid.png){height=25%}

![](figures/world/Vatican_owid.png){height=25%}
<!--![](figures/world/the_Holy.See..Vatican.City.State_owid.png){height=25%}-->
<!--![](figures/world/Holy_See_owid.png){height=25%}-->
![](figures/world/Gibraltar_ecdc_2.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Middle East 

![](figures/world/Iran_owid.png){height=25%}
![](figures/world/Qatar_owid.png){height=25%}

![](figures/world/United_Arab_Emirates_owid.png){height=25%}
![](figures/world/Lebanon_owid.png){height=25%}

![](figures/world/Turkey_owid.png){height=25%}
![](figures/world/Israel_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: America - North/Central

![](figures/world/Canada_owid.png){height=25%}
![](figures/world/United_States_owid.png){height=25%}

![](figures/world/Mexico_owid.png){height=25%}
![](figures/world/Cuba_owid.png){height=25%}

![](figures/world/Guatemala_owid.png){height=25%}
![](figures/world/Dominican_Republic_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: America - North/Central - USA

![](figures/USA/Florida_nyt.png){height=25%}
![](figures/USA/California_nyt.png){height=25%}
	    
![](figures/USA/Louisiana_nyt.png){height=25%}
![](figures/USA/Massachusetts_nyt.png){height=25%}
	    
![](figures/USA/Arizona_nyt.png){height=25%}
![](figures/USA/New York City_nyt.png){height=25%}


\tiny

\url{github.com/nytimes/covid-19-data}

# Pandemic: America - South - North


![](figures/world/Panama_owid.png){height=25%}
![](figures/world/Venezuela_owid.png){height=25%}


![](figures/world/Colombia_owid.png){height=25%}
![](figures/world/Guyana_owid.png){height=25%}

![](figures/world/Ecuador_owid.png){height=25%}
![](figures/world/Suriname_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: America - South - South

![](figures/world/Peru_owid.png){height=25%}
![](figures/world/Brazil_owid.png){height=25%}

![](figures/world/Chile_owid.png){height=25%}
![](figures/world/Bolivia_owid.png){height=25%}

![](figures/world/Argentina_owid.png){height=25%}
![](figures/world/Uruguay_owid.png){height=25%}



\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: Asia 

![](figures/world/China_owid.png){height=25%}
![](figures/world/Taiwan_owid.png){height=25%}

![](figures/world/Japan_owid.png){height=25%}
![](figures/world/South_Korea_owid.png){height=25%}

![](figures/world/Singapore_owid.png){height=25%}
![](figures/world/India_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Africa - North

![](figures/world/Egypt_owid.png){height=25%}
![](figures/world/Morocco_owid.png){height=25%}

![](figures/world/Mauritania_owid.png){height=25%}
![](figures/world/Libya_owid.png){height=25%}

![](figures/world/Algeria_owid.png){height=25%}
![](figures/world/Tunisia_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Africa - West

![](figures/world/Senegal_owid.png){height=25%}
![](figures/world/Gambia_owid.png){height=25%}


![](figures/world/Mali_owid.png){height=25%}
![](figures/world/Niger_owid.png){height=25%}

![](figures/world/Burkina_Faso_owid.png){height=25%}
![](figures/world/Liberia_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Africa - West


![](figures/world/Chad_owid.png){height=25%}
![](figures/world/Cameroon_owid.png){height=25%}

![](figures/world/Ghana_owid.png){height=25%}
![](figures/world/Nigeria_owid.png){height=25%}

![](figures/world/Gabon_owid.png){height=25%}
![](figures/world/Democratic_Republic_of_Congo_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Africa - East

![](figures/world/Somalia_owid.png){height=25%}
![](figures/world/Djibouti_owid.png){height=25%}

![](figures/world/Ethiopia_owid.png){height=25%}
![](figures/world/Eritrea_owid.png){height=25%}

![](figures/world/Kenya_owid.png){height=25%}
![](figures/world/Uganda_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}



# Pandemic: Africa  - Central

![](figures/world/Burundi_owid.png){height=25%}
![](figures/world/Rwanda_owid.png){height=25%}

![](figures/world/Tanzania_owid.png){height=25%}
![](figures/world/Angola_owid.png){height=25%}

![](figures/world/Zambia_owid.png){height=25%}
![](figures/world/Zimbabwe_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: Africa - South

![](figures/world/Namibia_owid.png){height=25%}
![](figures/world/Mozambique_owid.png){height=25%}

![](figures/world/Botswana_owid.png){height=25%}
![](figures/world/Eswatini_owid.png){height=25%}

![](figures/world/Lesotho_owid.png){height=25%}
![](figures/world/South_Africa_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: Oceania

![](figures/world/Malaysia_owid.png){height=25%}
![](figures/world/Indonesia_owid.png){height=25%}

![](figures/world/Australia_owid.png){height=25%}
![](figures/world/New_Zealand_owid.png){height=25%}

![](figures/world/French_Polynesia_ecdc_2.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Central - Germany

![](figures/world/Europe_owid.png){height=25%}
![](figures/world/Germany_owid.png){height=25%}

![](figures/world/Germany_ecdc_2.png){height=25%}
![](figures/Germany/Germany_npgeo.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/}

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}


# Germany: most deaths/1e5

![](figures/Germany/top_1.png){height=25%}
![](figures/Germany/top_2.png){height=25%}

![](figures/Germany/top_3.png){height=25%}
![](figures/Germany/top_4.png){height=25%}

![](figures/Germany/top_5.png){height=25%}
![](figures/Germany/top_6.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Germany: most new cases 

![](figures/Germany/top_1_last.png){height=25%}
![](figures/Germany/top_2_last.png){height=25%}

![](figures/Germany/top_3_last.png){height=25%}
![](figures/Germany/top_4_last.png){height=25%}

![](figures/Germany/top_5_last.png){height=25%}
![](figures/Germany/top_6_last.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}


# Germany: strongest recent increase 

![](figures/Germany/top_1_rise.png){height=25%}
![](figures/Germany/top_2_rise.png){height=25%}

![](figures/Germany/top_3_rise.png){height=25%}
![](figures/Germany/top_4_rise.png){height=25%}

![](figures/Germany/top_5_rise.png){height=25%}
![](figures/Germany/top_6_rise.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}




# Data sources

Scripts at \url{gitlab.com/raim/corona}:

\scriptsize

* Cases and fatalities - Germany, RKI data at npgeo: `scrape_NPGEO.R`,
* Cases and fatalities - global, \url{ourworldindata.org}: `scrape_OWID.R`.
* Cases and fatalities - global, from ECDC:  `scrape_ECDC_weekly.R`.
* USA mortality data, from CDC Fluview: `scrape_CDC_fluview.R`
* USA SARS-CoV-2 data, from New York Times: `scrape_NYTimes.R`
* Austria Mortality data, Statistik Austria: `scrape_StatAustria.R`
* Austria SARS-CoV-2 data, Statistik Austria: `scrape_coronaDAT_2.R`

\tiny
\url{npgeo-corona-npgeo-de.hub.arcgis.com/} - RKI data by age/county(Kreis) and with correctish dates\newline
\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/} - ECDC\newline
\url{ourworldindata.org/coronavirus-data-explorer} - Our World in Data, Johns Hopkins\newline
\url{gis.cdc.gov/grasp/fluview/mortality.html} - CDC fluview, US mortality\newline
\url{github.com/nytimes/covid-19-data} - New York Times, US SARS-CoV-2\newline
\url{data.statistik.gv.at/web/catalog.jsp} - Austria mortality data\newline
\url{github.com/statistikat/coronaDAT} - Austria SARS-CoV-2 data

# SARS-CoV-2: *Flatten the Curve*.

\scriptsize

* avoid cold & dry air
    - \scriptsize low T and low humidity are key factors in
    influenza transmission, and prelim. evidence
    (and common sense) suggests the same for SARS-CoV(-1)
    - use humidifiers, preferably cold evaporation (Venta)
    or with certified sterilization (eg. Philipps)
* wash hands & face regularly
    - \scriptsize offer more public facilities,
    - enforce at airports by indicator liquid?
* watch yourself: any/which symptoms?
    - \scriptsize stay at home (except perhaps mild rhinovirus:\newline
    it may protect against influenza!).
* watch RKI reports, beware the flu season
    - \scriptsize before/during flu season: avoid large groups,
    - be careful visiting the elderly & infants.
* personal non-expert advice:
    - \scriptsize drink lots of water (wash them down),
    - sterilize your throat, daily: gin tonic (chinin!) & whisky, 
    - breathe through nose (filter the air!),
    - wash your nose with physiological salt solution.

# SARS-CoV-2: *Flatten the Curve* & Fight Back.


\centering

protect infants, the elderly and immuno-compromised,\newline let the
healthy population fight the virus(es)\newline by combined immuno-power\newline
until we find a vaccine


\raggedbottom


