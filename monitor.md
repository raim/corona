---
title: SARS-CoV-2 Monitor
---

# SARS-CoV-2 Monitor: Machne

\centering

![](originalData/Zelle-und-Virus_small.jpg){width=60%}

\tiny
\url{www.derstandard.at/story/2000122673193/was-wir-ueber-die-neue-britische-sars-cov-2-variante}

# Bild Legende

\scriptsize

* \scriptsize Covid19 Fallzahlen:
    - \scriptsize jeweils links: absolute Fallzahlen pro 100.000 Einwohner,
    - jeweils rechts: neue F&auml;lle pro 100.000 Einwohner pro Woche.
    - schwarz: Corona-positiv getestete (**cases/1e5**),
    - rot: Corona-positiv Verstorbene (**deaths/1e5**),
* \scriptsize Sterbedaten:
    - \scriptsize schwarz: Tote pro Woche &uuml;ber die letzten Jahre,
    - rot: Sterbedaten abz&uuml;glich offizieller Covid19 Sterbezahlen.
    - gr&uuml;n: Vorhersage mit `prophet`,
    - blau: Vorhersage mit Fourier Transformation,
* \scriptsize Blaue vertikale gestrichelte Linien auf der x-Achse: Datum der letzten verf&uuml;gbaren Daten,
* \scriptsize Datenquellen: EuroStat, ECDC, Statistik Austria, RKI/NPGEO.


# Covid19 Fallzahlen &Ouml;sterreich, Bezirke.

![](figures/Austria/Bezirke/Bregenz_2.png){width=45%}
![](figures/Austria/Bezirke/Innsbruck.Stadt_2.png){width=45%}

![](figures/Austria/Bezirke/Lienz_2.png){width=45%}
![](figures/Austria/Bezirke/Wien_2.png){width=45%}


# Covid19 Fallzahlen NRW, Kreise.

![](figures/Germany/Nordrhein.Westfalen/SK.Duesseldorf_npgeo.png){width=45%}
![](figures/Germany/Nordrhein.Westfalen/SK.Duisburg_npgeo.png){width=45%}

![](figures/Germany/Nordrhein.Westfalen/SK.Oberhausen_npgeo.png){width=45%}
![](figures/Germany/Nordrhein.Westfalen/SK.Essen_npgeo.png){width=45%}

# Covid19 Fallzahlen Deutschland


![](figures/Germany/Nordrhein.Westfalen_npgeo.png){width=45%}
![](figures/Germany/Bayern_npgeo.png){width=45%}

![](figures/Germany/Berlin_npgeo.png){width=45%}
![](figures/Germany/Sachsen_npgeo.png){width=45%}

# Covid19 Fallzahlen Europa.

![](figures/world/Netherlands_owid.png){width=45%}
![](figures/world/Germany_owid.png){width=45%}

![](figures/world/Switzerland_owid.png){width=45%}
![](figures/world/Austria_owid.png){width=45%}


# Covid19 Fallzahlen Europa.

![](figures/world/Sweden_owid.png){width=45%}
![](figures/world/Denmark_owid.png){width=45%}

![](figures/world/Norway_owid.png){width=45%}
![](figures/world/Finland_owid.png){width=45%}


# Gsund bleiben.

\scriptsize

* H&auml;nde und Gesicht regelm&auml;ssig waschen,
* Viel Wasser trinken,
* Gut L&uuml;ften, trockene Luft meiden,
* Nachm Skifahren oder bei Bedarf:
    - Stamperl Schnaps & Wasser,
    - Husten/Bronchientee mit **S&uuml;ssholzwurzel**,
    - Ingwer/Zitrone, Hollersulze etc.,
* **SKS** - Sei kein Superspreader:
    - **nicht** viel und laut reden,
    - **nicht** rumbrüllen oder laut singen.

# Sterbedaten &Ouml;sterreich

![](figures/Europe/AT_mortality.png){width=100%}

# Sterbedaten Deutschland

![](figures/Europe/DE_mortality.png){width=100%}

# Sterbedaten &Ouml;sterreich

![](figures/Austria/Austria_context_prediction_zoom.png){width=100%}

# Sterbedaten Tirol

![](figures/Austria/Tyrol_context_prediction_zoom.png){width=100%}

# Sterbedaten Vorarlberg

![](figures/Austria/Vorarlberg_context_prediction_zoom.png){width=100%}


\centering

