
* eurostat mortality data: 
    - get in higher geographical resolution,
    - plot as heatmaps with age as y-axis.
    - compare with age-resolved pcr-test incidence rates.
