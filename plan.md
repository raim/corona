
* Theoretical approach:
    - use McKendrick-von-Foerster type PDE model to understand
    epidemics in context of (seasonal) mortality and age-structure,
    - model seasonality by periodic forcing [@Lueck2017],
    - agent-based models to understand local outbreak dynamics, eg.
    R package SpaDES or the NeoLogo framework.
* Data scientific approach:
    - decomposition, clustering and forecasting,
    - scan geographically resolved data for local hot-spots,
    - investigate specific hot-spot transmission lines, enviromental
    correlations (lock-down, weather, indoor climate, pollution, nutrition),
    - compare to previous mortality peaks, interactions/carry-over,
    immune status?
* Biological approach (virology&immunology):
    - map sequence variants to individual hot-spots and transmission lines?
