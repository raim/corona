# SARS-CoV-2 Data Collection

## 1 Clone this git

```
cd <yourpath>
git clone git@gitlab.com:raim/corona.git
```

## 2 Download & Plot Latest Data

### Install Required Libraries and R Packages

Packages in Ubuntu (likely not complete) required for R packages and pandoc:

```
sudo apt install libxml2-dev libudunits2-dev libv8-dev default-jdk default-jre libssl-dev libcurl4-openssl-dev curl  libgdal-dev pandoc pandoc-citeproc 
```

R packages:

```
install.packages(c("xml2","rvest","lubridate","jsonlite","eurostat","prophet"))
```

### Download and Plot Data

Run the R scripts (requires packages `xml2`, `rvest`, `lubridate`,
`jsonlite`, `eurostat`, and `prophet`). If run from the main
directory of the git repo, you should not need to adapt paths. If you
run them interactively, eg. in the `R` console, `emacs+ess` or
`rstudio`, you need to adapt the path in the `setwd` calls.

```
mkdir figures # directory for all generated plots
R --vanilla < scripts/scrape_NPGEO.R 
#R --vanilla < scripts/scrape_RKI.R 
#R --vanilla < scripts/scrape_worldometer.R
#R --vanilla < scripts/scrape_ECDC.R
R --vanilla < scripts/scrape_ECDC_weekly.R
R --vanilla < scripts/scrape_OWID.R
R --vanilla < scripts/scrape_StatAustria.R
R --vanilla < scripts/scrape_NYTimes.R
R --vanilla < scripts/scrape_CDC_fluview.R
R --vanilla < scripts/scrape_eurostat.R
R --vanilla < scripts/scrape_coronaDAT_2.R
```

The scripts download and add latest data to `downloadedData` and
produce figures in the `figures` directory.

The order of the above matters: script `scrape_CDC_fluview.R` requires
data stored in `downloadedData` by `scrape_NYTimes.R`.
Some scripts require manually downloaded data in folder `originalData`.
Try to update the repo (`git pull`) to obtain potentially updated data
sets.

### Update CDC Data Manually

The USA mortality data require manual download of updated data
from \url{https://gis.cdc.gov/grasp/fluview/mortality.html}.
Go the the website, click "Downloads" and select "Custom Data",
select "Surveillance Area: State" and click "Season: select all".
Save the file State_Custom_Data.csv in the folder `originalData`.
Re-run `scrape_CDC_fluview.R` to use the new data.


## 3 Compile Slides

with `pandoc`

```
pandoc corona_slides.md  --filter pandoc-citeproc -t beamer -o slides.pdf 
```

You may have to remode the slide for "Landeck" if you didn't also
run the script for Austria above.

## 4 Set up a cronjob

To download new data and update plots regularly you can set cronjobs
to run regularly run all of the above, e.g., with the bash script
`scripts/runscripts.sh`.

Example entry for `crontab -e`:

```
30 2,8,14,20 * * * <yourpath>/corona/scripts/runscripts.sh
```
