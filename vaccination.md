# 20210115, https://legemiddelverket.no/nyheter/covid-19-vaccination-associated-with-deaths-in-elderly-people-who-are-frail

23 deaths associated with covid-19 vaccination of which 13 have been assessed. Common adverse reactions may have contributed to a severe course in elderly people who are frail. 

# https://orf.at/stories/3194840/

Auch in Österreich wird am Sonntag die erste Person geimpft. Konkret handelt es sich um fünf Personen, die alle über 80 Jahre alt sind und eine Vorerkrankung haben. Die Impfung wird ab 9.00 Uhr in Wien stattfinden – durchgeführt von Ursula Wiedermann-Schmidt, der Vorsitzenden der österreichischen Impfkommission, und Thomas Szekeres, dem Präsidenten der Ärztekammer
