
# Databases of Government Measurements

* https://www.who.int/emergencies/diseases/novel-coronavirus-2019/phsm

collects data from several

* https://supertracker.spi.ox.ac.uk/

Oxford Supertracker

* https://www.bsg.ox.ac.uk/research/research-projects/covid-19-government-response-tracker

* https://www.coronanet-project.org/

Citation: Cheng, Cindy, Joan Barceló, Allison Hartnett, Robert Kubinec, and Luca Messerschmidt. 2020. COVID-19 Government Response Event Dataset (CoronaNet v1.0). Nature Human Behaviour (2020). https://doi.org/10.1038/s41562-020-0909-7


* https://covid19-interventions.com/

R Codes for exploring the CCCSL dataset are available at: https://doi.org/10.5281/zenodo.3949808 and https://github.com/amel-github/CCCSL-Codes 

Desvars-Larrive A., Ahne V., Álvarez S., Bartoszek M., Berishaj D., Bulska D., Chakraborty A., Chen E., Chen X., Cserjan D., Dervic A., Dervic E., Di Natale A., Ferreira M.R., Flores Tames E., Garcia D., Garncarek Z., Gliga D.S., Gooriah L., Grzymała-Moszczyńska J., Jurczak A., Haberfellner S., Hadziavdic L., Haug N., Holder S., Korbel J., Lasser J., Lederhilger D., Niederkrotenthaler T., Pacheco A., Pocasangre-Orellana X.M., Reddish J., Reisch V., Roux A., Sorger J., Stangl J., Stoeger L., Takriti H., Ten A., Vierlinger R., Thurner S. CCCSL: Complexity Science Hub Covid-19 Control Strategies List (2020). Version 2.0. https://github.com/amel-github/covid19-interventionmeasures

 Desvars-Larrive, A., Dervic, E., Haug, N. et al.
A structured open dataset of government interventions in response to COVID-19. Sci Data 7, 285 (2020). https://doi.org/10.1038/s41597-020-00609-9

