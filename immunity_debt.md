# 20210801, https://www.nytimes.com/2021/08/01/world/delta-variant-kids-infections.html

In addition to Covid, more children are getting a respiratory virus more commonly seen in winter.

Cases of R.S.V. have risen gradually since early June, with an even greater spike in the past month, according to data from the Centers for Disease Control and Prevention. The illness, which can cause symptoms that include a runny nose, coughing, sneezing and fever, normally begins to spread in the fall, making this summer spike unusual.

In a series of posts on Twitter, Dr. Heather Haq, a pediatrician at Texas Children’s Hospital in Houston, described an increase in both coronavirus and R.S.V. hospitalizations. 

“After many months of zero or few pediatric Covid cases, we are seeing infants, children and teens with Covid pouring back into the hospital, more and more each day,” she wrote, adding that patients have ranged in age from 2 weeks to 17 years old, including some with Covid pneumonias.

“We are on the front end of a huge Covid surge,” wrote Dr. Haq, who could not be reached for comment on Sunday. “We are now having winter-level patient volumes of acutely ill infants/toddlers with R.S.V., and I worry that we will run out of beds and staff to handle the surge upon surge.”

R.S.V. cases in Texas began to increase in early June and appeared to peak in mid-July, according to data from the state’s health department.

There has been a similar spike in Florida, where infections “were above those seen at this time in past years,” according to a surveillance report.

In Louisiana, where cases have jumped 244 percent in the past two weeks, Our Lady of the Lake Children’s Hospital in Baton Rouge was nearing its capacity on Friday, CNN reported.

In Oklahoma, which has also had a spike in R.S.V. cases, beds are becoming scarce at hospitals.

Dr. Cameron Mantor, the chief medical officer for Oklahoma Children’s Hospital at OU Health, told The Oklahoman that in the past two months R.S.V. cases in the state had been “exponentially off the charts.”

“R.S.V. is a real issue right now,” he told the newspaper. “What is going to happen if we do have a surge in pediatric Covid cases?”

The rise comes as new coronavirus infections have risen 148 percent in the United States in the past two weeks and hospitalizations have increased 73 percent, according to New York Times data. The surge of coronavirus infections has been largely attributed to the highly contagious Delta variant and to low vaccination rates in some states.

Surges in R.S.V. infections have also been reported in places like New Zealand, where it is currently winter. Experts there say that children may be more vulnerable than usual to seasonal viruses and infections because they were underexposed to germs during lockdowns early in the pandemic.

#20210701, https://www.stuff.co.nz/national/politics/local-democracy-reporting/300346815/auckland-emergency-departments-battling-winter-virus-outbreak?rm=a

A surge in viral winter illnesses is creating havoc in Auckland emergency departments – with concerns on the frontline it’ll only get busier.

Public hospitals and primary healthcare providers in the Auckland region have been overwhelmed by patients with viruses, including RSV (Respiratory Syncytial Virus).

The virus causes infections of the lungs and respiratory tract. For healthy adults and older children, symptoms are often mild and are very similar to the common cold. However, for infants the virus can lead to more serious illnesses, including bronchiolitis and pneumonia.

Counties Manukau Health confirmed its hospital’s emergency department has been under pressure, with 415 people coming through the doors on Monday alone. The usual daily number is around 300.

# 20210612, https://www.forbes.com/sites/brucelee/2021/06/12/cdc-warning-rsv-is-spreading-in-southern-us-why-this-is-unusual-for-this-respiratory-virus/

Bruce Y. Lee

CDC Warning: RSV Is Spreading In Southern U.S., Why This Is Unusual For Respiratory Synctial Virus

On Thursday, the Centers for Disease Control and Prevention (CDC) issued health advisory about about a familiar foe: RSV, which is short for the respiratory syncytial virus. This virus seems to be now spreading throughout various parts of the Southern U.S. The CDC is calling this increased “interseasonal” RSV activity, because June ain’t usually RSV season. Instead, RSV activity typically tends to pick up during the Fall and Winter months and then fall in the Spring, similar to what cold and flu viruses do. 

over the past two months or so, NREVSS has shown an increased number of RSV cases, particularly in states that comprise the U.S. Health and Human Services (HHS) Region 4 (Alabama, Florida, Georgia, Kentucky, Mississippi, North Carolina, South Carolina, and Tennessee) and Region 6 (Arkansas, Louisiana, New Mexico, Oklahoma, and Texas). 

Each year, RSV is responsible for an average of 58,000 hospitalizations and 100 to 500 deaths among children younger than 5 years of age and 177,000 hospitalizations with 14,000 deaths among adults 65 years or older. 




# 20210707, https://www.japantimes.co.jp/news/2021/07/07/national/rsv-unseasonable-infections-japan/

Experts in Japan warn of unseasonable spread of cold-like virus

A cold virus called the respiratory syncytial virus (RSV) that causes inflammation of the respiratory tract in infants, which remained dormant last year, could soon see an unusual uptick in Japan.

RSV mainly spreads via respiratory droplets when a person coughs or sneezes, as well as through direct contact with a contaminated surface — somewhat similar transmission routes to the coronavirus. Unlike that virus, however, there are no vaccines available against RSV.

RSV usually causes cold-like symptoms, including fever and a severe cough, and most people recover in a week or two, but it can lead to more serious complications, especially for infants, older adults and those with immunodeficiency or congenital heart disease.

The virus typically spreads during fall, winter and spring. But according to the National Institute of Infectious Diseases, confirmed RSV infections have begun to emerge outside the peak period. The coronavirus outbreak last year somehow kept RSV at bay, but it has now begun to reemerge just as national COVID-19 cases remain relatively flat.


# 20210628, https://www.20min.ch/story/ungewoehnlich-viele-atemwegsinfekte-bei-kindern-und-babys-484718726938

Viele Säuglinge und Kleinkinder infizieren sich momentan mit RS-Viren. Eine derartige Häufung von Atemwegsinfekten im Sommer sei sehr ungewöhnlich, sagen Experten.

Es sei davon auszugehen, dass die Immunität in der gesamten Bevölkerung gegen diese Infekte abgenommen hat, so Traudel Saurenmann, Chefärztin Klinik für Kinder- und Jugendmedizin am Kantonsspital Winterthur

# 20210708, https://www.theguardian.com/world/2021/jul/08/new-zealand-children-falling-ill-in-high-numbers-due-to-covid-immunity-debt

Doctors say children haven’t been exposed to range of bugs due to lockdowns, distancing and sanitiser and their immune systems are suffering

Wellington has 46 children currently hospitalised for respiratory illnesses including respiratory syncytial virus, or RSV. A number are infants, and many are on oxygen. Other hospitals are also experiencing a rise in cases that are straining their resources – with some delaying surgeries or converting playrooms into clinical space.

The “immunity debt” phenomenon occurs because measures like lockdowns, hand-washing, social distancing and masks are not only effective at controlling Covid-19. They also suppress the spread of other illnesses that transmit in a similar way, including the flu, common cold, and lesser-known respiratory illnesses like RSA. In New Zealand, lockdowns last winter led to a 99.9% reduction in flu cases and a 98% reduction in RSV - and near-eliminated the spike of excess deaths New Zealand usually experiences during winter.

...


New Zealand has reported nearly 1,000 RSV cases in the past five weeks, according to the Institute of Environmental Science and Research. The usual average is 1,743 over the full 29-week winter season. Australia is also experiencing a surge, with overcrowded Victoria hospitals also hit by unusually high rates of RSV.

# 20210721, https://nltimes.nl/2021/07/21/dutch-hospitals-full-kids-sick-rs-virus

The RS virus is spreading so fast among children in the Netherlands that all beds in Dutch hospitals' pediatric intensive care units were occupied on Tuesday. At least one young patient had to be transferred to a hospital in Antwerp, AD reports.

The RS outbreak started a few weeks ago in Rotterdam, and quickly spread to Amsterdam and surrounds, the Nijmegen region, and Limburg. The virus is particularly dangerous for young children, who can develop a serious respiratory infection. Some have to be hospitalized for oxygen, fluids or tube feeding. There are now seriously ill children all over the country and all 90 pediatric ICU beds in the country were occupied on Tuesday.

...

Several hospitals, including the Emma Children's Hospital, had to cancel planned surgeries. "Normally wee see these crowds in winter. I'm holding my breath over the number of corona infections, because soon we will also have nurses who have to be quarantined," Van Goudoever said.

# @Cohen2021: Pediatric Infectious Disease Group (GPIP) position paper on the immune debt of the COVID-19 pandemic in childhood, how can we fill the immunity gap? 

... The lack of immune stimulation due to the reduced circulation of microbial agents and to the related reduced vaccine uptake induced an "immunity debt" which could have negative consequences when the pandemic is under control and NPIs are lifted. The longer these periods of "viral or bacterial low-exposure" are, the greater the likelihood of future epidemics. This is due to a growing proportion of "susceptible" people and a declined herd immunity in the population. The observed delay in vaccination program without effective catch-up and the decrease in viral and bacterial exposures lead to a rebound risk of vaccine-preventable diseases. With a vaccination schedule that does not include vaccines against rotavirus, varicella, and serogroup B and ACYW Neisseria meningitidis, France could become more vulnerable to some of these rebound effects. 


