
## contra

* @Behnood2021: children and young people, no difference to control group,

## pro

* @Pretorius2021: *Persistent clotting protein pathology in Long
  COVID/Post-Acute Sequelae of COVID-19 (PASC) is accompanied by
  increased levels of antiplasmin*

## unclear

* @Arjun2022pre: *one of the strongest predictors of Long COVID was
  the severity of COVID-19 disease and hospital admission.*, *An
  observational paradox in our study was that the participants who
  took two doses of COVID-19 vaccination had higher odds of developing
  Long COVID.*
