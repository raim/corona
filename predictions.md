# Catastrophic Predictions based on Exponential Models

* 20200319, Stellungnahme2020Corona_DGEpi-20200319.pdf

Stellungnahme der Deutschen Gesellschaft für Epidemiologie (DGEpi) zur
Verbreitung des neuen Coronavirus (SARS-CoV-2).

So gibt es zum Beispiel an Tag 50 bei einer Reproduktionszahl von 2,5
5.687.270 infizierte Personen (Panel A), bei einer Reproduktionszahl
von 2 würden an Tag 100 1.140.233 Personen intensivpflichtig sein.

* Executive-Summary-Covid19.pdf

Stellungnahme zur COVID19 Krise, 30.3.2019
Executive Summary

Mathias Beiglböck (Uni Wien), Philipp Grohs (Uni Wien), Joachim
Hermisson (Uni Wien, Max Perutz Labs), Magnus Nordborg (ÖAW), Walter
Schachermayer (Uni Wien). Mit Unterstützung der Rektoren Heinz Engl
(Uni Wien) und Markus Müller (Med Uni)

Wenn es nicht gelingt, rasch den Faktor R​ 0​ unter den Wert von 1 zu
drücken, sind in Österreich ​ zehntausende zusätzliche Tote und ein
Zusammenbruch des Gesundheitssystems​ zu erwarten.

Graphiken:
* R0 ~ 0.9: ca. 6000 Todesfaelle, Dez 2020
* täglicher Zuwachs 14%: ca. 95000 Todesfaelle, Dez 2020
* täglicher Zuwachs 30%: ca. 130000 Todesfaelle, Dez 2020


* 20200415, @Gardner2020pre

the current Swedish public-health strategy will result in a peak
intensive-care load in May that exceeds pre-pandemic capacity by over
40-fold, with a median mortality of 96,000 (95% CI 52,000 to 183,000).