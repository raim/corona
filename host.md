# Host Susceptability to Respiratory Viruses

seasonality, indoor environment, microbiome and bacterial
co-infections

## Seasonality, Temperature & Humidity

* Historical data on seasonal influenza

incl. russian flu

http://www.iayork.com/MysteryRays/index.php?s=influenza

* Low ambient humidity impairs barrier function and innate resistance
  against influenza infection

https://www.pnas.org/content/116/22/10905

Exposure of mice to low humidity conditions rendered them more
susceptible to influenza disease. Mice housed in dry air had impaired
mucociliary clearance, innate antiviral defense, and tissue repair
function. Moreover, mice exposed to dry air were more susceptible to
disease mediated by inflammasome caspases. Our study provides
mechanistic insights for the seasonality of the influenza virus
epidemics, whereby inhalation of dry air compromises the host’s
ability to restrict influenza virus infection.

* Indoor environmental factors and acute respiratory illness in a
  prospective cohort of community-dwelling older adults

https://academic.oup.com/jid/article-abstract/doi/10.1093/infdis/jiaa188/5820877

In total, 168 episodes of ARI were reported with an average risk of
36.8% per year. We observed a negative association of ARI with indoor
AH up to five lag days in cool season, with a 6-day cumulative ER
estimate of -9.0% (95% confidence interval: -15.9%, -1.5%). Negative
associations between household temperature or RH and ARI were less
consistent across warm and cool seasons.  Discussion

Lower indoor AH in household was associated with a higher risk of ARI
in the community dwelling older adults in Hong Kong during cold
season.

* 2019 Novel Coronavirus (COVID-19) Pandemic: Built Environment
  Considerations To Reduce Transmission

https://msystems.asm.org/content/5/2/e00245-20

In this paper, we synthesize this microbiology of the BE research and
the known information about SARS-CoV-2 to provide actionable and
achievable guidance to BE decision makers, building operators, and all
indoor occupants attempting to minimize infectious disease
transmission through environmentally mediated pathways. We believe
this information is useful to corporate and public administrators and
individuals responsible for building operations and environmental
services in their decision-making process about the degree and
duration of social-distancing measures during viral epidemics and
pandemics.

NOTE: FIG 2 Conceptualization of SARS-CoV-2 deposition. (a

NOTE: FIG 3 Spatial connectivity, highlighting betweenness and
connectance of common room and door configurations. (a)

* Restricted transmission of 2019-nCoV/SARS-CoV-2 in human body due to
  meteorological variables dependent immune system

https://s3.amazonaws.com/academia.edu.documents/62950810/COVID_19.pdf

OSR Journal Of Pharmacy And Biological Sciences
(IOSR-JPBS)e-ISSN:2278-3008, p-ISSN:2319-7676. Volume 15, Issue 2
Ser. III (Mar –Apr 2020), PP 19-23www.Iosrjournals.Org


The collected data from 57 countries and more than 600000 people
around the world and its corresponding graph gives a very clear trend
about the 2019-nCoV/SARS-CoV-2 or most commonly novel coronavirus
infection transmission rate. The analysis represents that there will
be gradual decrease of infection transmission rate with the natural
hike of temperature.In this case may be Stromal interaction molecule
1(STIM1) is activated moderately with high degree of temperature.


* High Temperature and High Humidity Reduce the Transmission of
  COVID-19

https://arxiv.org/abs/2003.05003


One-degree Celsius increase in temperature and one percent increase in
relative humidity lower R by 0.0225 and 0.0158, respectively. This
result is consistent with the fact that the high temperature and high
humidity reduce the transmission of influenza and SARS. It indicates
that the arrival of summer and rainy season in the northern hemisphere
can effectively reduce the transmission of the COVID-19. We also
developed a website to provide R of major cities around the world
according to their daily temperature and relative humidity:
http://covid19-report.com/#/r-value

* Humidity-Dependent Decay of Viruses, but Not Bacteria, in Aerosols
  and Droplets Follows Disinfection Kinetics

https://pubs.acs.org/doi/abs/10.1021/acs.est.9b04959#

Viruses survived well at RHs lower than 33% and at 100%, whereas their
viability was reduced at intermediate RHs. We then explored the
evaporation rate of droplets consisting of culture media and the
resulting changes in solute concentrations over time; as water
evaporates from the droplets, solutes such as sodium chloride in the
media become more concentrated. Based on the results, we suggest that
inactivation of bacteria is influenced by osmotic pressure resulting
from elevated concentrations of salts as droplets evaporate. We
propose that the inactivation of viruses is governed by the cumulative
dose of solutes or the product of concentration and time, as in
disinfection kinetics. These findings emphasize that evaporation
kinetics play a role in modulating the survival of microorganisms in
droplets.




## Immunology, Microbiome & Bacterial Coinfection

* The respiratory microbiome and susceptibility to influenza virus infection

https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0207898

Using boosting and linear mixed effects models, we found associations
between the nose/throat microbiota and influenza also existed at the
taxa level, specifically with the relative abundance of
Alloprevotella, Prevotella, and Bacteroides oligotypes.

Two oligotypes, Alloprevotella sp. and Prevotella histicola / sp. /
veroralis / fusca / scopos / melaninogenica, were positively
associated with influenza virus infection (S4 Table). One oligotype
was negatively associated with influenza virus infection. Although
unclassified in the HOMD database, a BLAST search using the GenBank
database (https://www.ncbi.nlm.nih.gov/genbank/) classified the
oligotype as Bacteroides vulgatus. All three oligotypes were rare in
all NOP CSTs (S2 Table) and in the total community composition (range:
0.01–0.38%).

NOTE: Prevotella

Ley, R.E. 2016. “Gut Microbiota in 2015: Prevotella in the Gut: Choose Carefully.” Nat Rev Gastroenterol Hepatol
13 (2): 69–70. doi:10.1038/nrgastro.2016.4.

Sandeep Chakraborty at pubpeer: Prevotella (gastric tract bacterium, associated
with chronic inflammatory disease (Ley 2016)) sequences found in unusual
quantities in Wuhan, and SARS:16S reads in Wuhan, Hong Kong and Wisconsin
patient samples;
pubpeer.com/publications/884D5017B8D48D5BA2466AC323F91B


* Differential Susceptibilities of Human Lung Primary Cells to H1N1
  Influenza Viruses

https://jvi.asm.org/content/89/23/11935

Human alveolar epithelial cells (AECs) and alveolar macrophages (AMs)
are the first lines of lung defense. ... Compared to A/California04/09
(CA04), A/New York/1682/09 (NY1682) is more infectious and causes more
epithelial barrier injury, although it stimulates less cytokine
production.



* Inflammation as a Modulator of Host Susceptibility to Pulmonary
  Influenza, Pneumococcal, and Co-Infections

https://www.frontiersin.org/articles/10.3389/fimmu.2020.00105/full

Morbidity and mortality from these infections is increased in
populations that include the elderly, infants, and individuals with
genetic disorders such as Down syndrome. Immune senescence, concurrent
infections, and other immune alterations occur in these susceptible
populations, but the underlying mechanisms that dictate increased
susceptibility to lung infections are not fully defined. Here, we
review unique features of the lung as a mucosal epithelial tissue and
aspects of inflammatory and immune responses in model pulmonary
infections and co-infections by influenza virus and Streptococcus
pneumoniae.

In these models, lung inflammatory responses are a double-edged sword:
recruitment of immune effectors is essential to eliminate bacteria and
virus-infected cells, but inflammatory cytokines drive changes in the
lung conducive to increased pathogen replication. Excessive
accumulation of inflammatory cells also hinders lung function,
possibly causing death of the host. Some animal studies have found
that targeting host modulators of lung inflammatory responses has
therapeutic or prophylactic effects in these infection and
co-infection models. However, conflicting results from other studies
suggest microbiota, sequence of colonization, or other unappreciated
aspects of lung biology also play important roles in the outcome of
infections. Regardless, a predisposition to excessive or aberrant
inflammatory responses occurs in susceptible human populations.


* Allergic inflammation alters the lung microbiome and hinders
  synergistic co-infection with H1N1 influenza virus and Streptococcus
  pneumoniae in C57BL/6 mice

https://www.nature.com/articles/s41598-019-55712-8

Our previous work, together with epidemiologic findings that
asthmatics were less likely to suffer from severe influenza during the
2009 pandemic, suggest that additional complications of influenza such
as increased susceptibility to bacterial superinfection, may be
mitigated in allergic hosts.

To test this hypothesis, we developed a murine model of
‘triple-disease’ in which mice rendered allergic to Aspergillus
fumigatus were co-infected with influenza A virus and Streptococcus
pneumoniae seven days apart. Significant alterations to known
synergistic effects of co-infection were noted in the allergic mice
including reduced morbidity and mortality, bacterial burden,
maintenance of alveolar macrophages, and reduced lung inflammation and
damage.

The lung microbiome of allergic mice differed from that of
non-allergic mice during co-infection and antibiotic-induced
perturbation to the microbiome rendered allergic animals susceptible
to severe morbidity. Our data suggest that responses to co-infection
in allergic hosts likely depends on the immune and microbiome states
and that antibiotics should be used with caution in individuals with
underlying chronic lung disease.

* Predominant Role of Bacterial Pneumonia as a Cause of Death in
  Pandemic Influenza: Implications for Pandemic Influenza Preparedness

https://academic.oup.com/jid/article/198/7/962/2192118

The postmortem samples we examined from people who died of influenza
during 1918–1919 uniformly exhibited severe changes indicative of
bacterial pneumonia. Bacteriologic and histopathologic results from
published autopsy series clearly and consistently implicated secondary
bacterial pneumonia caused by common upper respiratory-tract bacteria
in most influenza fatalities.

The majority of deaths in the 1918–1919 influenza pandemic likely
resulted directly from secondary bacterial pneumonia caused by common
upper respiratory-tract bacteria. Less substantial data from the
subsequent 1957 and 1968 pandemics are consistent with these
findings. If severe pandemic influenza is largely a problem of
viral-bacterial copathogenesis, pandemic planning needs to go beyond
addressing the viral cause alone (e.g., influenza vaccines and
antiviral drugs). Prevention, diagnosis, prophylaxis, and treatment of
secondary bacterial pneumonia, as well as stockpiling of antibiotics
and bacterial vaccines, should also be high priorities for pandemic
planning.


* The role of lung development in the age-related susceptibility of
  ferrets to influenza virus.

https://europepmc.org/article/med/6487534

the lungs of newborn ferrets possess a greater proportion of ciliated
epithelium-lined airway in comparison with the lungs of suckling and
adult ferrets. This situation might also contribute to the increased
susceptibility of the lower respiratory tract although the
difficulties of assessing this influence precisely are discussed. In
addition, the occlusion of the narrower airways of the immature lung
in the infected newborn ferret contributes to the increased
respiratory complications.


* Two waves of pro-inflammatory factors are released during the
  influenza A virus (IAV)-driven pulmonary immunopathogenesis#

https://journals.plos.org/plospathogens/article?id=10.1371/journal.ppat.1008334

After IAVs spread to the lung, extensive pro-inflammatory cytokines
and chemokines are released, which largely determine the outcome of
infection. Using a single-cell RNA sequencing (scRNA-seq) assay ...
of more than 16,000 immune cells in the pulmonary tissue of infected
mice, and demonstrated that two waves of pro-inflammatory factors were
released. A group of IAV-infected PD-L1+ neutrophils were the major
contributor to the first wave at an earlier stage (day 1–3 post
infection). Notably, at a later stage (day 7 post infection) when IAV
was hardly detected in the immune cells, a group of platelet factor
4-positive (Pf4+)-macrophages generated another wave of
pro-inflammatory factors, which were probably the precursors of
alveolar macrophages (AMs).

* Single-cell analysis of upper airway cells reveals host-viral
  dynamics in influenza infected adults

https://www.biorxiv.org/content/10.1101/2020.04.15.042978v1.abstract

single-cell RNA sequencing (scRNA-Seq) on cells from freshly collected
nasal washes from healthy human donors and donors diagnosed with acute
influenza during the 2017-18 season.

previously uncharacterized goblet cell population, specific to
infected individuals, with high expression of MHC class II
genes. Furthermore, leveraging scRNA-Seq reads, we obtained deep viral
genome coverage and developed a model to rigorously identify infected
cells that detected influenza infection in all epithelial cell types
and even some immune cells. Our data revealed that each donor was
infected by a unique influenza variant and that each variant was
separated by at least one unique non-synonymous difference.

NOTE: infected immune cells!

