
Public media reports on Coronavirus outbreaks

## Wuhan 2019 Military Games

* 20200526 - https://www.kreiszeitung.de/lokales/oldenburg/wildeshausen-ort49926/reporter-nach-besuch-wuhan-positiv-corona-antikoerper-getestet-13777099.html

Der 58-jährige Wildeshauser Torsten Timm war Ende Oktober als Videoberichterstatter bei den Weltsoldatenspielen in Wuhan in China. Er und ein Teamkollege hatten sich dort eine Erkältung eingefangen. Beim Kollegen wurden später Antikörper gegen das Coronavirus nachgewiesen. Daraufhin ließ sich auch Timm testen.

Wildeshausen – Wuhan – da war doch was? In der Millionenmetropole der chinesischen Provinz Hubei soll im Dezember das Coronavirus als Auslöser der Atemwegserkrankung Covid-19 aufgetreten sein. Genau dort fanden jedoch bereits vom 18. bis 27. Oktober die Weltsoldatenspiele statt. Sie könnten Berichten zufolge ein massiver Corona-Verteiler gewesen sein. Einige Indizien sprechen laut dem Sportinformationsdienst (SID) dafür. Sportler aus verschiedenen Nationen, die erkrankt waren, hätten sich gemeldet.

Auch der Wildeshauser Torsten Timm war dabei. Der 58-Jährige war bis zum 31. März 2019 als Bundeswehrsoldat im aktiven Dienst in Garlstedt und nahm im Rahmen einer Wehrübung als Videofilmer an dem internationalen Event teil. Bei der Erinnerung an die Zeit vor Ort und danach stutzte er vor einigen Tagen und erzählte: „Ich hatte mir vor dem Rückflug am 28. Oktober eine Erkältung eingefangen, die ungewöhnlich lange dauerte.“ Bis kurz vor Weihnachten habe er damit zu tun gehabt – also fast zwei Monate – was sonst bei ihm nie der Fall sei.

„Das gibt einem zu denken“, so Timm, der daraufhin beschloss, zum Arzt zu gehen. „Ich fand es interessant zu wissen, ob ich mir damals, als noch niemand darüber geredet hat, eine Infektion mit dem Coronavirus eingefangen habe“, sagte er. Bestärkt wurde er bei seinem Plan, einen Mediziner zu konsultieren, als er kurz darauf erfuhr, dass ein Teammitglied aus Wuhan den Befund einer überstandenen Infektion erhalten hatte – wann auch immer diese stattgefunden hat. Vielleicht in China.

„Wir haben während der Militärweltspiele mit sechs Leuten in einer Wohnung gelebt“, so Timm. „Ein Kollege hatte gegen Ende der Veranstaltung eine schwere Erkältung, und ich vermutete, dass ich mich bei ihm angesteckt habe. Mich hat es auf jeden Fall kurz darauf erwischt.“

Von einer möglichen Infektion mit dem Coronavirus konnte er damals nichts wissen. In seinem Umfeld in Deutschland ist Timms Angaben zufolge bislang auch niemand positiv auf Covid-19 getestet worden. Am Montag erhielt er dann das Ergebnis: Beim ihm sind keine Antikörper nachzuweisen. Mit hoher Sicherheit ist Timm also bislang nicht mit dem Coronavirus infiziert gewesen.

Wettbewerb in Wuhan ein Corona-Superverbreiter?

Was andere Personen betrifft, gibt es einige Unklarheiten: Mehrere Indizien nähren laut dem SID die Theorie, dass die Armee-Weltspiele mit fast 10.000 Athleten aus 110 Ländern sowie vielen weiteren Delegationsteilnehmern und 230.000 freiwilligen Helfern ein sogenannter Corona-Superverbreiter gewesen ist. Womöglich sogar der Erste. So wird Italiens Fecht-Olympiasieger Matteo Tagliariol in einem Bericht mit den Worten zitiert: „Als wir in Wuhan eingetroffen sind, sind wir alle erkrankt. Alle sechs Personen in meiner Wohnung waren krank, auch viele Athleten anderer Delegationen.“ Er habe schwer gehustet, nach der Rückkehr sehr hohes Fieber bekommen und kaum atmen können. „Auch Antibiotika halfen nicht“, so der Sportler.

Geschah der Coronaausbruch in China deutlich früher?

Zwar wird der erste offizielle Coronafall weiterhin auf Anfang oder Ende Dezember datiert, doch Studien legen einen früheren Ausbruch nahe. Ein deutsch-britisches Forscherteam hat laut Süddeutscher Zeitung errechnet, dass dieser mit 95-prozentiger Wahrscheinlichkeit zwischen dem 13. September und 7. Dezember stattgefunden hat. Laut der französischen Sporttageszeitung L’Equipe haben sich die frühere Fünfkampf-Weltmeisterin Elodie Clouvel („Wir waren alle krank“) und ihr Kollege Valentin Belaud, Weltmeister 2019, wahrscheinlich ebenfalls bei den Spielen in Wuhan infiziert. Weitere Fälle werden aus Italien gemeldet, in Luxemburg gibt es zumindest einen Verdacht.

Und in Deutschland? „Im Nachhinein haben wir natürlich schon gescherzt: Wuhan, da waren wir doch, das kannte vorher ja keiner“, sagte Luftpistolenschützin Sandra Reitz aus Regensburg dem SID. Reitz hat bei den Militärspielen Gold gewonnen, Außergewöhnliches hat sie unter den 243 deutschen Sportlern und insgesamt nicht bemerkt: „Es gab auch in unserem Umfeld verschiedene Erkrankungen. Das ist aber nichts Verwunderliches, das war eigentlich immer so.“

Laut Auskunft des deutschen Delegationsleiters Christian Lützkendorf lag die Zahl der Erkrankungen im deutschen Team sogar unter jener vergangener Spiele. „Wir haben gar nichts davon gehabt“, sagte er dem SID.



## Deaths of Health Care Personal

* 20200516, https://www.theguardian.com/world/2020/apr/16/doctors-nurses-porters-volunteers-the-uk-health-workers-who-have-died-from-covid-19

A number of NHS and private healthcare staff, from heart surgeons to
nurses, porters and volunteers, have lost their lives to the
coronavirus in the UK.

The government says there have been 49 verified deaths of NHS staff
from Covid-19 during the pandemic, but it is clear that many others
have died. The Guardian has recorded 180 deaths that have been
reported in the news, but the true figure is likely to be higher
because not all deaths will be in the public domain.

## Wrong Treatment

### Medication

* 20200424, https://www.nature.com/articles/d41586-020-01165-3

Chloroquine hype is derailing the search for coronavirus treatments
With politicians touting the potential benefits of malaria drugs to
fight COVID-19, some people are turning away from clinical trials of
other therapies.

Hospitals in Iran, New York, Spain and China turned to
hydroxychloroquine and chloroquine as a standard therapy for patients
with COVID-19, despite guidance from the World Health Organization and
several medical associations that the drugs should not be used against
COVID-19 except in clinical trials.

* https://www.bmj.com/content/369/bmj.m1432/rr-21
also see https://www.bmj.com/content/369/bmj.m1432/rr-22

vitamin C and chloroquin treatment vs. G6PDH deficit

COVID-19 Treatment with CHLOROQUINE or Intravenous VITAMIN C Requires
Prior Exclusion of G6PD DEFICIENCY!

Professor Robin Ferner and Dr Jeffrey Aronson [1] request
controlled-trials of chloroquine and hydroxy-chloroquine which doctors
in China and USA [2 3] are using. Sue Llewellyn cautions “how to be
careful with trust and expertise on social media” [4] therefore we
must examine reports from China that Vitamin C in large doses
intravenously has had a remarkably beneficial effect on patients with
COVID-19 [5 6].


VITAMIN C TREATMENT IN USA TOO

New York Hospitals are treating COVID-19 with Vitamin C [7 8], with
patients given 1,500 milligrams (mg) of vitamin C intravenously,
repeated three or four times a day. [7]

CAUTION WITH G6PD DEFICIENCY

Large doses of oral Chloroquine and/or intravenous Vitamin C can have
serious effects on people with Glucose-6 Phosphate Dehydrogenase
(G6PD) Deficiency, an ubiquitous hereditary sex-linked enzyme
deficiency affecting millions of people world-wide, highest incidence
in Black Africa, the Mediterranean countries, Middle East, India, the
Far East, and in their relatives living in rest of the world. Indeed,
all new patients I saw in UK had to do G6PD investigation in the one
Central London Laboratory that could do the test as accurately as we
did it half a century ago in Ghana [9 to 38].


GLUCOSE-6 PHOSPHATE DEHYDROGENASE (G6PD) DEFICIENCY AND DRUG/FOOD INTERACTION

...

Large doses of Vitamin C given to COVID-19 patients with G6PD
Deficiency can cause haemolytic and other problems [30 31 37 38 40
(pages 533 to 535)]. Ghanaian doctors often see patients complaining
of “passing coca cola urine” following treatment with Chloramphenicol
for Typhoid Fever. [26 32 37 38 40].

G6PD DEFICIENCY AND DRUG/FOOD INTERACTION

Could G6PD Deficiency produce poor response to treatment? High Sickle
Cell Trait incidences in Greece (30%) and Turkey (20%) [46 47] where
G6PD Deficiency is also common [48 49] need watching because if
COVID-19 patients were given large doses of Vitamin C and they
haemolysed they could be misdiagnosed as sickle Cell Anaemia “SS” when
they were rather Sickle Trait “AS” with iatrogenic anaemia. [40 48].

CHLOROQUINE FOR COVID-19: EXCLUDE G6PD DEFICIENCY FIRST

Many with G6PD Deficiency in the USA remain unrecognised until
challenged. [38 39 40]. President Donald Trump has authorized use of
“chloroquine phosphate and hydroxychloroquine sulfate” for COVID-19
patients. [50] Haemolysis is but one complication. Bilateral ptosis
after taking Chloroquine described by Bedu-Addo [51] could, I thought,
be no-enzyme-at-all G6PD Deficiency effect [28 38 52].

* 20200502, https://multipolar-magazin.de/artikel/covid-19-a-case-for-medical-detectives

Covid-19 – a case for medical detectives

The numerous and disproportionately frequent deaths of Covid-19
patients with dark skin colour and from southern countries are
apparently also the result of a drug-related mistreatment. Affected
are people with a specific enzyme deficiency, which occurs mainly in
men whose families come from regions where malaria was or still is
endemic. They are currently being treated with hydroxychloroquine, a
drug which they do not tolerate, now being used all over the world to
fight Covid-19. If this practice does not end soon, there is a great
risk of widespread deaths, especially in Africa.  WOLFGANG WODARG


* https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(20)30691-7/fulltext

immunosupression may be the wrong reaction to hyperinflammation

### INTUBATION

* 20200508, https://www.bibliomedmanager.de/news/fachgesellschaften-wehren-sich-gegen-kritik-zu-intubation-und-beatmung-bei-covid-19-patienten-2

Nicht-invasive Beatmung und invasive Beatmung seien keine
Entweder-Oder-Konzepte der intensivmedizinischen Behandlung, sondern
ergänzen einander. Sie würden – abgestimmt auf den jeweiligen,
individuellen Patienten und dessen Situation – angewendet. Dieses
Stufenkonzept sei Teil der leitliniengerechten, intensivmedizinischen
Behandlungsstrategie.

"Vielmehr zeigen die aus dem Intensivregister der DIVI sowie aus einer
Umfrage der DGAI ableitbaren Daten, dass rund 70 Prozent der
Intensivpatienten mit COVID-19 die Behandlung auf einer
Intensivstation in Deutschland überleben." Warum andere Länder
deutlich geringere Überlebensraten melden, könne aus der bestehenden
Datenlage zur Hochzeit der Pandemie nicht abschließend beantwortet
werden. Mögliche Gründe für die besseren Ergebnisse in Deutschland
seien – neben einer leitliniengerechten intensivmedizinischen
Behandlung - ein gut vorbereitetes und hochwertiges Gesundheitssystem
sowie ein guter Zugang der gesamten Bevölkerung zu
Gesundheitsleistungen im Rahmen der Pandemien, bei beherrschbaren
Fallzahlen. Die Fachgesellschaften weisen darauf hin, dass
gleichzeitig in anderen Ländern wie Italien und den USA eine Vielzahl
von Patienten in kurzer Zeit auf ein – in Summe – deutlich schlechter
aufgestelltes Gesundheitssystem getroffen seien. Hinzu komme, dass die
in der bislang größten US-amerikanischen Studie publizierten und
häufig zitierten Zahlen zur Sterblichkeit von intensivmedizinisch
behandelten Covid-19-Patienten fehlerhaft gewesen seien und von der
Fachzeitschrift mittlerweile korrigiert wurden.

* 20200413, https://ganzemedizin.at/coronavirus-intensivabteilung-zu-intensive-therapie-fuehrt-zum-verlust-der-patienten

wenn intubiert verliert man die Patienten praktisch immer

starke Diskrepanz zw. objektiven Mess-Parametern (Sättigung, Labor,
CT) und individueller Befindlichkeit. Wenn man sich an die Guidelines
hält und frühzeitig intubiert verliert man die Patienten.

NIV-Beatmung funktioniert: der Astronautenhelm mit geringem Überdruck,
selbst 85j gehts damit recht gut obwohl die objektiven Daten
intensivere Therapie verlangen würden

Dr. Tobias Schindler beschreibt etwas frustriert und verzweifelt:
„kollegiale Arroganz, nicht zuhorchen, auf Erfahrungen nicht eingehen“
welche zum Tod seiner Patienten führt.

Fieber einfach lassen, der Automatismus das Fieber zu eliminieren bringt nichts

VitD und VitC substituieren, etwas NIV-Beatmung und sonst das
Immunsystem arbeiten lassen und nicht zuviel auf die Sättigung oder
Atemfrequenz oder CT-Befund achten gibt „zufriedenstellende
Ergebnisse„: er berichtet von schwererkrankten Uralten, die wieder
heimgegangen sind.


* 20200413, https://www.welt.de/vermischtes/article207221877/Corona-Pandemie-Sterberate-bei-Beatmungspatienten-gibt-Raetsel-auf.html

„Wir wissen, dass mechanische Beatmung nicht unkritisch ist“, sagt der
Experte Eddy Fan vom Klinikum Toronto. „Eine der wichtigsten
Erkenntnisse der vergangenen Jahrzehnte ist, dass medizinische
Beatmung Lungenverletzungen verschlimmern kann“, erklärt er. „Also
müssen wir aufpassen, wie wir sie einsetzen.“

Beatmungsgeräte pressen Sauerstoff in den Körper, wenn die Eigenatmung
unzureichend ist. Bei den Maschinen wird der Patient in der Regel
ruhiggestellt und durch den Hals intubiert. Die Risiken der Behandlung
können dabei mit geringerem Umfang und Druck der Luftstöße reduziert
werden, wie Fan erklärt.

Ganz allgemein sterben nach Expertenangaben etwa 40 bis 50 Prozent der
an Beatmungsgeräte angeschlossenen Kranken mit schweren
Atemnotsyndromen. Inmitten der Corona-Pandemie wurden aber deutlich
höhere Sterbezahlen gemeldet, was den Medizinern zu denken gibt.

In New York City waren es 80 Prozent der Corona-Patienten oder mehr,
wie die Behörden mitteilten. Auch aus anderen Teilen der USA seien
überdurchschnittlich hohe Sterberaten bekannt geworden, bestätigt
Albert Rizzo von der Medizinorganisation American Lung Association.

Ähnliche Berichte gab es auch aus China und Großbritannien. In Wuhan,
wo Covid-19 im Dezember zuerst auftauchte, kam eine kleine Studie auf
einen Prozentsatz von 86. Aus dem Vereinigten Königreich wurde eine
Sterberate von 66 Prozent gemeldet.

Einige Mediziner spekulieren über eine mögliche schädliche Reaktion
des Immunsystems, die mit dem Beatmungsgerät ausgelöst oder verstärkt
werden könnte.

„Wenn wir es schaffen, ihren Zustand zu verbessern, ohne sie
intubieren zu müssen, ist das Ergebnis wahrscheinlich besser“, betont
der Notfallmediziner Joseph Habboushe aus Manhattan mit Blick auf die
Kranken. Vor einigen Wochen seien schwer betroffene Corona-Patienten
routinemäßig an Beatmungsgeräte gekommen, erklärt er.


Nach Expertenangaben werden Patienten mit bakterieller
Lungenentzündung vielleicht ein bis zwei Tage so beatmet. Bei
Corona-Patienten zeichnet sich eine deutlich längere Zeit ab. „Sieben
Tage, zehn Tage, 15 Tage, und sie sterben“, sagte kürzlich der New
Yorker Gouverneur Andrew Cuomo, als Journalisten ihn danach fragten.


Lungenspezialist Roger Alvarez von der Universität Miami setzt selbst
auf den Einsatz von Stickoxid, um die Beatmungsgeräte erst als letztes
Mittel heranziehen zu müssen. Die Geräte seien „eine unterstützende
Maßnahme, während wir warten, dass sich der Körper des Patienten
erholt“.

## A/C, Cooling Systems etc.

* 20200227, https://facilityexecutive.com/2020/02/coronavirus-and-hvac-ashrae-releases-guidance-material/

“The recent escalation in the spread of coronavirus disease 2019 is
alarming on a global scale,” said 2019-20 ASHRAE President Darryl
K. Boyce, P.Eng. “While ASHRAE supports expanded research to fully
understand how coronavirus is transmitted, we know that healthy
buildings are a part of the solution. ASHRAE’s COVID-19 Preparedness
Resources are available as guidance to building owners, operators and
engineers on how to best protect occupants from exposure to the virus,
in particular in relation to airborne particles that might be
circulated by HVAC systems.”

* https://people.com/health/air-conditioning-coronavirus-spreader-9-people-infected-at-restaurant/

According to the study [see below], which will be published in
Emerging Infectious Diseases, a journal from the Centers for Disease
Control, the infected diner was eating at a restaurant in Guangzhou,
China, in January with nine other people. Their table, and two others,
were directly in line with the restaurant’s air conditioning
unit. Four people at the infected diner’s table later tested positive
for COVID-19, along with five people at the neighboring tables.

* https://wwwnc.cdc.gov/eid/article/26/7/20-0764_article

Volume 26, Number 7—July 2020
Research Letter
COVID-19 Outbreak Associated with Air Conditioning in Restaurant, Guangzhou, China, 2020

Jianyun Lu1, Jieni Gu1, Kuibiao Li1, Conghui Xu1, Wenzhe Su, Zhisheng Lai, Deqian Zhou, Chao Yu, Bin XuComments to Author , and Zhicong YangComments to Author

* https://www.cdc.gov/coronavirus/2019-ncov/php/cooling-center.html

Purpose: This document provides interim guidance to reduce the risk of
introducing and transmitting SARS COV-2 (the agent responsible for
causing COVID-19 disease) in cooling centers. It should be used in
conjunction with existing cooling center operation and management
plans, procedures, guidance, resources, and systems.

Purpose: This document provides interim guidance to reduce the risk of
introducing and transmitting SARS COV-2 (the agent responsible for
causing COVID-19 disease) in cooling centers. It should be used in
conjunction with existing cooling center operation and management
plans, procedures, guidance, resources, and systems.

* https://www.rehva.eu/activities/covid-19-guidance

As response to the coronavirus (COVID-19 or SARS-CoV-2) pandemic,
REHVA experts drafted a guidance document on how to operate and use
building services in areas with a coronavirus outbreak to prevent the
spread of COVID-19 depending on HVAC or plumbing systems related
factors.

## Outbreaks in Tight Living/Working Quarters

* 20200521, https://www.onetz.de/oberpfalz/schwandorf/vier-neue-coronafaelle-vatertag-landkreis-schwandorf-id3028944.html

Vier neue Corona-Infektionen sind im Landkreis Schwandorf
verzeichnen. Es handelt sich um eine dreiköpfige Familie in der
Gemeinschaftsunterkunft in Neunburg vorm Wald und um einen Fall aus
einer anderen Gemeinde.

Am Sonntag, 8. März, war der erste positive Coronavirus-Fall im
Landkreis Schwandorf bestätigt worden. Ebenso erging am selben Tag die
erste Pressemitteilung. Am Feiertag Christi Himmelfahrt, an dem das
Landratsamt ebenfalls mit einigen Mitarbeitern Dienst tut, wird die
Öffentlichkeit mit der 75. Pressemitteilung informiert. Die Gesamtzahl
der bestätigten Infektionen liegt jetzt bei 489.

Von den am Dienstag im Schlachthof in Schönsee abgestrichenen rund 20
Personen liegen die Ergebnisse, die allesamt im Landesamt für
Gesundheit und Lebensmittelsicherheit in Erlangen ausgewertet werden,
erst zum Teil vor. Zur Stunde haben das Landratsamt sieben
Rückmeldungen erreicht. Erfreulicherweise sind alle sieben Tests
negativ.
 
Von den am Mittwoch in der Gemeinschaftsunterkunft in Neunburg vorm
Wald im Rahmen der Reihentestung abgestrichenen Personen liegen noch
keine Ergebnisse vor. Die aus dieser Unterkunft nun zu vermeldenden
drei Fälle wurden nicht im Rahmen dieser Reihenuntersuchung getestet,
sondern unabhängig davon von einem anderen Arzt abgestrichen und in
einem anderen Labor ausgewertet.

* 20200520, https://www.regensburg-digital.de/dutzende-corona-faelle-in-regensburger-fluechtlingsunterkunft/20052020/

Mindestens 37 Menschen haben sich in einer Flüchtlingsunterkunft in
Regensburg mit dem Covid 19-Virus angesteckt. Seit heute gilt für die
250 Bewohnerinnen und Bewohner offenbar eine Ausgangssperre. UPDATE:
Zwischenzeitlich gibt es eine Stellungnahme der Regierung, die wir am
Ende des Textes komplett veröffentlichen.

Die Flüchtlingsunterkunft in der Dieselstraße in Regensburg ist ein
Corona-Hotspot. Bislang wurden 37 Bewohnerinnen und Bewohner positiv
auf das Covid 19-Virus getestet. Laut BI Asyl handelt es sich dabei um
rund die Hälfte aller bisher getesteten Personen. Insgesamt leben 250
Menschen in der Gemeinschaftsunterkunft.  Zu dritt auf dem Zimmer: Ein
Infizierter, zwei Gesunde

Bereits Anfang Mai waren in der Unterkunft im Weinweg neun Geflüchtete
positiv getestet worden. Die damalige Reaktion: Alle 80 Bewohnerinnen
und Bewohner wurden für 14 Tage unter Quarantäne gestellt und durften
das Flüchtlingsheim nicht mehr verlassen. So scheint das Vorgehen nun
auch in der Dieselstraße auszusehen. „Wir haben eben erfahren, dass
die Geflüchteten die Unterkunft nicht mehr verlassen dürfen“, so ein
Mitglied der BI Asyl. Ansonsten sei der Informationsfluss bislang
spärlich.

Ein Bewohner schreibt, dass am Montag getestet worden sei und es am
Dienstag die ersten Ergebnisse gegeben habe. Weiter heißt es
(Übersetzung aus dem Englischen): „Sie sagen, dass 37 Leute Corona
haben. Wir sind in unserem Zimmer zu dritt. Einer von uns ist positiv,
aber sie wollen ihn nicht aus dem Zimmer holen. Sie haben gesagt, es
gebe dafür keinen Platz. Also müssen wir anderen zwei mit ihm
gemeinsam im Zimmer bleiben.“

* 20200425, https://www.reuters.com/article/us-health-coronavirus-prisons-testing-in/in-four-u-s-state-prisons-nearly-3300-inmates-test-positive-for-coronavirus-96-without-symptoms-idUSKCN2270RX

In four U.S. state prisons, nearly 3,300 inmates test positive for
coronavirus -- 96% without symptoms


* 20200517, https://www.vienna.at/hagenbrunn-wohl-ausloeser-des-coronavirus-clusters-in-wien/6621088

Am Sonntag hat der Wiener Gesundheitsstadtrat Peter Hacker (SPÖ) eine
erste Analyse des aktuellen Clusters an Coronavirus-Infektionen
präsentiert. Es deute alles darauf hin, dass das Postzentrum im
niederösterreichischen Hagenbrunn bzw. die Leiharbeitsfirmen Auslöser
waren. Offenbar wiesen nur zehn Prozent der Wiener Betroffenen
Symptome auf. Das Bundesheer reagierte indes auf Kritik zur Versorgung
der in Hagenbrunn helfenden Soldaten.

"Es zeigt alles nach Hagenbrunn", sagte Hacker im Gespräch mit der
APA. Dies habe die genaue Betrachtung der jüngsten Zahlen - viele der
in Hagenbrunn tätigen Arbeiter leben in Wien - ergeben. Leiharbeit sei
offenbar diesbezüglich ein großes Problem. Mit Flüchtlingen habe dies
nichts zu tun. Denn die meisten Fälle etwa in der Unterkunft in
Erdberg ließen sich auf die Post-Verteilzentren zurückführen - und
nicht umgekehrt.

Von mehr als 1.000 Tests in verschiedenen Asyl-Einrichtungen waren nur
knapp 40 (davon vier Betreuungspersonen, Anm.) positiv. Dabei sei ein
Konnex in Sachen Leiharbeit erkennbar gewesen, hieß es. Der Großteil
der betroffenen Leiharbeiter seien zudem keine Flüchtlinge, wurde
versichert.

Der Cluster in Sachen Post sei erkannt worden, weil Wien genauer
hinschaue: "Wir bringen Licht in den Schatten." Dabei habe man
entdeckt, dass nur zehn Prozent der Fälle symptomatisch waren. Beim
Rest sei die Infektion offenbar sehr leicht verlaufen. Dies sei eine
neue Erkenntnis, versicherte Hacker. Man stehe vor keinem neuerlichen
"Ausbruch", sondern die gestiegenen Zahlen ergäben sich daraus, dass
punktuell sehr intensiv untersucht werde.


Die Unterstützung der Gesundheitsbehörden sei überdies die gesetzliche
Verpflichtung der Exekutive, gab er zu bedenken. Man nehme diese Hilfe
bereits in Anspruch. Contact-Tracing durch die Kriminalpolizei sei
jedoch nicht nötig, versicherte er. "Der Innenminister soll sich viel
mehr Sorgen machen, dass an der Grenze nicht kontrolliert wird."
Offenbar sei es etwa möglich, dass Personen aus Kroatien ohne
Kontrollen einreisen, berichtete Hacker.

Er plädierte darüber hinaus auch für eine Diskussion über
Leiharbeit. Denn auch die Post - auch wenn deren Verteilzentren nun
stark betroffen seien - sei nicht unmittelbar dafür verantwortlich zu
machen. Vielmehr sei es ein Problem, dass Personen, die als "neue
Selbstständige" auf Abruf arbeiten würden, sozial nicht abgesichert
seien.



* 20200517, https://kurier.at/chronik/wien/corona-alarm-obdachlosenunterkunft-in-wien-gesperrt/400844672

In einer Unterkunft für Obdachlose in Wien-Hietzing wurde ein Bewohner
positiv auf das Coronavirus getestet. Laut Presse wurden insgesamt 50
Personen in ein ehemaliges Krankenhaus (Floridsdorf) gebracht, wo sie
nun 14 Tage in Quarantäne bleiben sollen.

* 20200516, https://www.tagesschau.de/inland/heinsberg-coronavirus-101.html

In einem Paketzentrum in Hückelhoven (Kreis Heinsberg) sind rund 80
Mitarbeiter mit dem Coronavirus infiziert. Bislang liegt aber nur ein
Teil der Testergebnisse vor.

Der Paketdienst DPD hat seinen Standort in Hückelhoven im Kreis
Heinsberg wegen zahlreicher Coronavirus-Fällen geschlossen. Das
bestätigte eine Sprecherin der Kreisverwaltung. Nach Worten des
Heinsberger Landrats Stephan Pusch sind rund 80 Beschäftigte mit dem
Coronavirus infiziert worden. Etwa 340 der insgesamt 400 Proben seien
ausgewertet worden, sagte der CDU-Politiker am Samstag in einer
Videobotschaft bei Facebook. Am Vormittag hatte der Kreis Heinsberg
als erstes Zwischenergebnis zunächst die Zahl von 42 Infektionen
genannt. Alle Beschäftigten sind in zweiwöchiger Quarantäne.

* 20200518, https://www.br.de/nachrichten/bayern/corona-faelle-in-schlachthof-straubing-bogen-reisst-obergrenze,RzGL9oe

Corona-Fälle in Schlachthof: Straubing-Bogen reißt Obergrenze


Der Landkreis Straubing-Bogen und die Stadt Straubing reißen in der
Corona-Epidemie die Obergrenze von 50 Neuerkrankungen pro 100.000
Einwohner binnen sieben Tagen. Laut einer Pressemitteilung des
Landratsamtes vom Sonntag liegt die aktuelle Quote bei 53,6. Dennoch
sei keine Rücknahme der Lockerungen der bisher geltenden Auflagen
notwendig. Dies hätten die Gesundheitsbehörden bestätigt.  Infizierte
Schlachthofarbeiter treiben Zahl nach oben

Die Zahl der positiv getesteten Schlachthofmitarbeiter in Bogen ist
demnach weiter gestiegen. Mittlerweile seien 81 Mitarbeiter des
Schlachthofs in Bogen positiv auf das Coronavirus getestet worden. Das
übersteigt zwar den Schwellenwert der Sieben-Tages-Inzidenz (50 Fälle
pro 100.000 Einwohner), doch das erarbeitete Konzept von Stadt,
Landkreis und zuständigen Behörden wurde jetzt vom Staatsministerium
für Gesundheit und Pflege gebilligt, weswegen es keine verschärften
Corona-Maßnahmen gibt.

* 20200510, https://www.aerzteblatt.de/nachrichten/112728/Corona-in-Fleischfabrik-Kritik-an-Zustaenden-mehr-als-200-Infizierte

Düsseldorf/Coesfeld – Nach dem SARS-CoV-2-Ausbruch in einem
Fleischbetrieb in Coes­feld mit mehr als 200 infizierten Arbeitern
gibt es massive Kritik an den Zuständen in der Fabrik. Das
Verwaltungsgericht Münster lehnte einen Eilantrag der Firma
West­fleisch ge­gen die befristete Schließung des betroffenen
Schlacht- und Zerlegebetriebes ab.

Knapp die Hälfte der Ergebnisse von bisher rund 950 Corona-Tests lag
demnach vor. Er­neut waren heute Teams des Gesundheitsamtes vor Ort,
um die Arbeiter in ihren verstreut im Kreis Coesfeld liegenden
Unterkünften zu testen und über die Quarantäne zu beleh­ren, wie ein
Kreissprecher sagte. Dabei unterstützten sie Dolmetscher. Insgesamt
hat der betroffene Betrieb rund 1.200 Beschäftigte. Die Arbeiter sind
nach Angaben von Westflei­sch mehrheitlich in Wohnungen mit drei, vier
oder fünf Personen untergebracht. Viele Ar­beiter in der
Fleischbranche kommen aus Osteuropa.

Der Kreis Coesfeld hatte die Schließung der Westfleischfabrik von
Samstag bis zum 18. Mai verfügt. Es sei davon auszugehen, dass es noch
eine unbestimmte Anzahl von Ver­dachtsfällen oder Ansteckungen dort
gebe, hieß es in dem Gerichtsbeschluss. Westfleisch kann dagegen
innerhalb von zwei Wochen Beschwerde beim Oberverwaltungsgericht NRW
einlegen.

Laschet rechtfertigt Stopp

Das Amt für Arbeitsschutz habe bei einer Überprüfung festgestellt,
dass es sowohl im Be­reich des Zerlegebandes als auch in den Umkleiden
Probleme gebe, den Mindestabstand von 1,50 Metern einzuhalten, so das
Gericht. Die Mund-Nasen-Schutzmasken würden am Zerlegeband nicht
korrekt getragen. Die Firma sei nicht in der Lage gewesen,
Infektions­schwerpunkte zu benennen.

Auch der Kreis Steinfurt im Münsterland testete unter Hochdruck
Bewohner von Sammel­unterkünften, die insbesondere in
fleischverarbeitenden Betrieben arbeiten. Insgesamt rund 1.700
Bewohner in etwa 100 Unterkünften seien aus nahezu allen Städten und
Ge­meinden gemeldet worden, teilte der Kreis mit.

* 20200508, https://www.aerzteblatt.de/nachrichten/112696/Ausbrueche-in-Schlachthoefen-Notbremsen-wegen-SARS-CoV-2-gezogen

Die Regierung in Düsseldorf griff heute durch und schloss
vorübergehend einen Schlacht­betrieb in Coesfeld, in dem sich
besonders viele Mitarbeiter angesteckt hatten. Auch in
Schleswig-Holstein war eine Schlachterei betroffen. Im Thüringer
Landkreis Greiz hatten sich vor allem Bewohner und Personal von
Altheimen infiziert.

* 20200511, https://www.n-tv.de/panorama/Fleischfabriken-werden-zu-Corona-Hotspots-article21772670.html

In den USA sind Schlachthöfe und Fleischfabriken gefährliche
Coronavirus-Brutstätten. In Europa zunächst nicht. Doch jetzt steigen
ausgerechnet in Deutschland die Infektionszahlen in
fleischverarbeitenden Betrieben massiv an. In NRW wird ein Schlachthof
geschlossen, die Parallelen zu den USA sind deutlich.

Damit entwickelt sich eine Branche in Deutschland zum Corona-Hotspot,
die auch schon in den USA aufgefallen war. Dort ruht mittlerweile die
Produktion in Dutzenden Schlachthöfen und fleischverarbeitenden
Fabriken, nachdem sich zahlreiche Arbeiter mit dem Coronavirus
angesteckt hatten. Inzwischen sind offiziellen Angaben zufolge rund
5000 von ihnen an Covid-19 erkrankt, mindestens 20 sind gestorben.

In diesen Fabriken sind die Maßnahmen, die die Ausbreitung des
Coronavirus verhindern können, kaum einzuhalten. Denn die Arbeiter
stehen eng beieinander an den Fließbändern, an denen Rinder, Schweine
und Hühner geschlachtet und weiterverarbeitet werden. Der Job ist
außerdem körperlich sehr anstrengend, weshalb viele Arbeiter auf die
schützende Maske verzichten, um besser atmen zu können. In den letzten
Wochen haben 115 Fleisch- und Geflügelfabriken in den USA
Covid-19-Infektionen gemeldet, berichtet Bloomberg. Einige Ausbrüche
waren so schwerwiegend, dass mindestens 18 Anlagen komplett
stillgelegt wurden.

Schlecht für die Arbeiter, gut für das Virus
USSchweine.jpg
Wirtschaft 28.04.20
Corona-Not bei US-Farmern Hunderttausende Schweine landen direkt im Müll

Die Hallen für die Fleischproduktion sind immer kühl und feucht, in
diesem Klima hat es das Coronavirus offenbar besonders leicht. Die
großen Tierkörper bewegen zudem die Luft ständig, sodass das Virus
immer weiter getragen wird. In den USA rekrutieren die Fleischfirmen
ihre Mitarbeiter vor allem aus mexikanischen und mittelamerikanischen
Einwanderern, die dann in Sammelunterkünften eng beieinander
leben. Die meisten dieser Arbeiter haben keine Krankenversicherung,
sie arbeiten bei den Konzernen für Stundenlöhne um die 15 US-Dollar,
andere Arbeit gibt es in den ländlichen Gebieten
kaum. US-Medienberichten zufolge wurden Arbeiter gezwungen, weiter zum
Dienst zu erscheinen, obwohl sie bereits deutliche Covid-19-Symptome
wie Husten und hohes Fieber zeigten.

...

 Das Amt für Arbeitsschutz habe bei einer Überprüfung festgestellt,
 dass es sowohl im Bereich des Zerlegebandes als auch in den Umkleiden
 Probleme gebe, den Mindestabstand von 1,50 Metern einzuhalten, so das
 Gericht. Die Mund-Nasen-Schutzmasken würden an den Fließbändern nicht
 korrekt getragen. Die Firma sei nicht in der Lage gewesen,
 Infektionsschwerpunkte zu benennen. Gewerkschaften fordern schon seit
 langem schärfere Kontrollen und grundlegend bessere
 Arbeitsbedingungen in allen Fleischbetrieben.

In den wenigen Schlacht- und Zerlegebetrieben, die in den USA
inzwischen als vorbildhaft gelten, wurden die Schutzmaßnahmen für die
Arbeiter deutlich ausgebaut. Bei den Mitarbeitern wird zweimal während
der Schicht Fieber gemessen. Schon wer erhöhte Temperatur hat, wird zu
weiteren Symptomen befragt. Die ausgegebene Schutzkleidung wurde
erweitert, die Arbeiter müssen Masken tragen, überall wurden
Plexiglasscheiben eingezogen. Trotzdem kehren viele aus Angst vor dem
Virus nicht an ihren Arbeitsplatz zurück. Die Debatte um die Zukunft
von Fleischproduktion und -konsum hat in den USA bereits Fahrt
aufgenommen.

* 2020, https://www.swr.de/swraktuell/baden-wuerttemberg/karlsruhe/birkenfeld-fleischfabrik-102.html

Nach einer zweiten Testreihe in einer von Corona-Infektionen
betroffenen Großschlachterei in Birkenfeld (Enzkreis) ist klar: Mehr
als ein Drittel der Belegschaft ist betroffen. SPD und FDP üben nun
scharfe Kritik.

Nach Angaben des Landratsamtes des Enzkreises sind damit insgesamt 412
der 1.100 Beschäftigten der Fleischfabrik mit dem Coronavirus
infiziert. Fast alle sind Leiharbeiter aus Osteuropa, die überwiegend
in Gemeinschaftsunterkünften lebten. Die meisten von ihnen sind
inzwischen wieder gesund. Etwa 80 Infizierte befinden sich noch in
einer Quarantäne-Einrichtung. Eine ersten Testung der Belegschaft vor
drei Wochen hatte bereits rund 300 Infizierte ergeben.

* 20200319, https://news.yahoo.com/coronavirus-hits-migrant-workers-qatar-133251577.html

DUBAI (Reuters) - Qatar's old industrial zone has emerged as a hot
spot for the coronavirus in the Gulf Arab state, putting at risk many
migrant workers who live and work in the area of car service centers,
warehouses and small shops.

* TODO:
    - Migrant workers behind Singapore's second wave
    - Migrant workers in Saudi-Arabia 

## Outbreaks in Care Homes

* 20200525, https://www.nytimes.com/2020/05/25/world/europe/coronavirus-uk-nursing-homes.html

But the frailest spot on the island remained catastrophically exposed:
Home Farm, a 40-bed nursing home for people with dementia. Owned by a
private equity firm, Home Farm has become a grim monument of the push
to maximize profits at Britain’s largest nursing home chains, and of
the government’s failure to protect its most vulnerable citizens.

Today, all but seven of the residents have been stricken. More than a
quarter are dead.

The virus has ravaged nursing homes across Europe and the United
States. But the death toll in British homes — 14,000, official figures
say, with thousands more dying as an indirect result of the virus — is
becoming a defining scandal of the pandemic for Prime Minister Boris
Johnson.

By focusing at first on protecting the health system, Mr. Johnson’s
strategy meant that some infected patients were unwittingly moved from
hospitals and into nursing homes. Residents and staff members were
denied tests, while nursing home workers begged in vain for protective
gear.

“We were witnessing horrendous images in Spain and Italy, so a lot of
attention was paid to maintaining and securing the National Health
Service,” said Dr. Donald Macaskill, the chief executive of Scottish
Care, which represents nursing homes. “The N.H.S. was prioritized at
the expense of social care.”

At Home Farm, set above a silvery loch on a northeastern finger of the
island, employees do not know how the virus got inside. But early in
the pandemic, they expressed fears to their bosses about the company
bringing in workers from outside the island. And they fretted over the
half-dozen new residents who were deposited in empty beds, some of
them from hospitals and others from their own homes.

Responding to the outbreak, the Scottish health secretary said a
review should be conducted of its entire nursing home system, which
falls under Scottish government control. In England, an independent
commission is looking into “serious potential breaches” of human
rights in nursing homes.

Problems funding nursing homes, a bugaboo in British politics since
Margaret Thatcher privatized them in 1990, hobbled Mr. Johnson’s
predecessor, Theresa May, whose proposal to raise resident fees was
nicknamed the “dementia tax.”

Now Mr. Johnson is feeling the heat. In the House of Commons, he faces
a weekly barrage over nursing homes, including accusations that he
lied about government guidance playing down the chance of outbreaks.

Britain’s hospitals are revered for providing free, universal health
coverage. But the nursing home system is a decidedly American export,
with corporate giants based in offshore tax havens often paying
workers the minimum wage and trying to wring profits out of an aging
population.

For-profit nursing homes now control even more of the British market —
86 percent — than the American market. And the biggest chain, HC-One,
which owns Home Farm, has been hit hard. Cases have broken out in
two-thirds of its 328 homes. Four employees and 934 residents have
died.

Among the dead was Colin Harris, 66, a witty Home Farm resident with
dementia and Parkinson’s disease.

In the months before Mr. Harris died on May 6, staffing was so thin
that his incontinence pads were often left wet, eroding the skin on
his thighs, Mandie Harris, his wife, wrote in a complaint. His
dentures came unglued when he ate.

After a video call on April 8, Ms. Harris complained to HC-One that
she saw aides without protective gear and a resident’s husband walking
down the corridor in street clothes. “Where is the infection control?”
she asked. In response, the company told her in an email that the man
was being hired as a cleaner and that Mr. Harris’s “teeth appeared
clean and secure.”

Inside the home, staff members were becoming panicked, said three
workers, who spoke on the condition of anonymity because they had been
instructed not to talk publicly. In early April staff meetings, they
pleaded for better protective gear, and in some cases ordered their
own.

But management told workers to wear masks only around suspected
coronavirus patients — an approach that Ms. Harris, in her complaint,
compared to “closing the gate after the horse has bolted.” The company
told her that aides who wanted masks were provided with them starting
April 9. Not until April 18, a week before the outbreak, were masks
required.

Even so, managers sometimes refused to wear masks themselves,
including on medicine rounds to residents’ rooms, complaining that
they itched, the three workers said.

...

For HC-One, the nursing home business has been lucrative, as the
company paid more than 50 million pounds, or nearly $61 million, in
dividends from 2017 to 2019.

But staff members at Home Farm suffered. During 12-hour shifts, they
sometimes made do with no more than one nurse and two aides on an
18-bed floor, employees said. The residents’ help buttons buzzed
incessantly.

Staffing shortages were so dire, regulators said in January, that Home
Farm stopped accepting new residents. An inspection found that the
home was unclean, staffing was uneven and “the level and quality of
care and support people received was not always adequate.”

HC-One said it “faced chronic recruitment challenges,” forcing the
home to rely on temporary workers.

But as the pandemic raged in Britain, the moratorium on new admissions
at Home Farm was lifted. When employees complained about the risk of
transmission and even volunteered to temporarily move into the home to
avoid carrying the virus inside, management said beds needed to be
filled with paying customers, two workers said.

HC-One said that, like other homes, it was asked to help hospitals by
admitting some patients.

In late April, employees’ fears were realized: An aide tested
positive. Employees said they learned the news not from management,
but from Facebook, where the aide’s mother posted about it.

Residents, too, were showing symptoms, like slackening appetites and
high temperatures. By April 27, a Monday, staff members were adamant
that residents needed help. Management urged them not to worry,
arguing that it was just the flu, the three workers said.

At the time, testing was scarce: Not until days later did Scotland say
it would offer tests to any nursing homes with cases. So only four of
the home’s 36 residents were initially given tests. Blanket tests
later that week revealed the calamitous extent of the outbreak: 28
residents were infected, along with 26 of 52 staff members.

Residents’ families said administrators were slow to acknowledge the
likely spread. Up until her husband’s positive result, Ms. Harris
said, management told her he was being treated as though he had a
chest or urinary tract infection. Later, Ms. Harris said management
insisted he was tired, but nothing worse, only for her to see him on a
video call “looking deathly.”

HC-One attributed the number of infections in part to Home Farm being
“one of the first care homes where everyone was tested.” The company
said it was “confident the manager acted appropriately with regards to
Mr. Harris’s health.”

The police are now investigating the deaths of three residents. The
local health service has stepped in to help run Home Farm. Regulators
tried this month to take HC-One’s license in court but have since
backed off.



...



* 20200515, https://www.br.de/nachrichten/bayern/raum-coburg-corona-neuinfektionen-uebersteigen-kritischen-wert,RyxiedK

Die Neuinfektionen im Landkreis Coburg würden hauptsächlich in
Pflegeheimen in Stadt und Landkreis Coburg auftreten und stünden unter
anderem im Zusammenhang mit Dialysebehandlungen. Das Gesundheitsamt
Coburg habe bereits alle notwendigen Maßnahmen zur Eindämmung des
Coronavirus eingeleitet, Reihentestungen in den Pflegeheimen hätten
bereits stattgefunden.



* 20200507, https://www.spiegel.de/panorama/corona-in-krankenhaus-in-hamburg-harburg-bei-uns-wurde-ein-ausbruch-vertuscht-a-2a89b1a0-fc27-4721-9cd2-e2807584a5c8

Das Asklepios Klinikum Harburg, ein 868-Betten-Krankenhaus im Süden
Hamburgs, kämpft seit Wochen mit einem Corona-Ausbruch. Drei
Patienten, darunter eine Frau Jahrgang 1970, sind gestorben, ein
weiterer ist erkrankt. Alle waren Patienten des Krankenhauses und
haben sich womöglich erst dort mit dem Virus infiziert. Fünf
Klinikmitarbeiter wurden positiv auf Covid-19 getestet, 48 Ärzte und
Pfleger mussten in Quarantäne. Zwei Stationen sind geschlossen.

Das Virus kam wahrscheinlich mit zwei betagten Patienten aus einem
Pflegeheim in Hamburg-Wilhelmsburg in das Allgemeine Krankenhaus. Der
Fall zeigt exemplarisch, wie gefährlich es für einen Klinikbetrieb
sein kann, wenn Seniorenheime nicht sorgfältig jeden Verdacht auf
Corona bei einer Überstellung von Patienten melden.



Am 1. und 2. April trafen zwei Bewohner des Pflegeheims "Am Inselpark"
aus Hamburg-Wilhelmsburg in dem Asklepios-Krankenhaus ein, sie hatten
urologische Beschwerden. Dass die beiden Männer aber möglicherweise
auch mit dem Coronavirus infiziert waren, teilte das Seniorenzentrum
des Betreibers KerVita der Klinik nicht mit. Denn bereits am 31. März
war in dem Heim ein erster Bewohner positiv auf Covid-19 getestet
worden. Einer der beiden eingelieferten Senioren war noch am Morgen
des 2. April kurz vor seinem Transport nach Harburg auf Covid-19
getestet worden. Am selben Tag meldete auch das zuständige
Gesundheitsamt einen Corona-Ausbruch in dem Heim an das Robert
Koch-Institut in Berlin.

Die Klinikärzte erfuhren von alledem nichts. Ohne die beiden Senioren
zu testen, legten sie den einen auf die Urologie, den anderen auf die
Thoraxchirurgie. Gut eine Woche später mussten beide Stationen
schließen: wegen Corona.

Die Hamburger Gesundheitssenatorin erfuhr erst am 27. April von dem
Fall – offenbar durch Recherchen des SPIEGEL. Nach einem Bericht über
einen Covid-19-Ausbruch am Universitätsklinikum Hamburg (UKE) hatten
sich zwei Mitarbeiterinnen aus der Asklepios Klinik Harburg beim
Nachrichtenmagazin gemeldet. "Auch bei uns wurde ein nosokomialer
Ausbruch vertuscht", sagt eine der beiden.

Beide Frauen, die den SPIEGEL kontaktierten und den Vorfall unabhängig
voneinander schilderten, gehören zum medizinischen Personal. Sie
möchten anonym bleiben, weil sie um ihre Arbeitsplätze fürchten, ihre
Identitäten sind dem SPIEGEL aber bekannt. Eine davon behandelte
selbst einen der infizierten Senioren, ohne Schutzkleidung. Besonders
empört sie, wie fahrlässig mit dem Leben fragiler Patienten umgegangen
wurde. "Bei uns liegen viele Krebskranke."


Der Klinikbetreiber scheint da weniger sicher: "Ob sich ein Patient im
Klinikum Harburg infiziert hat, ist aufgrund der langen
Inkubationszeit nicht nachweisbar", teilt ein Asklepios-Sprecher
mit. Fakt ist: Am 7. und am 8. April, nachdem bekannt wurde, dass die
ehemaligen Heimbewohner Corona-positiv waren, wurden alle
Kontaktpersonen auf den betroffenen Stationen durchgetestet. Bei
insgesamt vier Patienten lag ein positives Testergebnis vor. Drei
davon sind laut SPIEGEL-Informationen inzwischen verstorben.

Wie lange sich die vier positiv getesteten Patienten in der Klinik
aufhielten, teilt Asklepios nicht mit – angeblich aus
Datenschutzgründen. Sie seien aber symptomfrei aufgenommen worden;
erst "eine spätere Testung im Zusammenhang mit den beiden Patienten
aus dem KerVita-Pflegeheim ergab den Nachweis einer Infektion mit
Sars-CoV-2", bestätigt der Sprecher.


Sechs Ärzte und 42 Pflegekräfte in Quarantäne

Die beiden Klinikmitarbeiterinnen, die sich beim SPIEGEL meldeten,
sind überzeugt davon, dass sich die Patienten erst auf ihren Stationen
angesteckt haben – so wie mehrere ihrer Kolleginnen und
Kollegen. Letzteres räumt auch Asklepios ein: Fünf Angestellte auf den
betroffenen Stationen wurden positiv auf das Virus getestet, darunter
der Arzt, der den Patienten auf der Urologie operierte. Sie würden
sich immer noch zu Hause erholen. Insgesamt wurden sechs Ärzte und 42
Pflegekräfte in Quarantäne geschickt. Die beiden Stationen sind seit
dem 9. April geschlossen.

Hiobsbotschaft aus dem Heim

Das Testergebnis des Heimbewohners lag am 3. April vor. An diesem Tag
meldete das durchführende Labor den positiven Befund dem
Gesundheitsamt, so berichtet es Asklepios im Nachgang. Doch erst am
Montag, 6. April, rief jemand aus dem Heim in der Klinik an und
überbrachte die Hiobsbotschaft. Das positive Testergebnis, verteidigt
sich Betreiber KerVita, habe dort nicht früher vorgelegen. Warum auch
das Gesundheitsamt die Kontaktpersonen des Infizierten im Krankenhaus
nicht früher informierte, lässt die Behörde offen. So hatte der
Heimbewohner bereits vier volle Tage auf der Urologie verbracht, bis
dort bekannt wurde, dass er positiv auf Corona getestet wurde.

Im selben Zeitraum befanden sich laut Asklepios elf Patienten mit der
Diagnose Krebs auf der Station. Dass sich keiner von ihnen infiziert
haben soll, dürfte Glück im Unglück sein. Der Patient aus dem Heim lag
laut der Klinik wegen des Verdachts auf einen multiresistenten Keim
isoliert in einem Einzelzimmer. Ein Mund-Nasen-Schutz war bei seiner
Versorgung aber nicht erforderlich. "Der Patient war sehr aufwendig in
der Pflege, weshalb es viele enge Mitarbeiterkontakte gab", schreibt
der Sprecher.

Mittlerweile wurden Maßnahmen ergriffen, die verhindern sollen, dass
sich ein Fall wie in Harburg wiederholt. Seit dem 20. April sind
Hamburger Seniorenunterkünfte verpflichtet, vor dem Transport eines
Bewohners in ein Krankenhaus diesem zu melden, wenn zuvor bei ihnen
gehäuft nachgewiesene Covid-19-Fälle aufgetreten sind. Die
Krankenhäuser der Hansestadt wiederum müssen seit vergangener Woche
jeden Patienten bei der Aufnahme auf das neuartige Virus testen.

Eine der SPIEGEL-Informantinnen bezweifelt, dass damit alles geregelt
ist. "Der Profit geht Klinikkonzernen immer vor dem Wohl ihrer
Mitarbeiter", sagt sie. "Pflegeheime wie das von KerVita werden schon
deshalb nicht angezeigt, weil sie einen steten Nachschub an Patienten
liefern."

* 20200423, https://www.washingtonpost.com/world/europe/nursing-homes-coronavirus-deaths-europe/2020/04/23/d635619c-8561-11ea-81a3-9690c9881111_story.html


Many European countries have banned family visits to nursing homes, an
attempt to shelter the residents from the spread of the virus, since
it is far more lethal among older people and those with preexisting
conditions. But those bans, though well-intentioned, may have deprived
the elderly of advocates as conditions swiftly deteriorated.

“This pandemic has shone a spotlight on the overlooked and undervalued
corners of our society,” Kluge said.

He and other WHO officials who spoke Thursday said they did not have
enough data to say conclusively that people in nursing homes were
being transferred to hospitals less often than they should be, or that
they were being discharged from hospitals prematurely — fears raised
by advocates in Britain and elsewhere. But the WHO officials hinted
strongly that those factors might be contributing to the high death
rates.

Many countries in Europe have essentially ignored coronavirus testing
in nursing homes to focus their testing capacity on hospital patients
and hospital staffers. In Italy, for instance, a recent national
health service report indicated that people dying in nursing homes
were overwhelmingly unlikely to have been tested for the virus.

* 20200319, https://www.onetz.de/deutschland-welt/mitterteich/mitterteich-ausnahmezustand-stadt-coronahaft-id2997291.html

Der Unmut über die Gerüchteküche bei den Mitterteichern ist groß: "Ich
war ja selbst beim Starkbierfest", sagt Rosner, "das war am 7. März
überhaupt noch kein Thema." Jetzt höre man, dass eine einzige
Besucherin offiziell infiziert gewesen sein soll." Vielen Bürgern
scheint das nicht plausibel zu sein. "Angeblich waren 20 Mitterteicher
im Skiurlaub in Südtirol", nennt der Konnersreuther ein anderes
Gerücht. "Aber auch das lässt sich nicht nachvollziehen."

"Es ist nicht zielführend, wenn von offizieller Seite Vermutungen
rausgehauen werden, die nicht verifiziert werden können", sagt das
Stadtoberhaupt. "Die Infektionsketten sind meines Wissens noch nicht
identifiziert."

...

Besorgte Bürger melden sich auch bei Oberpfalz-Medien. Frau Lehner aus
Neusorg, den Vornamen will sie nicht verraten, hatte das Starkbierfest
in Mitterteich besucht. Die 40-Jährige sei ein Corona-Verdachtsfall,
momentan auf Anweisung des Gesundheitsamts unter Quarantäne. Sie habe
seit vier Tagen Fieber, fühle sich im Stich gelassen. "Bisher hat noch
kein Arzt vorbeigeschaut und einen Test gemacht", klagt sie am
Telefon. Es fühle sich keiner für sie zuständig.


"Es waren statt wie sonst 1400 Besucher vielleicht 1000 überwiegend
junge Leute da", glaubt Bürgermeister Grillmeier dennoch nicht an das
Fest als Wurzel allen Übels. Die seien aus dem ganzen Landkreis
gekommen, weshalb dann nicht nur Mitterteich betroffen sein dürfte.

"Von den 25 Corona-Fällen kenne ich 12 persönlich", sagt er. Von
keinem sei ihm bekannt, dass er auf dem Fest oder im Skiurlaub in
einem Risikogebiet gewesen sei. "Unser erster Fall trat am 9. oder
10. März auf", fährt er fort, zu kurz für Infektion und Ausbruch. "Ein
83-Jähriger, der sicher nicht am Fest war." Dasselbe gelte für einen
infizierten Mitarbeiter und eine Kindergärtnerin. Beide sind
ursächlich, dass im Rathaus auf Homeoffice umgestellt wurde und sich
der Kindergarten in Quarantäne befindet.




* 20200415, https://www.kreis-tir.de/buergerservice/aktuelles/news/news/detail/News/bundeswehr-schickt-helfende-haende/

Zwölf Senioren- und Pflegeheime im Landkreis Tirschenreuth werden
aktuell von 28 Soldaten der Bundeswehr unterstützt. Sie kommen vom
Logistik-Bataillon 472 aus Kümmersbruck.


* 20200326, https://www.sonntagsblatt.de/artikel/bayern/ein-ort-auf-schuldsuche-mitterteich-sind-mindestens-71-menschen-corona-erkrankt

So rückt wegen der hohen Zahl an Corona-Infizierten in Mitterteich
immer wieder ein Starkbierfest in den Mittelpunkt. Dazu gab es eine
erste Strafanzeige. Wie der Leitende Oberstaatsanwalt Gerd Schäfer
bestätigt, ist bei der Staatsanwaltschaft ein Strafantrag wegen des
Verdachts der fahrlässigen Körperverletzung eingegangen. Der
Antragsteller sei der Meinung, dass das Starkbierfest am 7. März nicht
mehr veranstaltet werden hätte dürfen.

Ausgangspunkt war eine Aussage von Ministerpräsidenten Markus Söder:
"Übrigens vermuten die Experten - das ist noch nicht endgültig
bestätigt - dass der Ausgangspunkt für die dort hohe Infektion ein
Starkbierfest gewesen ist."

...

Unterdessen bereitet Pfarrer Schlenk noch ein anderes Problem Sorgen:
Was wird mit den Todesopfern, fragte er. Wie können sich die
Angehörigen angemessen verabschieden? Ein 83-jähriger Mann, das wohl
erste Corona-Opfer in Mitterteich, soll zwei Wochen vor seinem Tod im
Koma gelegen haben. "Niemand durfte sich von ihm verabschieden." Der
tote Mann sei vom Krankenhaus direkt ins Krematorium nach Selb
transportiert worden. In Mitterteich wird man nach der Pandemie nicht
nur um den sozialen Frieden ringen müssen, sondern auch um den
seelischen.


* 20200323, https://www.onetz.de/oberpfalz/mitterteich/corona-anzeige-wegen-starkbierfest-mitterteich-id2999228.html

Ausgangspunkt für ihn ist die Aussage des Ministerpräsidenten Markus
Söder: "Übrigens vermuten die Experten, das ist noch nicht endgültig
bestätigt, dass der Ausgangspunkt für die dort hohe Infektion ein
Starkbierfest gewesen ist."

* 20200319, https://www.zeit.de/gesellschaft/zeitgeschehen/2020-03/mitterteich-ausgangssperre-coronavirus-quarantaene-ansteckungsgefahr-deutschland/komplettansicht



Bürgermeister Grillmeier widerspricht den Spekulationen, dass das
Starkbierfest schuld sei an der hohen Fallzahl. Er kenne die Hälfte
der Corona-Patienten aus Mitterteich, sagte er laut der
Frankenpost. Seines Wissens habe keiner davon das Fest besucht.


* 20200302, https://www.kreis-tir.de/buergerservice/aktuelles/news/news/detail/News/corona-77-bestaetigte-faelle-im-landkreis-erster-todesfall-aus-dem-landkreis-tirschenreuth/

Stadtgebiet Mitterteich

* 20200416, https://www.br.de/nachrichten/bayern/corona-patienten-aus-seniorenheim-erfolgreich-verlegt,RwLPEgg & https://www.br.de/nachrichten/bayern/corona-24-bewohner-eines-straubinger-seniorenheims-infiziert,RwII0yH

NOTE: no report on deaths there, but 24 asymptomatic cases
were transferred, perhaps indicating a major outbreak!?

Corona-Patienten aus Seniorenheim erfolgreich verlegt

Die Bewohner des Seniorenheims St. Nikola in Straubing, die positiv
auf Corona getestet worden waren, sind erfolgreich verlegt
worden. Alle 22 Heimbewohner sind wohlbehalten in ihren Einrichtungen
angekommen. 30 Helfer waren an der Aktion beteiligt.

Corona-Tests am Ostersonntag

Das Gesundheitsamt hatte am Ostersonntag 24 bisher symptomfreie
Bewohner des St. Nikola Heims positiv auf Covid-19 getestet. Zwei
Senioren wurden stationär in Kliniken untergebracht. Die anderen 22 in
sogenannten Behelfs-Einrichtungen im Raum Passau.


* 20200420, https://www.br.de/nachrichten/bayern/corona-im-seniorenheim-15-bewohner-verstorben,RwhPCIk

Corona im Seniorenheim: 15 Bewohner in Aschau verstorben

In einem Seniorenheim in Aschau im Chiemgau sind mittlerweile 15
verstorbene Bewohner zu beklagen. Wie der Heimleiter dem BR
bestätigte, sind damit seit den ersten positiven Corona-Tests in dem
Heim 15 der 70 Bewohner verstorben.

Ende März habe es die ersten positiven Corona-Tests in dem
Seniorenheim in Aschau im Chiemgau gegeben, so der Heimleiter im
Gespräch mit dem BR. Betroffen waren Mitarbeitern und Bewohnern. Dann
sei die Erkrankungswelle ins Rollen gekommen. Fünf schwere Fälle
seinen aus dem Heim in Krankenhäuser verlegt worden. 15 Bewohner seien
mittlerweile verstorben.

Zu wenig Schutzmasken und Schutzausrüstung

Vor allem hätten Schutzmasken und Schutzausrüstung gefehlt. Die
Zusammenarbeit mit den Behörden wird vom Heimleiter ausdrücklich
gelobt, er habe große Unterstützung erfahren, sagt er. Insgesamt zeigt
sich der Heimleiter tief bewegt und erschüttert.



Landratsamt äußert sich nicht zu den Todesfällen

Das Landratsamt Rosenheim will sich zu Todesfällen in Alten- und
Pflegeheimen nicht äußern, "zum Schutz der Betroffenen und der
Persönlichkeitsrechte", heißt es. Es sei grundsätzlich die Aufgabe der
Heimbetreiber, die Versorgung der Bewohner sicherzustellen.  Behörden
klären Personalbedarf

Das Gesundheitsamt, die Heimaufsicht sowie die Führungsgruppe
Katastrophenschutz stehen laut Landratsamt im engen Austausch mit den
60 Alten- und Pflegeeinrichtungen im Landkreis Rosenheim. Bei rund 30
habe es bereits vor Ort-Termine gegeben, die verbleibenden Heime
würden noch in dieser Woche besucht. Bei vier der über 60
Einrichtungen gab es laut Landratsamt Bedarf an Pflegekräften, um das
vorhandene Personal zu entlasten.

Drei Einrichtungen wurden bereits personell unterstützt, die vierte
Einrichtung bekommt diese Woche zusätzliche Pflegekräfte. "Das
zusätzliche Personal stammt aus eigener Organisation der Heime, aus
dem Pflegepool sowie vom Bayerischen Roten Kreuz", so das Landratsamt.


Unterstützung auch aus Nachbarlandkreis

Geholfen hat offenbar auch der Landkreis Ebersberg, der ebenfalls
Pflegekräfte in den Kreis Rosenheim für Alten- und Pflegeeinrichtungen
vermittelte und das "unkompliziert und schnell", so das Landratsamt
Rosenheim. Solange die Versorgung der Heimbewohner gewährleistet
werden könne, werde versucht, sie in ihrer gewohnten Umgebung zu
lassen.

* 20200420, https://www.ovb-online.de/rosenheim/landkreis/lebensabend-ausnahmezustand-rosenheimer-seniorenheime-corona-krise-reagieren-13645623.html

Lebensabend im Ausnahmezustand: Wie Seniorenheime auf die Corona-Krise reagieren

Ein Mangel an Schutzbekleidung, Panik bei Demenzkranken und Angst vor
einer Ausbreitung des Virus: Bewohner und Mitarbeiter in Alten- und
Pflegeheimen trifft die Corona-Pandemie besonders hart. Adelheid Lappy
aus Rosenheim bringt es auf den Punkt: „Was Pfleger gerade bringen,
ist psychische Hochleistung.“

Rosenheim - Am Telefon wird der Reporter der OVB-Heimatzeitungen Zeuge
eines freudigen Ereignisses. Gregor Kumberger unterbricht das Gespräch
und meldet sich kurz darauf erleichtert zurück. „Jetzt ist grad neues
Desinfektionsmittel angekommen – 15 Liter.“

Der Leiter von Haus Lohholz in Kolbermomor feiert mit der Lieferung so
etwas wie einen kleinen Sieg gegen den Mangel. Noch halten die Alten-
und Pflegeheimen in der Region das Wichtigste vorrätig – für den
Moment. Doch was ist, wenn sich Bad Feilenbach wiederholt? Was, wenn
Corona in den Mauern der Einrichtung festgestellt wird?

Wir haben noch eine Reserve“, sagt Kumberger, „aber wir müssen sparsam damit umgehen.“ Und für den Fall der Fälle hoffe man auf den Katastrophenschutz. Und natürlich müsse man die diversen Internetanbieter im Blick haben. Sein Desinfektionsmittel hat er von einem Anbieter, der sonst Hotels beliefert.
Ein Mangel an Masken

Vor allem einen Mangel an Masken mit zertifizierten Schutzklassen
stellt Ralf Schwärz vom Seniorenwohnheim Küpferling in Rosenheim
fest. Bestellen ist dieser Tage Glücksspiel. Schwärz erzählt von
jemandem, der in China bestellt hat. „Es hieß, die Lieferfrist betrage
eine Woche. Der wartet jetzt schon vier Wochen darauf.“ Bezahlt hatte
er natürlich schon. Franz Bachleitner vom Altenheim St. Konrad in
Wasserburg hat eine Maske, die sonst 39 Cent koste, für 10,49 Euro
angeboten gesehen. „Die Sorgen und die Ängste treiben die Preise“,
sagt der Heimleiter.

Demenzkranke leiden besonders

Was vielen, aber nicht allen zu schaffen mache, hat Adelheid Lappy,
Seniorenpastoral der Rosenheimer Stadtteilkirche am Zug,
festgestellt. „Es gibt alte Menschen, für die ändert sich gar
nichts. Sie leben alleine, sie sind gewohnt, alleine zu leben, sie
wollen das auch, und sie vermissen nichts. Besonders schlimm sei die
Situation dagegen für Menschen mit Demenz. „Die verstehen gar nicht,
was geschieht, wenn ein Pfleger mit Mundschutz auftaucht. Das verwirrt
sie, das macht ihnen Angst.“

* 20200401, https://www.ovb-online.de/weltspiegel/bayern/coronavirus-bad-feilnbach-seniorenheim-evakuierung-st-josef-13636148.html

Zur Stunde werden die Bewohner des Hauses St. Lukas in Bad Feilnbach
verlegt. In dem Seniorenheim sind außergewöhnlich viele
Coronavirus-Fälle aufgetreten. Es läuft eine groß angelegte Aktion.

Wie das Landratsamt Rosenheim nun mitteilt, sind zahlreiche
Mitarbeiter sowie die Leitung des Seniorenheimes St. Lukas in
Quarantäne. Sie sind mit dem Coronavirus infiziert. Auch ein Großteil
der Bewohner wurde inzwischen positiv getestet. Insgesamt wurden 41
Personen verlegt, 31 davon sind nachweislich infiziert.

Um eine Versorgung sicherzustellen, wurden die Bewohner im Medical
Park Bad Feilnbach Reithofpark, im Medical Park Bad Feilnbach
Blumenhof, in der Schön Klinik Bad Aibling Harthausen und in der
Rheumaklinik Bad Aibling untergebracht.

An dem Einsatz beteiligt waren Kräfte der Kreisverbände Rosenheim und
Miesbach des Bayerischen Roten Kreuzes, der Ambulanz Rosenheim, des
Malteser Hilfsdienstes Rosenheim, der Johanniter Wasserburg sowie von
Polizei und Feuerwehr Bad Feilnbach.

Die Erstmeldung zum Einsatz in Bad Feilnbach

Bad Feilnbach – Die Verlegung der Bewohner des Seniorenheimes
St. Lukas in Bad Feilnbach läuft seit Mittwochmorgen, 8 Uhr. Mit
voller Wucht traf das Coronavirus das Bad Feilnbacher Altenheim. Eine
aktuell noch nicht genannte Zahl an Mitarbeitern und Bewohnern ist
dort an COVID-19 erkrankt, die Versorgung sei laut Heimaufsicht nicht
mehr gesichert.

Auf der Apfelmarktwiese am Ortsrand von Bad Feilnbach stehen rund 25
Krankenwägen. Kräfte der FüGK-Katastrophenschutzgruppe sind vor Ort
und koordinieren unter ärztlicher Leitung die Verlegung der Patienten.

Bewohner kommen auf Isolierstationen

Dazu haben die Bad Feilnbacher Medical Park Kliniken Blumenhof und
Reithof jeweils 15 Betten auf ein kurzfristig eingerichteten
Isolierstationen bereitgestellt, nachdem es erste Hinweise auf
möglichen Handlungsbedarf bereits am Sonntag gegeben habe.

* 20200407, https://www.onetz.de/oberpfalz/erbendorf/caritas-schlaegt-alarm-altenheime-immer-haeufiger-virusfalle-id3006306.html

Senioren sind durch Corona besonders gefährdet. Weil Ausrüstung fehlt,
wird das Virus vor allem in Seniorenheimen immer mehr zum Problem.

Das Coronavirus wird zur Todesgefahr für immer mehr Bewohner in
Altenheimen. In jeder zehnten Einrichtung in Bayern wurden Bewohner
oder Mitarbeiter positiv getestet, das hat Gesundheitsministerin
Melanie Huml am Dienstag erklärt.

Erst vor einigen Tagen schockte die Nachricht aus einem Seniorenheim
in Windischeschenbach, in dem drei Senioren verstorben waren. Zig
Bewohner und Mitarbeiter waren dort zudem positiv auf den Erreger
getestet worden. Am Montag gab es auch in Erbendorf im Caritas-Heim
einen Großeinsatz. Feuerwehrleute wurden dort zu Hilfe gerufen, um
gesunde von infizierten Patienten zu trennen. Ebenfalls am Montag gab
es in einem Heim des Roten Kreuzes in Hirschau einen größeren
Einsatz. Während bei den Problemen in Windischeschenbach das
Landratsamt Neustadt/WN die Öffentlichkeit durch eine Mitteilung
informierte, hält sich das Amt in Tirschenreuth bisher zurück. Auch zu
Gerüchten über angebliche Todesfälle in Heimen gibt es keine
Informationen. Das Rote Kreuz bestätigt zu Hirschau, dass dort "neun
Mitarbeiter und vier Bewohner positiv auf Covid-19" getestet worden
seien. Derzeit liefen weitere Tests.

Brandbrief aus Regensburg

Einblick bietet ein Brief, den der Regensburger
Diözesan-Caritas-Direktor Diakon Michael Weißmann ans
Gesundheitsministerium geschickt hat. In dem Brandbrief beschwert sich
der Diakon, dass die Pflege in Sachen Schutzausrüstung lediglich an
zweiter Stelle hinter den Krankenhäusern stehe. Während die Zahl der
Erkrankten stetig steige, fehle in immer mehr Heimen die
Schutzausrüstung für die Pflegekräfte.

Eine Befragung der Caritas-Einrichtungen im Bistum habe ergeben, dass
40 Prozent der ambulanten Pflegedienste und 60 Prozent der stationären
keine Schutzausrüstung erhalten haben.

* 20200406, https://www.onetz.de/oberpfalz/erbendorf/kampf-gegen-coronavirus-mehreren-altenheimen-landkreis-tirschenreuth-id3005811.html

In einigen Häusern im Landkreis Tirschenreuth sollen sowohl Bewohner
als auch Mitarbeiter betroffen sein. Die Erbendorfer
Caritas-Einrichtung St. Marien verlegt kranke Senioren in anderen
Trakt.

Eines der betroffenen Häuser ist das Caritas-Seniorenheim St. Marien
in Erbendorf. Mitte März gab es dort bei einem Bewohner einen ersten
Verdachtsfall. Ende März traf dann das Testergebnis ein.

Mangel an vielen Dingen

Die Verantwortlichen der Behörde haben sich jedoch im Landkreis
Tirschenreuth darauf verständigt, keine konkreten Fallzahlen zu
einzelnen Häusern herauszugeben. So bleibt vieles Spekulation. Fakt
ist allerdings, dass es zumindest in verschiedene Seniorenheimen im
Landkreis an wichtigen Dingen mangelt. Landauer betont, dass vieles
leider nicht in dem Umfang verfügbar seien, wie man es
bräuchte. Angefangen von Schutzmasken über Handschuhe bis hin zu
Desinfektionsmitteln gebe es mit der Versorgung Probleme. Und diese
Dinge seien einfach wichtig, um die Ansteckungsgefahr zu verringern,
sagt Landauer. Altenpflege sei leider ebenso wenig wie Krankenpflege
kontaktlos nicht zu bewältigen.


* 20200414, https://www.onetz.de/oberpfalz/tirschenreuth/corona-hotspot-landkreis-tirschenreuth-hohe-todesrate-noch-raetsel-id3009075.html

Der Landkreis Tirschenreuth ist mit fast 1000 Corona-Infizierten und
über 60 Covid-19-Toten momentan der Corona-Hotspot in
Deutschland.

...

Spielen Altenheime eine besondere Rolle in der Statistik? In einem
Heim in Erbendorf wurden in Absprache mit dem Gesundheitsamt in einer
Hau-Ruck-Aktion die Bewohner verlegt. Auch von anderen Seniorenheimen
wird von massiven Problemen berichtet.

„Das ist richtig. Im Landkreis sind mehrere Seniorenheime betroffen“,
bestätigte Lippert. Daher seien in einigen Einrichtungen Bewohner und
Mitarbeiter getestet worden. Nach entsprechenden positiven Tests seien
Maßnahmen ergriffen worden, beispielsweise die Verlegung von Bewohnern
innerhalb des Heims.

Das Landratsamt werde aber kein Altenheim namentlich nennen. „Das
Gesundheitsamt sagt, das sollte man aus zwei Gründen auf keinen Fall
tun: Man schafft Ängste innerhalb der Mitbewohner und verunsichert das
Personal“, erläuterte der Landrat. Ein Träger könne die Zahlen aber
natürlich herausgeben. „Daran werden wir ihn nicht hindern.“
Regierungsdirektorin Kestel verwies zudem auf den Datenschutz als
weitere Hürde für ihre Behörde.

....

Verantwortliche von Altenheimen erzählen hinter vorgehaltener Hand,
dass ihnen nicht nur Personal, sondern auch Schutzausrüstung fehlt.

Wöchentlich werde auch der Bedarf der Einrichtungen abgefragt. In
diesem Zusammenhang hatte der Landrat auch zwei Neuigkeiten: Für 12
Pflegeeinrichtungen im Landkreis gibt es ab sofort Unterstützung durch
28 Soldaten aus Kümmersbruck. Bis zum 12. Juni helfen sie bei
logistischen Tätigkeiten. Zudem habe Landtagsabgeordneter Tobias Reiß
dafür gesorgt, dass der Landkreis zusätzliche Schutzkittel
bekomme. „Die sind absolut nötig“, so der Landrat.

Regina Kestel bekräftigte, dass der Landkreis momentan vor allem
Schutzkittel und Schutzanzüge für Altenheime, Klinikum, Haus- und
Zahnärzte braucht. Momentan warte die Behörde seit 14 Tagen auf eine
Bestellung. In jedem Altenheim gebe es zudem mittlerweile einen
Pandemie-Beauftragten. Dazu wurden Konzepte für diese Einrichtungen
erarbeitet, die das Vorgehen nach positiven Coronatests
festlegen. Außerdem verwies die Regierungsdirektorin darauf, dass nach
und nach in allen Altenheimen die Bewohner und das Personal
vorsorglich auf Covid 19 getestet werden. Dies übernimmt die Task
Force Infektiologie des Landesamts für Gesundheit und
Lebensmittelsicherheit (LGL) zusammen mit dem Gesundheitsamt (wir
berichteten). „Da der Landkreis Tirschenreuth ein Corona-Hotspot ist,
wollen wir das vorausschauend planen und frühzeitig reagieren. Wir
sind die ersten in Bayern“, erklärte Kestel.

Abschließend betonte Landrat Lippert, dass der Landkreis bei der
Ursachenforschung und Eindämmung der Corona-Pandemie nicht alleine
gelassen werde. Es gebe viel Unterstützung.

* 20200416, https://www.newyorker.com/news/news-desk/from-the-streets-to-the-nursing-homes-how-spains-coronavirus-infection-rate-became-one-of-the-worlds-highest

Health workers make up an estimated fifteen per cent of Spaniards who
have been infected; to date, at least twenty-three doctors, a nurse,
and a nursing assistant have died from COVID-19. Uclés told me that
several of his colleagues had also shown mild symptoms, including a
loss of smell or taste, but they hadn’t been able to access a
test. “Authorities know it will likely turn out positive,” he
said. “That’s counterproductive, because it means the medical staff
will have to be sent home. And where do you find a hundred doctors?”

Spanish officials have insisted that all health workers were
guaranteed testing, but the doctors and nurses I spoke to claimed
otherwise. Gabriela Lobel, a nurse in Madrid, said that test kits were
in short supply and that they were offered only to workers with clear
symptoms. “The system will ultimately collapse,” she told me, “not
just because there won’t be enough hospital beds but because there
won’t be enough workers to look after them.”

From the onset of the pandemic, Spain’s limited testing capacity has
been a core problem. Health officials have struggled to identify and
properly isolate infected individuals showing no symptoms but who may
be unwittingly spreading the virus. On March 9th, four medical
associations sent a letter to the Spanish Health Ministry urging
authorities to prioritize early detection. In response, Salvador Illa,
the Health Minister, announced that thirty thousand people had been
screened.

First, the government ordered more than half a million tests made in
China, which proved to be faulty. In an effort to make up for that
setback and its faltering response over all, the government spent more
than nine hundred million dollars, the Health Ministry recently
announced, on tests, ventilators, and masks, but the equipment has
been slow in coming.

Years of budget cuts also curtailed the resources available to Spanish
health workers. In the past decade, Spain’s investment in health care
has dropped to 5.9 per cent of the gross domestic product—nearly two
points lower than the average spending of other E.U. members. As a
result, there are only forty-four hundred intensive-care beds in the
country, a number that pales in comparison with Germany’s twenty-eight
thousand I.C.U. beds.

Husamidden Kharat, an anesthesiologist who works at the Rey Juan
Carlos Hospital, in Madrid, told me that patients over the age of
seventy-five were no longer being admitted into I.C.U.s. “They are
left to die,” he said. “It’s very difficult, but it is the reality.”
Kharat said that the number of available beds at his hospital had
nearly doubled and a makeshift intensive-care unit had been built, but
ventilators remained in short supply, as did protective gear for
workers. Kharat said that he had got used to wearing the same mask for
at least two weeks and reusing his robe again and again.


In Madrid, the epicenter of Spain’s outbreak, close to three thousand
deaths have been reported in nursing homes. Spaniards were recently
shaken by news of military members entering nursing homes and finding
bodies unattended for days. The military is now overseeing some of
those centers, and public prosecutors are looking into possible
criminal charges.

Marta Cano, a primary-care physician in the northern city of
Valladolid, told me that she has delivered hospital care in a nursing
home for several weeks. About a hundred residents live in the center
where she currently works. At least twenty died before she arrived
there and twenty-three are now under observation for coronavirus
infection.

She blamed the government for downplaying the dangers of the
virus. “It was cast as a flu,” she said. Some of the elderly people
who had died early on had shown no symptoms and gone untested. “They
died all of a sudden,” she said. “Some of the residents who are holed
up in their rooms don’t quite know how many of their neighbors have
passed. But then there’s the couples, who’ve lost a husband, or a
wife, and are now all alone.”

Lobel, the nurse based in Madrid, said that primary-care facilities,
where patients receive routine medical treatment, are overwhelmed, as
well. “When people talk about a collapse in the system, they generally
think of an overcrowded hospital,” Lobel told me. “But, because only a
small percentage of coronavirus patients are hospitalized, the role of
primary-care centers should not be overlooked.” For the past several
years, Lobel has worked part time in a public primary-care center. Her
duties at the center now include checking in with patients who are
quarantined at home and determining if they need hospital care.

“We know what’s cooking inside hospitals, so we’ve tried to limit the
number of transfers as much as possible,” she said. Lobel also works
in the emergency room of a small private hospital in Madrid where,
until a few days ago, protective gear was so scarce that she had been
using trash bags to cover her body. At her primary-care center, she
used equipment left over from the 2014 Ebola crisis. A week after we
first spoke, Lobel developed coronavirus symptoms, and she is now
quarantined at home, grappling with a sense of guilt over not being on
the front line. “I feel as if I had been ordered off the field,” she
said.



* 20200413, https://www.theguardian.com/world/2020/apr/13/half-of-coronavirus-deaths-happen-in-care-homes-data-from-eu-suggests

Snapshot data from varying official sources shows that in Italy,
Spain, France, Ireland and Belgium between 42% and 57% of deaths from
the virus have been happening in homes, according to the report by
academics based at the London School of Economics (LSE).

...

On Monday Prof Chris Whitty, the chief medical officer for England,
said 13.5% of the UK’s care homes had a confirmed case of coronavirus
among their residents, up from 9% last week, with 92 more homes
detecting cases in the previous 24 hours.

Ireland where, as of Saturday, 54% of deaths from coronavirus occurred
in care homes, according to centralised government figures.

In Italy, a government survey covering deaths in 10% of the nations’
care homes suggested 45% of all deaths in that country could be in
care homes, while central government data in France released at the
weekend showed the same proportion of the 13,832 deaths in that
country happened in care. In Belgium, health ministry figures showed
42% of Covid-19 deaths happened in care homes, and in Spain leaked
regional government data suggested 57% of the country’s death toll
from 8 March to 8 April was from care homes.


* 20200416, https://sverigesradio.se/sida/artikel.aspx?programid=2054&artikel=7453417
    - Sweden counts deaths in care homes, while many other countries don't

* 20200402, https://sverigesradio.se/sida/artikel.aspx?programid=2054&artikel=7444103
    - Sweden: 94 of Sweden's 290 local municipalities [had or suspected to have] coronavirus cases in a local care home

* 20200403, https://www.bbc.com/news/world-europe-52147861 :
    - France has revealed 1,416 residents have succumbed to the virus since the epidemic began,
    - Cases have also been reported in 100 care homes around the Swedish capital, public broadcaster SVT says more than 400 people have been infected and about 50 have died. 
    - In Spain, where 10,905 people have died in the pandemic, the Madrid region has been worst affected with 4,483 deaths. The president of the Madrid region, Isabel Díaz Ayuso, estimates that 3,000 people died in care homes in March and says that figure is 2,000 higher than normal. The Vitalia home in Leganés, on the outskirts of the capital, has had a reported 43 deaths, while another 46 have died in the Reina Sofía de Las Rozas home. Coffins were removed from the home in Leganés on Thursday. Of the 150 residents there, 99 have tested positive for the virus, Efe news agency reports.
    - Italy: Hundreds of deaths in residential homes were reported earlier on in the crisis in northern Italy, and infections have spread in homes further south in Naples. Health workers in protective gear on Thursday moved elderly residents to hospital from La Casa Di Mela rest home, in an attempt to curb the spread of Covid-19. The sister of one resident told local media that the home's managers had been pleading for tests to be carried out and it was only after someone died that they took notice.

* 20200415, https://www.ansa.it/english/news/general_news/2020/04/15/police-raid-lombardy-govt-on-care-homes-3_eca675f9-ab84-4b0b-a892-e4202904798b.html
    - (ANSA) - Milan, April 15 - Italian tax police on Wednesday raided
    the offices of the Lombardy regional government in a probe into
    the coronavirus-related deaths of elderly residents of care homes
    across the northern region.  The probe is centred on the Pio
    Albergo Trivulzio care home in Milan, where 143 residents have
    died since the start of the virus emergency. It also involves
    other care homes in Milan and across the region.  Finance police
    said they were looking for directives from the regional government
    on these homes, where scores of residents have died and directors
    are suspected of culpable negligence.  NAS health and hygiene
    police said Wednesday that of 600 care homes inspected across
    Italy, 17% of them had been found to present irregularities.
    Norms were not respected in 104 centres, and 15 of them closed
    down. Patients were moved elsewhere.

* 20200416, https://www.theguardian.com/world/2020/apr/16/italian-police-broaden-care-home-coronavirus-milan
    - Italian authorities have broadened their investigation into care home deaths during the coronavirus outbreak after 190 people were reported to have died at Milan’s largest care home. Police on Wednesday seized documents connected to Pio Albergo Trivulzio, which has more than 1,000 elderly residents, from the offices of the Lombardy regional authorities amid what has been described as a “massacre”. There is no official data on how many residents have died in Trivulzio since the coronavirus outbreak, although the newspaper reported on Thursday that there were more than 190 deaths during March and the first half of April. Silvio Brusaferro, the chief of the Higher Health Institute, said last Friday that 1,822 people had died across all care homes in Lombardy, but it is unclear how many were killed by Covid-19 as many were never tested. The manager of Trivulzio is under investigation amid allegations that no safety precautions were adopted and that care home staff were not allowed to wear face masks, at least in the early stage of the outbreak, in case they frightened the residents. About 200 staff have been infected with the virus, according to Rossella Delcuratolo from the Cisl labour union. Meanwhile, under a regional measure, 17 patients who were being treated for other illnesses at a hospital in Milan were transferred to the care home in March to free up beds in packed wards. “After these 17 people arrived, the first coronavirus cases emerged,” Delcuratolo said. “They said they were not positive but they couldn’t have known as they were not tested for coronavirus. The [home] has been mixing patients who are sick with those who are well, and this is why people are dying. It’s a massacre that could have been avoided if they had listened – on 11 March I asked for all staff to wear face masks, but it didn’t happen.”


* 20200321, https://www.nytimes.com/2020/03/21/us/coronavirus-nursing-home-kirkland-life-care.html
    - USA: 2/3 of residents and 47 workers ill, 35 died

* 20200417, https://www.nytimes.com/2020/04/17/nyregion/new-york-nursing-homes-coronavirus-deaths.html
    - At least 14 nursing homes in New York City and its suburbs have recorded more than 25 coronavirus-related deaths, according to new data from the state Health Department that shows the virus’s impact on individual facilities, and five had 40 or more. The homes that have been hit hardest are the Cobble Hill Health Center, a 364-bed nonprofit facility in Brooklyn, which reported 55 deaths, and the Kings Harbor Multicare Center, a for-profit, 720-bed facility in the Bronx, which had 45.  Until the state released its partial accounting on Friday, families with relatives in nursing homes had repeatedly been denied information, which was unavailable from official sources and often impossible to get from the homes themselves. Complaints from family members about a lack of disclosure from homes started in mid-March, when visitors were barred from entering facilities.
    
* 20200417, https://www.nytimes.com/2020/04/17/us/coronavirus-nursing-homes.html
    - About a fifth of U.S. virus deaths (~7000= are linked to nursing facilities. Now a nationwide tally by The New York Times has found the number of people living in or connected to nursing homes who have died of the coronavirus to be at least 7,000, far higher than previously known. In New Jersey, 17 bodies piled up in a nursing home morgue, and more than a quarter of a Virginia home’s residents have died. At least 24 people at a facility in Maryland have died; more than 100 residents and workers have been infected at another in Kansas; and people have died in centers for military veterans in Florida, Nevada, New York, Maine, Massachusetts, Oregon and Washington. On Friday, New York officials for the first time disclosed the names of 72 long-term care facilities that have had five or more deaths, including the Cobble Hill Health Center in Brooklyn where 55 people have died. At least 14 nursing homes in New York City and its suburbs have recorded more than 25 coronavirus-related deaths. 

* 20200411, https://www.nytimes.com/2020/04/11/nyregion/nursing-homes-deaths-coronavirus.html

* 20200415, https://eu.lohud.com/story/news/coronavirus/2020/04/15/how-ny-nursing-home-covid-19-secrecy-impacted-thousands-lives/2991464001/
    -  The coronavirus has killed more than 2,722 people living in such facilities, or about 25% of all virus-related deaths statewide [New York]. Yet the names of hundreds of nursing homes with coronavirus outbreaks remain a mystery to the public, leaving advocates, relatives and loved ones of some of the most vulnerable New Yorkers in the dark. Health Commissioner Dr. Howard Zucker on Monday said the decision to withhold the nursing home names was based on privacy concerns linked to the federal Health Insurance Portability and Accountability Act of 1996, known as HIPAA.
    - In many ways, the lack of early public reporting of outbreaks in nursing homes hindered the public health response, advocates and experts said. ... Mollot suggested authorities in New York and other states should have relocated and isolated more infected nursing home patients into COVID-19 only sites. ... Asked about the thousands of nursing home deaths due to the virus, Cuomo on Tuesday said “you cannot stop it” because of the existing health issues among the elderly and disabled in the facilities. ... He asserted COVID-19 exposed long-standing problems at nursing homes in New York and other states, spanning everything from insufficient staffing to poor infectious-control practices. ... Brian Lee, executive director of the advocacy group Families for Better Care, asserted many states withheld details about COVID-19 in nursing homes because officials were “trying to protect providers from potential litigation in a post-pandemic world.”

* 20200408, https://eu.lohud.com/story/news/coronavirus/2020/04/08/ny-plans-release-covid-19-racial-demographic-data-amid-concerns/2963727001/
    - about 18% of novel coronavirus deaths in New York, excluding New York City, were black people and 14% were Hispanic people, revealing the virus is disproportionately killing people of color. ... black people account for about 9% of the state's population outside New York City, and Hispanic people account for 11%. ...  the majority of New York’s more than 5,489 deaths due to coronavirus were among men, and 86% of all deaths were among people who had underlying illnesses, such as hypertension and diabetes. ... Outside New York City, 62% of the deaths due to COVID-19, the disease caused by the coronavirus, were white people, who represent about 75% of the population, the new data show. Asian people accounted for 4% of the COVID-19 deaths, which is the same as their population percentage outside New York City. ....  data for New York City showed 34% of COVID-19 deaths were Hispanic people, who represent 29% of the city population. Black people represented 28% of COVID-19 deaths and 22% of the city population, the data show. Asian people represented 7% of COVID-19 deaths and 14% of the city population, the data show. ... Illinois, for example, reported that 3,607 of its confirmed COVID-19 cases were black people, state data show. That's nearly 30% of the state's cases, despite the fact only about 15% of its population is black or African American, U.S. Census data show. Further, 129 of Illinois' coronavirus deaths, or 42%, were black people, the data show. ... In New Jersey, state officials reported information on the race of those who died for the first time Monday. Of the statewide deaths, 60% were white, 24% black, 5% Asian and 11% other, the USA TODAY Network reported. In contrast, about 15% of New Jersey's population is black, U.S. Census data show.

* 20200414, https://eu.northjersey.com/story/news/coronavirus/2020/04/14/coronavirus-nursing-homes-northeasts-achilles-heel-deaths-soar/2984933001/
    - More than 2,400 nursing home and assisted living facility residents have died in New York from the coronavirus, making up 24% of the state's total fatalities. New Jersey’s 252 nursing home fatalities as of last week accounted for one in eight of the state’s deaths. - TODO: get data from the article

* 20200416, https://www.diepresse.com/5800750/wie-missshymanagement-und-vertuschung-zum-massenshysterben-in-der-lombardei-fuhrten


* 20200411, https://www.vrt.be/vrtnws/en/2020/04/11/covid-19-in-116-brussels-care-homes/

COVID-19 in 116 Brussels care homes

Coronavirus cases have been diagnosed in 116 of the 146 care homes for
the elderly in Brussels.  194 deaths of patients who tested positive
for COVID-19 have been recorded.  A further 254 care home residents
are thought to have died from the disease.

Brussels care homes employ 7,000 workers.  568 are currently ill or
absent.  Since 22 March care homes in the capital have een receiving
support from Doctors without Borders MSF. 46 care homes are currently
benefiting from this support.

The defence department too is providing support.  Since Wednesday
military staff have been at work at the Jette care home.  Military
staff set to work at the Jean Van Aa home in Elsene today.


* 20200422, https://www.mbs.news/c/2020/04/3500-medics-in-turkey-are-infected-with-coronavirus-labor.html

According to the announcement, 3474 health workers were infected with
Covid-19, including 1307 doctors. Twenty-four of the infected,
including 14 doctors, have died.

The new coronavirus epidemic has claimed the lives of more than 2,200
people in Turkey, with more than 95,000 reported cases, nearly half of
them in Istanbul, according to recent official figures released last
night.

A statement from the Union of Doctors in Turkey states that the bulk
of the infected healthcare workers work in the metropolitan Istanbul,
home to about 16 million people.



## Pollution

* 20200420, https://www.theguardian.com/environment/2020/apr/20/air-pollution-may-be-key-contributor-to-covid-19-deaths-study?CMP=Share_AndroidApp_News_Feed

Air pollution may be ‘key contributor’ to Covid-19 deaths – study 

The analysis shows that of the coronavirus deaths across 66
administrative regions in Italy, Spain, France and Germany, 78% of
them occurred in just five regions, and these were the most polluted.

* 20200407, https://www.theguardian.com/environment/2020/apr/07/air-pollution-linked-to-far-higher-covid-19-death-rates-study-finds

Air pollution linked to far higher Covid-19 death rates, study finds

The study, by researchers at the Harvard TH Chan School of Public
Health in Boston,analysed air pollution and Covid-19 deaths up to 4
April in 3,000 US counties, covering 98% of the population. “We found
that an increase of only 1μg/m3 in PM2.5 [particles] is associated
with a 15% increase in the Covid-19 death rate,” the team concluded.

A small increase in exposure to particle pollution over 15-20 years
was already known to increase the risk of death from all causes, but
the new work shows this increase is 20 times higher for Covid-19
deaths.

“The results are statistically significant and robust,” they said. The
study took account of a range of factors, including poverty levels,
smoking, obesity, and the number of Covid-19 tests and hospital beds
available. They also assessed the effect of removing from the analysis
both New York City, which has had many cases, and counties with fewer
than 10 confirmed Covid-19 cases.



* 20200110, https://www.srf.ch/news/international/massive-schadstoffbelastung-nirgendwo-erkranken-so-viele-wegen-smog-wie-in-norditalien
    - Nirgendwo in der EU werden so viele Leute wegen Smog krank oder sterben gar frühzeitig wie in Norditalien. Die Beweisführung, dass jemand tatsächlich an Smog erkrankt oder gar gestorben ist, ist aber schwierig und es fehlen konkrete Zahlen. Experten gehen trotzdem davon aus, dass die regelmässige, hohe Schadstoffbelastung in der Po-Ebene die Lebenserwartung vieler Personen verkürzt.



## Opinions against the lock-down

* https://www.nzz.ch/feuilleton/corona-krise-wie-absurd-ist-denn-das-alles-ld.1552008

* https://www.hebbel-am-ufer.de/hau3000/vom-virus-lernen/




## Health-Care System Overload - Flu Seasons

* https://off-guardian.org/2020/04/02/coronavirus-fact-check-1-flu-doesnt-overwhelm-our-hospitals/

several articles on previous flu seasons linked


* https://dfw.cbslocal.com/2018/01/08/hospital-overrun-by-flu-cases-having-to-turn-them-away/

DALLAS (1080 KRLD) – The flu continues to spread across north
Texas. And, one hospital in North Texas wants people to make sure
their flu is life-threatening before they take a trip to the emergency
room.


* https://www.statnews.com/2018/01/15/flu-hospital-pandemics/

A severe flu season is stretching hospitals thin. That is a very bad omen


* https://www.latimes.com/local/lanow/la-me-ln-flu-demand-20180116-htmlstory.html

California hospitals face a ‘war zone’ of flu patients — and are
setting up tents to treat them

* https://www.theguardian.com/society/2017/dec/21/nhs-cancels-surgery-tens-of-thousands-avoid-winter-crisis

NHS cancels surgery for tens of thousands to avoid winter crisis
This article is more than 4 years old

Hospital chiefs are told by NHS England to take drastic action, including setting up makeshift wards

Tens of thousands of patients are having their surgery cancelled and hospitals will set up makeshift wards in a dramatic escalation of the NHS’s efforts to avoid the service going into meltdown this winter.

In an unprecedented edict, NHS England has told hospitals to delay operations such as cataract removals and hip and knee replacements until mid-January. The only exceptions to the new policy are cancer operations and also what the NHS called “time-critical procedures” where the surgery has to go ahead to avoid the patient’s condition deteriorating further.

The move, which reflects growing anxiety among NHS bosses about how difficult this winter may prove, is intended to help hospitals cope with an impending influx of patients being admitted as emergencies due to the colder weather.



* https://www.theguardian.com/society/2019/dec/02/nhs-winter-crisis-extra-beds-created-by-52-per-cent-of-uk-hospitals

The Guardian disclosed last week that hospitals in England have the
smallest number of beds available on record.

* https://www.theguardian.com/politics/2019/nov/25/hospital-beds-at-record-low-in-england-as-nhs-struggles-with-demand

More than 17,000 beds have been cut from the 144,455 that existed in 2010

The number of hospital beds has fallen to its lowest level ever,
despite the head of the NHS warning that bed closures have gone too
far.

The health service in England has cut so many beds in recent years
that it has just 127,225 left to cope with the rising demand for care,
which will intensify as winter starts to bite.

In total, 17,230 beds have been cut from the 144,455 that existed in
April-June 2010, the period when the coalition Conservative/Liberal
Democrat government took office and imposed a nine-year funding
squeeze on the NHS, even though critics cautioned against it because
of growing pressures on the service.

* The Times, https://archive.is/2eKCW
Coronavirus: Record weekly death toll as fearful patients avoid hospitals

England and Wales have experienced a record number of deaths in a
single week, with 6,000 more than average for this time of year.

Only half of those extra numbers were attributed to the
coronavirus. Experts said they were shocked by the rise, particularly
in non-Covid-19 deaths, and expressed concern that the lockdown might
be having unintended consequences for people’s health.

There are fears that patients are not seeking help for
life-threatening conditions, including heart attacks, because they are
worried about catching coronavirus in hospital.  Experts said that
conditions such as diabetes or high blood pressure may also be proving
harder to manage during the lockdown.  The latest figures are for the
week ending April 3. They record certified deaths throughout the
country, rather than only those recorded in hospitals.


# Indirect Effects of Coronavirus Crisis

* https://www.theguardian.com/world/2020/apr/16/coronavirus-concern-heart-attack-stroke-patients-delay-seeking-help

* https://www.nytimes.com/2020/04/06/well/live/coronavirus-doctors-hospitals-emergency-care-heart-attack-stroke.html

* https://www.theguardian.com/world/2020/apr/21/global-hunger-could-be-next-big-impact-of-coronavirus-pandemic



## Health Care Funding

* https://www.politico.eu/article/europe-health-care-systems-on-life-support-special-report-drug-pricing-medicines-public-services/


Doctors have been threatening massive strikes in Britain to protest
pay and conditions. Italian regions are going bankrupt trying to fund
medicines. Drugmakers are pulling diabetes drugs from Germany, blaming
government-set prices that don’t let them recoup their investment.

The graying of Europe alone may push up public spending on health and
long-term care in the EU by as much as 10 percent of GDP by 2060, the
Commission has said.

But state-funded health care systems “can survive with the right
policies,” said Chris James, economist and health policy analyst at
the OECD.

This includes moves towards more efficiency in delivering health care
and a focus on spending only on measures with proven results,
according to health officials, economists and drugmakers alike.



Patients suffering from multiple chronic diseases “need a lifelong
relation[ship] with the health care system and they have problems that
are much more costly than other patient groups,” Sweden’s Health
Minister Gabriel Wikström said. The health care system has to stop
treating one disease at a time and be more integrated so it can focus
on patients who often suffer from multiple diseases at once, Wikström
added.

In recent years, countries have tried to influence behavior and make
an extra buck by introducing so-called sin taxes on products including
sweets and alcohol. Hungary, the U.K. and Latvia are part of the
trend.

The idea takes aim at the 86 percent of deaths in Europe from chronic
diseases often caused by unhealthy behavior: smoking, drinking, a poor
diet and lack of physical activity, according to WHO data.


“Whenever countries have a shortfall in funds – look at the financial
crisis from 2008 – the first thing that gets cut is health promotion
and prevention,” the OECD’s James said.

The move is shortsighted, he said, as it will result in bigger health
care costs down the road.

The privatization route

European systems generally draw from taxes on employment or general
tax revenues to finance health care. In the Netherlands and
Switzerland, health systems are financed from a mix of compulsory
public and heavily regulated private insurance.

It doesn’t make the system cheaper for the public, according to James

...

Some governments are looking at linking the level or speed of care to
people’s lifestyle and bad habits.

Obese or smoking patients would have been put at the bottom of the
waiting list for surgeries that were not threatening their life in the
U.K.’s Vale of York region, under a recent proposal. Opposition to the
plan erupted before it was put into practice and the National Health
Service halted it for review.

German bliss ... for now

Germany’s health system is at the moment an outlier among its peers, with its economy booming amid high employment, said Evert Jan Van Lente, director of EU affairs at the German health fund AOK Bundesverband.

Patients don’t have to go to a primary care physician before heading to the hospital or specialist doctors when they are sick, Van Lente said, though health economists consider that the way to control costs.

Germany reimburses every new drug approved by the European Medicines Agency.

But if its health technology assessment system does not find a new
drug to have an added benefit compared to an existing one, the new
drug will be reimbursed at the same level as the existing one.


For cancer drugs, comparable therapies tend to be pricey, so that
keeps cancer drug prices high — too high for insurers.

“I am not afraid of the survival, but of the sustainability of the
system as it is now. There is no political will to change something if
there is enough money,” Van Lente said.

At the other end of the spectrum are diabetes drugs, which are
measured against standard therapy available in much cheaper generic
form. That's led big drugmakers like Novo Nordisk to pull their
diabetes drugs from the market.

Van Lente backs a ceiling on expensive cancer drugs in Germany.

“In Cyprus, there are seven hospitalizations per inhabitant, while in
Austria there are 20. If you look at aging and other factors, there is
no reason to explain the difference.”

Similarly, women spend on average only a few hours in the hospital
when delivering a baby in the U.K., while those giving birth in
Belgium can stay in the hospital for five days.

* 2013, https://www.ncbi.nlm.nih.gov/books/NBK447869/

The impact of the crisis on the health system and health in Belgium

However, in 2012, the new government had to implement a package of
austerity measures to make €11.3 billion worth of public sector
savings, of which €2.3 billion were in the health sector.


* 20200415, https://www.reuters.com/article/us-health-coronavirus-belgium-carehomes/belgium-records-half-of-coronavirus-deaths-in-nursing-homes-idUSKCN21X1VU

Belgium records half of coronavirus deaths in nursing homes

BRUSSELS (Reuters) - Nursing homes account for nearly half of all
coronavirus-related deaths in Belgium, data showed on Wednesday, in
what may serve as a warning to other nations which may be
underestimating fatality rates in the pandemic.

Belgium is one of few countries in Europe that includes in its daily
tally of coronavirus-related deaths all non-hospitalised people who
displayed symptoms of the disease even if they had not been confirmed
as having had it.

That may help to explain why Belgium, a small country of about 11.5
million people that has been in lockdown since March 18, now has the
fifth highest coronavirus death toll in Europe, ahead of more populous
nations like Germany and the Netherlands.

Belgium has so far reported 33,573 confirmed cases including 4,440
deaths. Of these total fatalities, 46% were in nursing homes, the
latest data showed. Of the 283 deaths recorded on Tuesday alone, 63%
were in nursing homes.

“TIME-BOMB”

Valerie Victoor, from the federation of hospitals and care homes for
the regions of Brussels and Wallonia, went further.

“It’s a time bomb,” she told Reuters. “We knew that the day when
clusters broke out in nursing homes, it would burst into flames and
the fire would be difficult to put out.”

“We need to take concrete measures to help directors of nursing homes,
caregivers, to take all the measures we still can to limit the effects
of this explosion.”

In the Netherlands, for example, a large number of coronavirus deaths
might not have been reported as such because of under-testing in
nursing homes.

Italy, with the second highest death toll in the world after the
United States, only started testing in nursing homes last week.

