---
title: "SARS-CoV-2 Primer & Monitor"
subtitle: "A Genetic & Memetic Pandemic."
author: Rainer Machne
date: \today
output:
    beamer_presentation:
    keep_tex:  true
header-includes:
  - '\setlength\itemsep{0em}'
  - \usepackage{caption}
  - \captionsetup[figure]{labelformat=empty}
  - \widowpenalties 1 150
bibliography: corona.bib
link-citations: true
---

# The Unknown.

\scriptsize

\vspace{1cm}

\strut\hfill ![](originalData/RKI_EM_Covid_19_koloriert_Krankheitsseite.jpg){width=40%}

\vspace{-2cm}

Reports that say that something hasn't happened\newline
are always interesting to me,\newline\vspace{-1ex}

because as we know, there are known knowns;\newline
there are things we know we know.\newline\vspace{-1ex}

We also know there are known unknowns;\newline
that is to say we know there are some things we do not know.\newline\vspace{-1ex}

But there are also unknown unknowns -\newline
the ones we don't know we don't know.\newline\vspace{-1ex}

And if one looks throughout the history of our country and other free countries,\newline
it is the latter category that tend
to be the difficult ones.\newline

\vspace{.5cm}

Donald Rumsfeld, Feb 12, 2002, about **the lack of evidence** linking the
government of Iraq with the supply of weapons of mass destruction to
terrorist groups.\newline
\url{en.wikipedia.org/wiki/There_are_known_knowns}

# The Unknown.

\scriptsize

Rough timeline:

* 2002-2004: SARS-CoV outbreak in China & Canada (and world-wide)
    - \scriptsize SARS: **Severe Acute Respiratory Syndrome**
    - CoV: Coronavirus
    - 8000+ infected, 774 dead $\Rightarrow$ ~**10% mortality**
* 2009: **swine flu (H1N1), tamiflu!** \tiny (0.1% mortality)
* \scriptsize 2012: MERS outbreak in Saudi-Arabia
* 2015: MERS outbreak in S. Korea: 186 infected, 36 dead, 19% mortality
* Dec 2019: reports of an outbreak of a virus highly
similar \newline  to SARS-CoV in Wuhan, Hubei province, China
    - \scriptsize Hubei province: **~4% mortality**\newline
    \tiny (outside Hubei: <1%, closer to seasonal influenza)
* Feb 2020: SARS-CoV-2 arrives in Europe
* High media coverage: Virologist and WHO adviser Drosten (Charit&eacute;
Berlin)\newline says **40-70% of the population will be infected**
* Feb/Mar 2020: Italian hospitals are overloaded
    - \scriptsize mortality ~5% \tiny (average age: 81)


Simple math:

\vspace{-.5cm}

People          Numbers
------------ ----------
Germans      80,000,000
70% infected 56,000,000
10% dead      5,600,000   
    



# The Unknown.

\footnotesize

![](originalData/drosten-christian-institut-fuer-virologie-charite_297x337.JPG){height=20%}
![](originalData/2020-01-29-Alexander_S._Kekule-Maischberger-8331_small.jpg){height=20%} \hspace{.8cm} ![](originalData/zhenlilishi.jpg){height=20%} \hspace{.8cm} ![](originalData/portrait_wodarg_2017.png){height=20%}
![](originalData/0809moelling_karin.jpg){height=20%}


* H1 (Team Drosten/Kekul&eacute;): a dangerous new virus strain causes a **pandemic with high mortality**, requiring immediate and drastic measures, 
* H0 (Team Wodarg/Mölling): nothing special is happening, we are going
through the normal seasonal rise in respiratory diseases (**flu season**), caused by
multiple viruses; we are simply measuring the spread of
one specific virus strain involved this year (**observation bias**)
and are experiencing a "social media or press epidemic" [@Moelling20200314].


\tiny

* Christian Drosten: Institute of Virology, Charit&eacute; Berlin, and WHO adviser on SARS/MERS.
* Alexander Kekul&eacute;: Medizinische Mikrobiologie und Virologie, Martin-Luther-Universität Halle-Wittenberg; Institut für Medizinische Mikrobiologie, Universitätsklinikums Halle (Saale).
* **Shi Zhengli: Wuhan Institute of Virology, Hubei province, China.**
* Wolfgang Wodarg: pulmonologist, public health expert, German parliament and European Council for SPD, Chairman of Health Commite of the Parliamentary Assembly of the Council of Europe; Transparency International.  
* Katrin Mölling: Institute of Medical Virology, University of Zurich (1993-2008), Max Planck Institute for Molecular Genetics in Berlin (1976-2003).

# The Unknown.

## SARS-CoV & SARS-CoV-2.

# SARS-CoV: Of Viruses & Vampires

![](originalData/rota03_abstract.png)

@Rota2003: first description of SARS-CoV 

# SARS-CoV: Of Viruses & Vampires

![](originalData/rota03_fig1.jpg)

@Rota2003: first description of SARS-CoV\newline
$\rightarrow$ **3' is different from other Coronaviruses**

# SARS-CoV: Of Viruses & Vampires

![](originalData/drosten03_fig1a.png)

@Drosten2003: suggested diagnostic PCR primers\newline
\footnotesize $\rightarrow$ Drosten still makes PCR primers [@Corman2020]
for and\newline is an adviser on SARS/MERS for WHO

# SARS-CoV: Of Viruses & Vampires

![](originalData/corman20_fig1.png)

@Drosten2003: suggested diagnostic PCR primers\newline
\footnotesize $\rightarrow$ Drosten still makes PCR primers [@Corman2020]
for and\newline is an adviser on SARS/MERS for WHO

# SARS-CoV: Of Viruses & Vampires

![\footnotesize @Menachery2015](originalData/menachery15_fig1ab.png)

@Li2005: BATS \scriptsize & civets/camels \footnotesize \hfill Yunnan$\leftrightarrow$Wuhan: 1800 km
\newline\footnotesize
$\rightarrow$ group leader: **Shi Zheng-Li**, **Wuhan Institute of
Virology**, and now: first description of SARS-CoV-2 [@Zhou20200203],
*remdesivir* (patent for China) & chloroquin may help [@Wang20200204]

\tiny
\strut\hfill \url{www.gisaid.org/epiflu-applications/next-hcov-19-app/}

# SARS-CoV-2: Of Viruses & Vampires

![\scriptsize @Zhou20200203](originalData/zhou20_abstract.png){heigh=80%}

\tiny
\strut\hfill \url{www.gisaid.org/epiflu-applications/next-hcov-19-app/}

@Zhou20200203]


# SARS-CoV-2: Of Viruses & Vampires

![\scriptsize @Zhou20200203: **Bat CoV RaTG13 from Yunnan**](originalData/zhou20_fig1d.png){heigh=80%}


# SARS-CoV-2: Of Viruses & Vampires

![\scriptsize @Zhang2020: **Malayan pangolin** smuggled to S. China 2017/2018](originalData/zhang20b_fig2.png){heigh=80%}


# SARS-CoV-2: Of Viruses & Vampires

![\scriptsize @Zhang2020: **Malayan pangolin** smuggled to S. China 2017/2018](originalData/zhang20b_gabstract.png){heigh=80%}

# SARS-CoV-2: Of Viruses & Vampires

![](originalData/nextstrain_20200409_tree.png){height=80%}

\url{nextstrain.org/ncov}: SARS-CoV-2 phylogeny from current data

# SARS-CoV-2: Of Viruses & Vampires

![](originalData/nextstrain_20200409_map.png){height=60%}

\url{nextstrain.org/ncov}: SARS-CoV-2 phylogeny from current data

# SARS-CoV-2: Of Viruses & Vampires

![](originalData/nextstrain_20200409_tree_unrooted.png){height=50%}

SARS-CoV-1.5 in Europe and N. America?\newline

\vspace{.15cm}
\scriptsize

\url{nextstrain.org/ncov}

# The Unknown.

## Assessing Mortality During an Outbreak?


# SARS-CoV-2: Assessing Mortality During an Outbreak?

\scriptsize

![](figures/worldometer_current_mortality.png){width=90%}

\tiny

\url{www.worldometers.info/coronavirus/\#countries}


# SARS-CoV-2: Assessing Mortality During an Outbreak?

\scriptsize

![](figures/worldometer_current_mortality_100.png){width=90%}

\tiny

\url{www.worldometers.info/coronavirus/\#countries}

# SARS-CoV-2: Assessing Mortality During an Outbreak?

\scriptsize

![](figures/worldometer_mortality.png){width=90%}

\tiny


\url{www.worldometers.info/coronavirus/\#countries}

# SARS-CoV-2: Assessing Mortality During an Outbreak?

\scriptsize

![](figures/worldometer_mortality_zoom.png){width=90%}

\tiny


\url{www.worldometers.info/coronavirus/\#countries}


# Different Data Sources: Germany, Johns Hopkins

![](figures/world/Germany_owid.png){height=60%}

\scriptsize

Johns Hopkins Univ. via \url{ourworldindata.org}

\tiny

\url{https://covid.ourworldindata.org/data/owid-covid-data.csv}

# Different Data Sources: Germany, ECDC

![](figures/world/Germany_ecdc_2.png){height=60%}

\scriptsize

European Center for Disease Prevention and Control: time-series data

\tiny

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/}


# Different Data Sources: Germany, cleaned RKI data

![](figures/Germany/Germany_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}, **CORRECT(ish) DATES**

# Pandemic & Isolation: Local Events

![](figures/Germany/Berlin_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}



# Pandemic & Isolation: Local Events

![](figures/Germany/Bayern_npgeo.png){height=60%}

\scriptsize

Robert Koch Institute: cases numbers and fatalities in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Pandemic & Isolation: Local Events

![](figures/Austria/Bezirke/Landeck_2.png){height=60%}

\scriptsize

European Hub: skiing resort Ischgl

\tiny

\url{github.com/statistikat/coronaAT/}

# Pandemic & Isolation: Local Events

![](figures/world/Iceland_owid.png){height=60%}

\scriptsize

Johns Hopkins Univ. via \url{ourworldindata.org}

\tiny

\url{https://covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic & Isolation: Local Events

![](figures/Germany/Nordrhein.Westfalen/LK.Heinsberg_npgeo.png){height=60%}

\scriptsize

First cluster in Germany: Heinsberg

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# Pandemic & Isolation: Local Events

![](figures/Germany/top_1.png){height=60%}

\scriptsize

Most Affected "Kreis" in Germany

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# Pandemic & Isolation: Local Events

![](figures/Germany/Nordrhein.Westfalen/LK.Guetersloh_npgeo.png){height=60%}

\scriptsize

Late cluster in Germany: slaughterhouse and meat factory Tönnies

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 



# Pandemic & Isolation: Local Events

![](figures/USA/New York/New York_New York City_nyt.png){height=60%}

\scriptsize

New York Times data collection by US counties

\tiny

\url{github.com/nytimes/covid-19-data}

# In Context: Berlin

![](figures/npgeo_berlin_context.png){height=60%}

\scriptsize

Total mortality in Berlin.

\tiny

Figure 43 (deaths/week in Berlin) from @RKI2019, digitized with `engauge`\newline
RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 
 
# In Context: Germany

![](figures/npgeo_germany_context.png){height=60%}

\scriptsize

Extrapolated from Berlin (pop: 3.7e6) to Germany (pop: 83.2e6), data from 2017

\tiny

Figure 43 (deaths/week in Berlin) from @RKI2019, digitized with `engauge`\newline
RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/} 

# In Context: European Countries

![](figures/Europe/Europe_mortality_19991231.png){height=60%}

\scriptsize

Total mortality in European countries, normalized to median 2016-2019.

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: Austria

![](figures/Austria/Austria_context_all.png){height=60%}

\scriptsize

Total mortality in Austria.

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Austria

![](figures/Austria/Austria_context_zoom.png){height=60%}

\scriptsize

Total mortality in Austria.

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Austria

![](figures/Austria/Austria_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Tyrol

![](figures/Austria/Tyrol_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Vienna

![](figures/Austria/Vienna_context_prediction_zoom.png){height=60%}

\scriptsize

Forecast by `prophet`: \url{facebook.github.io/prophet}

\tiny

Mortality: \url{data.statistik.gv.at/web/catalog.jsp}\newline
SARS-CoV-2/Corona: \url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data}

# In Context: Belgium

![](figures/Europe/BE_mortality.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Belgium

![](figures/Europe/BE_mortality_age.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Belgium

![](figures/Europe/BE_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Belgium (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline



# In Context: Spain

![](figures/Europe/ES_mortality_age.png){height=60%}

\scriptsize

Total mortality in Spain (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Spain

![](figures/Europe/ES_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Spain (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: Sweden

![](figures/Europe/SE_mortality_age.png){height=60%}

\scriptsize

Total mortality in Sweden (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: Sweden

![](figures/Europe/SE_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in Sweden (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: UK

![](figures/Europe/UK_mortality_age.png){height=60%}

\scriptsize

Total mortality in United Kingdom (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline

# In Context: UK

![](figures/Europe/UK_mortality_age_year.png){height=60%}

\scriptsize

Total mortality in United Kingdom (right axis: normalized to median 2016-2019).

\tiny

Mortality: \url{ec.europa.eu/eurostat/web/products-datasets/-/demo_r_mwk_10}\newline


# In Context: New York City

![](figures/USA/New_York_City_cdc_fluview_all.png){height=60%}

\scriptsize

high activity in New York City

\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}

# In Context: New York State w/o City

![](figures/USA/New_York_cdc_fluview_all.png){height=60%}

\scriptsize

"normal" activity in New York State w/o City

\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}
 
# In Context: California

![](figures/USA/California_cdc_fluview_all.png){height=60%}

\scriptsize

low activity in California

\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}

 
# In Context: USA

![](figures/USA/California_cdc_fluview_zoom.png){width=50%} California\newline
![](figures/USA/New_York_cdc_fluview_zoom.png){width=50%} New York state\newline
![](figures/USA/New_York_City_cdc_fluview_zoom.png){width=50%} New York City

\tiny

Mortality: CDC, \url{gis.cdc.gov/grasp/fluview/mortality.html}\newline
Coronavirus: New York Times, \url{github.com/nytimes/covid-19-data}

# Pandemic: world-wide data 

![](figures/ecdc_norm_cases.png){width=50%}
![](figures/ecdc_norm_deaths.png){width=50%}\newline
![](figures/ecdc_norm_tests.png){width=50%}
![](figures/ecdc_norm_mortality.png){width=50%}
\tiny

ECDC and PCR Test data [@Roser2020]

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} & \url{ourworldindata.org/covid-testing}

# Pandemic: world-wide data 

![](figures/ecdc_norm_cases_per_week.png){width=50%}
![](figures/ecdc_norm_deaths_per_week.png){width=50%}\newline
![](figures/ecdc_norm_tests_per_week.png){width=50%}

\tiny

ECDC and PCR Test data [@Roser2020]

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} & \url{ourworldindata.org/covid-testing}


# Pandemic: world-wide data 

![](figures/ecdc_norm_deaths_vs_cases.png){width=40%}
![](figures/ecdc_norm_tests_vs_cases.png){width=40%}\newline
![](figures/ecdc_norm_tests_per_cases_vs_deaths.png){width=40%}

\tiny

ECDC and PCR Test data [@Roser2020]

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} & \url{ourworldindata.org/covid-testing}

# Pandemic: world-wide data 

![](figures/ecdc_norm_deaths_vs_cases_final.png){width=40%}
![](figures/ecdc_norm_tests_vs_cases_final.png){width=40%}\newline
![](figures/ecdc_norm_tests_per_cases_vs_deaths_final.png){width=40%}

\tiny

ECDC and PCR Test data [@Roser2020]

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} & \url{ourworldindata.org/covid-testing}

# Pandemic: world-wide data 

![](figures/world/North_America_owid.png){height=25%}
![](figures/world/South_America_owid.png){height=25%}

![](figures/world/Europe_owid.png){height=25%}
![](figures/world/Africa_owid.png){height=25%}

![](figures/world/Asia_owid.png){height=25%}
![](figures/world/Oceania_owid.png){height=25%}


\tiny

ECDC

\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data} 


# Pandemic: Europe - South

![](figures/world/Italy_owid.png){height=25%}
![](figures/world/Greece_owid.png){height=25%}

![](figures/world/Cyprus_owid.png){height=25%}
![](figures/world/Malta_owid.png){height=25%}

![](figures/world/Spain_owid.png){height=25%}
![](figures/world/Portugal_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Central

![](figures/world/France_owid.png){height=25%}
![](figures/world/Belgium_owid.png){height=25%}

![](figures/world/Germany_owid.png){height=25%}
![](figures/world/Luxembourg_owid.png){height=25%}

![](figures/world/Austria_owid.png){height=25%}
![](figures/world/Switzerland_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}



# Pandemic: Europe - North

![](figures/world/United_Kingdom_owid.png){height=25%}
![](figures/world/Ireland_owid.png){height=25%}

![](figures/world/Netherlands_owid.png){height=25%}
![](figures/world/Denmark_owid.png){height=25%}

![](figures/world/Sweden_owid.png){height=25%}
![](figures/world/Norway_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - East

![](figures/world/Poland_owid.png){height=25%}
![](figures/world/Czechia_owid.png){height=25%}

![](figures/world/Slovakia_owid.png){height=25%}
![](figures/world/Hungary_owid.png){height=25%}

![](figures/world/Ukraine_owid.png){height=25%}
![](figures/world/Russia_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - South-East


![](figures/world/Slovenia_owid.png){height=25%}
![](figures/world/Croatia_owid.png){height=25%}

![](figures/world/Serbia_owid.png){height=25%}
![](figures/world/Bosnia_and_Herzegovina_owid.png){height=25%}

![](figures/world/Romania_owid.png){height=25%}
![](figures/world/Bulgaria_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - East, Small Countries


![](figures/world/Estonia_owid.png){height=25%}
![](figures/world/Lithuania_owid.png){height=25%}

![](figures/world/Latvia_owid.png){height=25%}
![](figures/world/Moldova_owid.png){height=25%}

![](figures/world/North_Macedonia_owid.png){height=25%}
![](figures/world/Kosovo_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Small Countries


![](figures/world/Liechtenstein_owid.png){height=25%}
![](figures/world/Monaco_owid.png){height=25%}

![](figures/world/San_Marino_owid.png){height=25%}
![](figures/world/Andorra_owid.png){height=25%}

![](figures/world/Vatican_owid.png){height=25%}
<!--![](figures/world/the_Holy.See..Vatican.City.State_owid.png){height=25%}-->
<!--![](figures/world/Holy_See_owid.png){height=25%}-->
![](figures/world/Gibraltar_ecdc_2.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Middle East 

![](figures/world/Iran_owid.png){height=25%}
![](figures/world/Qatar_owid.png){height=25%}

![](figures/world/United_Arab_Emirates_owid.png){height=25%}
![](figures/world/Saudi_Arabia_owid.png){height=25%}

![](figures/world/Turkey_owid.png){height=25%}
![](figures/world/Israel_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: America - North/Central

![](figures/world/Canada_owid.png){height=25%}
![](figures/world/United_States_owid.png){height=25%}

![](figures/world/Mexico_owid.png){height=25%}
![](figures/world/Cuba_owid.png){height=25%}

![](figures/world/Guatemala_owid.png){height=25%}
![](figures/world/Dominican_Republic_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: America - North/Central - USA

![](figures/USA/Florida_nyt.png){height=25%}
![](figures/USA/California_nyt.png){height=25%}
	    
![](figures/USA/Louisiana_nyt.png){height=25%}
![](figures/USA/Massachusetts_nyt.png){height=25%}
	    
![](figures/USA/Arizona_nyt.png){height=25%}
![](figures/USA/New York City_nyt.png){height=25%}


\tiny

\url{github.com/nytimes/covid-19-data}
**NOTE: 3x HIGHER Y-AXIS SCALE**

# Pandemic: America - South - North


![](figures/world/Panama_owid.png){height=25%}
![](figures/world/Venezuela_owid.png){height=25%}


![](figures/world/Colombia_owid.png){height=25%}
![](figures/world/Guyana_owid.png){height=25%}

![](figures/world/Ecuador_owid.png){height=25%}
![](figures/world/Suriname_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: America - South - South

![](figures/world/Peru_owid.png){height=25%}
![](figures/world/Brazil_owid.png){height=25%}

![](figures/world/Chile_owid.png){height=25%}
![](figures/world/Bolivia_owid.png){height=25%}

![](figures/world/Argentina_owid.png){height=25%}
![](figures/world/Uruguay_owid.png){height=25%}



\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}


# Pandemic: Asia 

![](figures/world/China_owid.png){height=25%}
![](figures/world/Taiwan_owid.png){height=25%}

![](figures/world/Japan_owid.png){height=25%}
![](figures/world/South_Korea_owid.png){height=25%}

![](figures/world/Singapore_owid.png){height=25%}
![](figures/world/India_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Africa 

![](figures/world/Egypt_owid.png){height=25%}
![](figures/world/Morocco_owid.png){height=25%}

![](figures/world/Djibouti_owid.png){height=25%}
![](figures/world/Senegal_owid.png){height=25%}

![](figures/world/Zimbabwe_owid.png){height=25%}
![](figures/world/South_Africa_owid.png){height=25%}

\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Oceania

![](figures/world/Malaysia_owid.png){height=25%}
![](figures/world/Indonesia_owid.png){height=25%}

![](figures/world/Australia_owid.png){height=25%}
![](figures/world/New_Zealand_owid.png){height=25%}


\tiny

\url{covid.ourworldindata.org/data/owid-covid-data.csv}

# Pandemic: Europe - Central - Germany

![](figures/Germany/Bayern_npgeo.png){height=25%}
![](figures/Germany/Baden.Wuerttemberg_npgeo.png){height=25%}

![](figures/Germany/Saarland_npgeo.png){height=25%}
![](figures/Germany/Nordrhein.Westfalen_npgeo.png){height=25%}

![](figures/Germany/Sachsen_npgeo.png){height=25%}
![](figures/Germany/Berlin_npgeo.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}

# Germany: most deaths/1e5

![](figures/Germany/top_1.png){height=25%}
![](figures/Germany/top_2.png){height=25%}

![](figures/Germany/top_3.png){height=25%}
![](figures/Germany/top_4.png){height=25%}

![](figures/Germany/top_5.png){height=25%}
![](figures/Germany/top_6.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}
**NOTE: 6x HIGHER y-axis scale**

# Germany: most new cases 

![](figures/Germany/top_1_last.png){height=25%}
![](figures/Germany/top_2_last.png){height=25%}

![](figures/Germany/top_3_last.png){height=25%}
![](figures/Germany/top_4_last.png){height=25%}

![](figures/Germany/top_5_last.png){height=25%}
![](figures/Germany/top_6_last.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}
**NOTE: 6x HIGHER y-axis scale**

# Germany: strongest recent increase 

![](figures/Germany/top_1_rise.png){height=25%}
![](figures/Germany/top_2_rise.png){height=25%}

![](figures/Germany/top_3_rise.png){height=25%}
![](figures/Germany/top_4_rise.png){height=25%}

![](figures/Germany/top_5_rise.png){height=25%}
![](figures/Germany/top_6_rise.png){height=25%}

\tiny

RKI COVID19 Dataset: \url{npgeo-corona-npgeo-de.hub.arcgis.com/}



# SARS-CoV-2: Why so drastically different mortalities? 

\scriptsize

Technical:

* **Ongoing**: critical cases
* Missing data: how many untested cases?
* Different tests & testing policies, **exponential increase in
PCR tests**?
* *post-mortem* tests counted, may have died from other causes?

Societal:

* Different attitudes: public health care, go to or avoid
doctors/hospitals?
* **Nosocomial**: transmission in hospitals [@Wang2020;@Nacoti2020]?
* Different social structure: **elderly in "homes" or at home?**,

Biological:

* Population age structure, e.g. on a cruiseship and in Italy?
* **Prior influenza wave** has already killed the susceptible,
e.g. Germany, Austria?
* Prior SARS waves & immunity, eg. Germany, Austria?
* Interference from flu vaccination? [@Wolff2020;@Lisewski2020]
* Potential co-infections? (Chakraborty: *Prevotella* in Wuhan samples)
* Protective microbiome, e.g. Kimchi in Korea [@Bae2018]

Environmental:

* Air pollution [@Xia2017], Northern Italy [@Carugno2016] & Wuhan 
* Climate: **humidity & temperature** [@Tan2005;@Chan2011]

\tiny

# Relevant Theoretical Models

\scriptsize

* Kormack-McKendrick theory: the SIR model
re-published as [@Kermack1991a;@Kermack1991b;@Kermack1991c]
* McKendrick-v.Foerster equation: population age structure model [@McKendrick1925;@Fredrickson1967;@Akushevich2019],
* Dual SIR model, two competing viruses [@Restif2006],
* Seasonally forced SIR model of of SARS-CoV-2 with birth/ageing and migration [@Neher2020],
* Phylodynamic theory of virus-host co-evolution [@Yan2019,],
* Agent-based spatially resolved models, TODO,
* Model of an *adaptive cyclic* lock-down strategy [@Karin2020],
* Interactive model with COVID-19 parameters \url{alhill.shinyapps.io/COVID19seir/}

# Health Care System Overload During "Flu Season"

\scriptsize

* 2018/01/08, USA: *North Texas: hospital overrun by flu cases having
  to turn them away*
* 2018/01/15, USA: *A severe flu season is stretching hospitals
  thin. That is **a very bad omen** [...]*
* 2019/11/15, UK: *More than 17,000 beds have been cut from the
  144,455 that existed in 2010. [...] just 127,225 left to cope
  with the rising demand for care, **which will intensify as winter
  starts to bite**. [...] when the coalition Conservative/Liberal
  Democrat government took office and imposed a nine-year funding
  squeeze on the NHS.*  
* 2020/05/25, UK: *For HC-One, the nursing home business has been
  lucrative, as the company paid more than 50 million pounds, or
  nearly $61 million, in dividends from 2017 to 2019. But staff
  members at Home Farm suffered. During **12-hour shifts**, they sometimes
  made do with no more than **one nurse and two aides on an 18-bed
  floor**, employees said. The **residents’ help buttons buzzed
  incessantly**.*

\tiny

\url{https://dfw.cbslocal.com/2018/01/08/hospital-overrun-by-flu-cases-having-to-turn-them-away/}\newline
\url{https://www.statnews.com/2018/01/15/flu-hospital-pandemics/}\newline
\url{https://www.theguardian.com/politics/2019/nov/25/hospital-beds-at-record-low-in-england-as-nhs-struggles-with-demand}\newline
\url{https://www.nytimes.com/2020/05/25/world/europe/coronavirus-uk-nursing-homes.html}


# A Fatal Mistake? Nosocomial Infections.

\scriptsize

* *An overwhelming majority of cases in Phase II of the Toronto
SARS-CoV were related to healthcare transmission* [@Popescu2020]
* S. Korea, MERS 2015: *Of the 186 cases confirmed,
184 were a result of nosocomial transmission* [@Popescu2020], *largely
attributable to infection management and policy failures rather than
biomedical factors.* [@Hsieh2015;@Kim2017;@Yang2020]!
* Wuhan: hospital-associated transmission suspected in 41% (of 138 patients)
and mortality was 4.1% [@Wang2020]
* Milan: "*And 40 more beds were dedicated to patients suspected or proven to have the virus, though not in a serious condition.*" (New York Times, 2020 03 18)
* Bergamo: "*hospitals might be the main Covid-19 carriers [...] ambulances and personnel rapidly become vectors [...] asymptomatic carriers or sick without surveillance*" [@Nacoti2020]
* "*All my friends in Italy tell me the same thing [...] patients started arriving and the rate of infection in other patients soared. That is one thing that probably led to the current disaster.*" (\url{statnews.com}, 2020 03 21)

\footnotesize

Nosocomial virus infections: "*Stark unterschätzte Inzidenz*"

**The vicious cycle: panic from deadly virus $\leftrightarrow$ nosocomial transmission.**

\tiny

Grünewald (2020 03 13), Informationsveranstaltung für Mitarbeiter
des Klinikums Chemnitz: \url{youtu.be/F7hGW2-o5V4}\newline
\url{www.nytimes.com/2020/03/18/opinion/coronavirus-italy.html}\newline
\url{www.statnews.com/2020/03/21/coronavirus-plea-from-italy-treat-patients-at-home/}\newline
\url{www.uniklinik-freiburg.de/fileadmin/mediapool/08_institute/virologie/pdf/Diagnostik/Nosokomiale_Virusinfektionen_BadenBaden2013.pdf}

# A Fatal Mistake? Nosocomial Infections.

![\tiny \url{www.theguardian.com/us-news/2020/mar/27/new-york-coronavirus-elmhurst-hospital}](originalData/guardian_20200327.png){width=60%}

\footnotesize

People waiting in cold and rainy weather to get tested for
coronavirus, Elmhurst Hospital in Queens, NYC.

**The vicious cycle: panic from deadly virus $\leftrightarrow$ nosocomial transmission.**

# A Fatal Mistake? Nosocomial Infections.

![\tiny \url{https://orf.at/stories/3161528/}](originalData/orf_turkey_20200411.jpg){width=60%}

\footnotesize

People in Turkey in front of a shopping center after the government
ordered a complete 48 h lock-down.

**The vicious cycle: panic from deadly virus $\leftrightarrow$ nosocomial transmission.**


# A Fatal Mistake? Nosocomial Infections.

![\tiny @Chen2004](originalData/chen04_abstract.png){width=60%}

# A Fatal Mistake? Nosocomial Infections.

![](originalData/learningfromsars04_screenshot.png){width=100%}


# A Fatal Mistake? How to avoid it:

\scriptsize
Gernot Walder, East-Tyrol's Drosten:

*Durch die frühe, sehr umfassende Testung ist es uns, wie ich meine,
gut gelungen, Infektionen bzw. auch so genannte Infektionscluster zu
erkennen, bevor der Erreger großflächig zu zirkulieren begann. Sehr
gut bewährt hat sich auch das Screening von Mitarbeitern und Patienten
im Lienzer Krankenhaus sowie in den Wohn- und Pflegeheimen. So konnten
wir die Verbreitung in diesen systemkritischen Einrichtungen
frühzeitig abwehren bzw. auf Einschläge rechtzeitig reagieren und
diese begrenzen.*

Also: infection in air conditioning?


# Pollution?

![](originalData/esa_airpollution_before.png){height=50%}

\scriptsize
\vspace{-1cm}

Hypothesis: Both Wuhan City and Northern Italy Lombardia region
suffer from strong air pollution which may contribute to high mortality.


* Air pollution effects in Shenzhen [@Xia2017]
* "*Natural mortality was positively associated with both pollutants*
[PM10 and NO2 ...]  *the percent variation for respiratory mortality
was highest in association with PM10 (1.64%, 90%CI: 0.35; 2.93).*"
[@Carugno2016]
* Air pollution rapidly dropped after lock-down, **2020 01 01** 
video at
\tiny \url{www.esa.int/ESA_Multimedia/Videos/2020/03/Coronavirus_nitrogen_dioxide_emissions_drop_over_Italy}

# Pollution?

![](originalData/esa_airpollution_after.png){height=50%}

\scriptsize
\vspace{-1cm}

Hypothesis: Both Wuhan City and Northern Italy Lombardia region
suffer from strong air pollution which may contribute to high mortality.


* Air pollution effects in Shenzhen [@Xia2017]
* "*Natural mortality was positively associated with both pollutants*
[PM10 and NO2 ...]  *the percent variation for respiratory mortality
was highest in association with PM10 (1.64%, 90%CI: 0.35; 2.93).*"
[@Carugno2016]
* Air pollution rapidly dropped after lock-down, **2020 03 11**
video at
\tiny \url{www.esa.int/ESA_Multimedia/Videos/2020/03/Coronavirus_nitrogen_dioxide_emissions_drop_over_Italy}

# Pollution?

![\tiny \url{edition.cnn.com/2019/07/10/asia/china-wuhan-pollution-problems-intl-hnk/}](originalData/cnn_20190711.png){width=60%}

\footnotesize

Protests in Wuhan in 2019 against a new incinerator

# Pollution?

![\tiny \url{patch.com/new-york/new-york-city/new-york-among-most-polluted-cities-u-s-analysis-shows}](originalData/patch_20200127.png){width=60%}


\scriptsize

@Dedoussi2020: *Premature Mortality Related to US Cross-State Air Pollution.*\newline
@Wu2020bpre: "Exposure to air pollution and COVID-19 mortality in the US"\newline
@Ogen2020: "Assessing nitrogen dioxide (NO2) levels as a 
contributing factor to coronavirus (COVID-19) fatality."

# Pollution?

\scriptsize

* Respiratory infections and SARS-CoV-1:
    - \scriptsize @Cui2003: *Air pollution and case fatality of SARS in the
    People's Republic of China: an ecologic study.*
    - @Xia2017: *The Association between Air Pollution and Population
    Health Risk for Respiratory  Infection: A Case Study of Shenzhen, China.*
    - @Carugno2016: *Air pollution exposure, cause-specific deaths and
    hospitalizations in a highly polluted Italian region.*
    - @Tsai2019: *Effects of Short- And Long-Term Exposures to Particulate
    Matter on Inflammatory Marker Levels in the General Population.*
    - @Dedoussi2020: *Premature Mortality Related to US Cross-State
    Air Pollution.*
    - @Cao2020b: *Environmental pollutants damage airway epithelial
    cell cilia: Implications for the prevention of obstructive lung diseases.*
* SARS-CoV-2:    
    - \scriptsize @Wu2020bpre: *Exposure to air pollution and
    COVID-19 mortality in the US.*
    - @Ogen2020: *Assessing nitrogen dioxide (NO2) levels as a
    contributing factor to coronavirus (COVID-19) fatality.*
    - @Conticini2020: *Can atmospheric pollution be considered a co-factor
    in extremely high level of SARS-CoV-2 lethality in Northern Italy?*

# Seasonality: will it get better in spring?

\footnotesize

@Yuan2006: "*The Beijing data suggest that SARS is likely to behave in
a seasonal manner, initially appearing between late autumn and early
spring*",

* **Spring?** peak spread of SARS (Guangdong, 2002) at T=17°C (CI: 11-23°C),
    humidity of 52.2% (33-71%), and wind at 2.8 m/s (2.0-3.6 m/s),
* Negative correlation with T \tiny (p<0.001), \footnotesize humidity
\tiny (p<0.001), \footnotesize and positive correlation with
wind  \tiny (p<0.05).


\vspace{1cm}
\scriptsize

Overview on potential SARS-CoV-2 seasonality:

\tiny

\url{www.sciencemag.org/news/2020/03/why-do-dozens-diseases-wax-and-wane-seasons-and-will-covid-19}\newline
\url{ccdd.hsph.harvard.edu/will-covid-19-go-away-on-its-own-in-warmer-weather/}


# Seasonality: will it get better in spring?

\scriptsize

Flu Season: *resistance is futile.*\newline
Why are respiratory diseases more prevalent in winter?

* Effect on hosts:
    - \scriptsize People meet in confined spaces.
    - **Low T**: body uses most energy to keep warm, immune
    system generally weakened [@Shephard1998], esp. in respiratory tract,
    - **Low humidity**: dry air makes us more susceptible to
    influenza [@Kudo2019],
    - **Higher day-to-day variation of T** (@Dowell2001/ @Tan2005).
* Effect on pathogen:
    - \scriptsize Higher stability of SARS-CoV at
    low T & humidity [@Chan2011],
    - UV kills RNA Viruses, most respiratory tract viruses
    are RNA+ viruses.
* Carrier/intermediate host biology:
    - \scriptsize eg. transmitting insects in summer.


# Seasonality: will it get better in spring?

\scriptsize

* Respiratory disease and SARS-CoV-1 seasonality
    - \tiny @Shephard1998
    - @Dowell2001
    - @Tan2005
    - @Yuan2006
    - @Alonso2007,@Almeida2018: traveling influenza wave in Brazil, north: May, south: July
    - @SchuckPaim2012: swine flu mortality progressively lower towards equatorial regions
    - @Tamerius2013: rainy season is flu season in tropical climates
    - @Rambaut2008,@Russell2008: global circulation of A/H3N2 and A/H1N1
    - @Shaman2010: absolute humidity and influenza in USA
    - @Matoba2015
    - @Chan2011
    - @Kudo2019
    - @Nickbakhsh2019
    - @Yan2019: phylodynamic theory of persistence, extinction and speciation
* SARS-CoV-2 seasonality
    - \scriptsize @Bu2020: **spring temperatures!**,
    - @Wang2020d: late winter?,
    - @Luo2020pre, @Quilodran2020pre: low T, but **high humidity**,
    - @Sajadi2020pre: low T and **low humidity**,
    - @Chin2020, @vanDoremalen2020: aerosol & surface stability,
    stable at 4°C, but sensitive to heat,

\vspace{.5cm}
\scriptsize

Overview on potential SARS-CoV-2 seasonality:

\tiny

\url{www.sciencemag.org/news/2020/03/why-do-dozens-diseases-wax-and-wane-seasons-and-will-covid-19}\newline
\url{ccdd.hsph.harvard.edu/will-covid-19-go-away-on-its-own-in-warmer-weather/}

# TODO

\scriptsize

* **sick building syndrome**: dry air, air conditioning, cf.
"built environment",
* co-evolution of influenza and immune system [@Boianelli2015;@Koelle2015],
 viral interference and viral hierarchy [@Cao2015;@Laurie2015],
antigenic distance hypothesis [@Francis2019]
* efficacy of influenza vaccination (VE):
    - \tiny very low VE for live attenuated vaccines [@Singanayagam2018],
    - VE *between 16–59% in healthy adults* [@Samuel2019],
    - VE against A(H3) and B declines significantly in the 6 months following vaccination [@Young2018],
    - VE decreases each season [@McLean2014;@Ohmit2015;@Neuzil2015;@Skowronski2017;@Pratt2019;@DiazGranados2016],
* adverse effects of influenza vaccination:
    - \tiny association with Covid-19 [@Lisewski2020],
    - association with other respiratory diseases
    [@Cowling2012; @Feng2017; @Sundaram2013; @Grijalva2015],
    - cytokine storms and other problems: TODO,
* less smokers among COVID-19 cases? [@Zhang20200214;@Changeux2020pre;@Docherty2020pre]
* protective microbiome [@Bae2018] vs bacterial co-infections,
  *Prevotella*? [@Ley2016;@Lee2019], Chakraborty at pubpeer,
* SARS-CoV-1.5?

# Data collected (daily) at \url{gitlab.com/raim/corona}

\scriptsize

* Most literature cited herein as `pdf`,
* NCBI genbank file for the novel strain SARS-CoV-2,
* Weakly deaths in Berlin, 2013-2018:\newline
digitized with `engauge` from yearly flu season report,
* Cases and fatalities - Germany, from RKI website: `scrape_RKI.R`,
* Cases and fatalities - Germany, RKI data at npgeo: `scrape_NPGEO.R`,
* Cases and fatalities - global, from \url{www.worldometers.info}: `scrape_worldometer.R`.
* Cases and fatalities - global, from ECDC:  `scrape_ECDC.R`.
* PCR test data, *via* \url{ourworldindata.org/coronavirus}: `scrape_rosen20.R`
* USA mortality data, from CDC Fluview: `scrape_CDC_fluview.R`
* USA SARS-CoV-2 data, from New York Times: `scrape_NYTimes.R`
* Austria Mortality data, Statistik Austria: `scrape_StatAustria.R`

\tiny
@RKI2019: Bericht zur Epidemiologie der Influenza in Deutschland\newline
\url{www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html}\newline
\url{npgeo-corona-npgeo-de.hub.arcgis.com/} - RKI data by age/county(Kreis) and with correctish dates\newline
\url{www.worldometers.info/coronavirus/\#countries} - Johns Hopkins, world-wide\newline
\url{www.euromomo.eu} - euroMOMO, EU mortality\newline
\url{data.europa.eu/euodp/en/data/dataset/covid-19-coronavirus-data/} - ECDC\newline
\url{ourworldindata.org/covid-testing} - @Roser2020: PCR test time-series\newline
\url{gis.cdc.gov/grasp/fluview/mortality.html} - CDC fluview, US mortality\newline
\url{github.com/nytimes/covid-19-data} - New York Times git repo\newline
\url{data.statistik.gv.at/web/catalog.jsp}


# The Unknown.

## Epidemiology & Seasonality


# **A Bold Null Hypothesis**: Observation Bias? 

![](originalData/wodarg200301.png){}

\vspace{-1cm}

Would we have noticed SARS-CoV-2,\newline if it weren't measured at
Wuhan Institute of Virology, and\newline if PCR primers (from Berlin)
weren't available immediately?

\footnotesize

\vspace{.5cm}

\strut\hfill\url{www.wodarg.com}

# Resistance is Futile: Flu Season in USA

![](originalData/dowell01_fig1.png){height=70%}

\scriptsize
\vspace{-1cm}

@Dowell2001: Seasonal variation in host suceptibility\newline
and cycles of certain infectious diseases.

# Resistance is Futile: Flu Season in Japan

![](originalData/matoba15_fig1.png){height=70%}

\scriptsize
\vspace{-1cm}

@Matoba2015: An Outbreak of Human Coronavirus OC43\newline
During the 2014-2015 Influenza Season in Yamagata, Japan


# Resistance is Futile: Flu Season in Glasgow

\vspace{-1ex}

![\tiny @Nickbakhsh2019: Temporal patterns of viral respiratory
infections in Glasgow, 2005 to 2013](originalData/nickbakhsh19_fig1.jpg){height=45%}

\footnotesize \vspace{-4ex}

* Multiple viruses each season\newline
\tiny CoV: human coronaviruses 229E, NL63 & HKU1 - **TODO: check all primers**
* \footnotesize Virus-virus interactions:
    - too little co-infections (gray)?
    - **\footnotesize rhinovirus (RV) anticorrelates with influenza A (IAV)**\newline
$\rightarrow$ worst case: vaccinate against one, get sick from a worse virus

\tiny

*RV: rhinoviruses (A–C); IAV: influenza A virus; IBV: influenza B
virus; RSV: respiratory syncytial virus; CoV: human coronaviruses;
AdV: human adenoviruses; MPV: human metapneumovirus; PIV1-PIV4:
parainfluenza virus 1--4*

# FluMOMO Model: Estimating Excess Mortality

![](originalData/nielsen18_fig1.png){}

\footnotesize

* @Nielsen2018: **European mortality monitoring** \strut\hfill \scriptsize  \url{www.euromomo.eu}
* \footnotesize @Goldstein2011: predicting the epidemic sizes
of influenza \newline
\scriptsize Goldstein Index = respiratory disease with fever X fraction of
influenza+ samples


# Resistance is Futile: Flu Season in Germany


![@RKI2019 (Abb. 43) weakly mortality in Berlin and Hessen](originalData/rki2018_abb43_berlin.png){ height=32% }
![RKI Grippeweb, 2020, KW 10](originalData/grippeweb_2020-10-01.png){ height=32% }

\tiny Berlin \hfill ARE: acute respiratory disease


\footnotesize

* Total mortality in Germany [@RKI2019], 
    - seasonal variation in death rate, peaks during flu season
    - 2017/2018: **~25000 "excess mortality" during flu season**
    - **<50% of samples during flu season  tested influenza+**\newline
* \url{www.rki.grippeweb.de}, 2020, KW 10\newline
self-reported cases: note the different shape of the curves!

\tiny
@RKI2019: yearly influenza report, and \url{www.rki.grippeweb.de}

# Resistance is Futile: Flu Season in Germany


![@RKI2019 (Abb. 43) weakly mortality in Berlin and Hessen](originalData/rki2018_abb43_hessen.png){ height=32% }
![RKI Grippeweb, 2020, KW 10](originalData/grippeweb_2020-10-02.png){ height=32% }

\tiny Hessen \hfill ILI: ARE with fever

\footnotesize

* Total mortality in Germany [@RKI2019], 
    - seasonal variation in death rate, peaks during flu season
    - 2017/2018: **~25000 "excess mortality" during flu season**
    - **<50% of samples during flu season  tested influenza+**\newline
* \url{www.rki.grippeweb.de}, 2020, KW 10\newline
self-reported cases: note the different shape of the curves!

\tiny
@RKI2019: yearly influenza report, and \url{www.rki.grippeweb.de}

# Resistance is Futile: Flu Season in Germany


![\tiny @RKI2018 (Tab. 3) detected influenza viruses in samples](originalData/rki2017_tab3.png){ height=39% }

\footnotesize
\vspace{-.5cm}

* Total mortality in Germany [@RKI2019], 
    - seasonal variation in death rate, peaks during *flu season*
    - 2017/2018: **~25000 "excess mortality" during flu season**
    - **<50% of samples during flu season tested influenza+**\newline
    $\rightarrow$ OTHER Viruses & Bacteria? Or even **SARS-CoV-1.5?** 
* \url{www.rki.grippeweb.de}, 2020, KW 10\newline
self-reported cases: note the different shape of the curves!

\tiny
@RKI2018: yearly influenza report, and \url{www.rki.grippeweb.de}

#

![](originalData/euromomo_all_20200418.png){height=75%}

\vspace{-1.5cm}
\footnotesize

* @Nielsen2018:\newline European mortality monitoring, \url{www.euromomo.eu}
* Dual peaks (countries shifted!) and LOW in recent winter(s)

# 

![](originalData/euromomo_countries_20200418.png){height=75%}

\vspace{-1.5cm}
\footnotesize

* @Nielsen2018:\newline European mortality monitoring, \url{www.euromomo.eu}
* Dual peaks (countries shifted!) and LOW in recent winter(s)


# Resistance is Futile: Flu Season in Italy

![](originalData/rosano19_fig2.jpg){height=60%}

\footnotesize
\vspace{-1cm}

* Italy: @Rosano2019 estimate 7027, 20259, 15801 and 24981 excess
mortality during 2013/14, 2014/15, 2015/16 and 2016/17 flu seasons
* <50% of samples were influenza+

\tiny

# Resistance is Futile: Flu Season in Italy

![](originalData/rosano19_fig1.jpg){height=250px}

\footnotesize
\vspace{-.5cm}

* Italy: @Rosano2019 estimate7027, 20259, 15801 and 24981 excess
mortality during 2013/14, 2014/15, 2015/16 and 2016/17 flu seasons
* <50% of samples were influenza+

\tiny

# Resistance is Futile: Flu Season in Austria

![](originalData/ages_mortalitaet_2020_large.png){height=250px}

\footnotesize

* Austria: 4436 excess mortality in 2016/2017
* Note the holiday drop in self-reported cases!

\tiny

\url{www.ages.at/themen/krankheitserreger/grippe/mortalitaet/}
\url{www.virologie.meduniwien.ac.at/wissenschaft-forschung/virus-epidemiologie/influenza-projekt-diagnostisches-influenzanetzwerk-oesterreich-dinoe}

# Some Better News: Holiday Recovery

![](originalData/univie_ILI_Inzidenz_2019-2020.jpg){height=42%}

\footnotesize

* Austria: 4436 excess mortality in 2016/2017
* Note the holiday drop in self-reported cases!

\tiny

\url{www.ages.at/themen/krankheitserreger/grippe/mortalitaet/}
\url{www.virologie.meduniwien.ac.at/wissenschaft-forschung/virus-epidemiologie/influenza-projekt-diagnostisches-influenzanetzwerk-oesterreich-dinoe}


# Even Better News: Exponential PCR Testing?

![](originalData/orf_20200329_cases.png){height=60%}

\vspace{-1cm}
\tiny

\url{orf.at/corona/stories/3157533/}

# Even Better News: Exponential PCR Testing?

![](originalData/orf_20200329_tests_cum.png){height=60%}

\vspace{-1cm}
\tiny

\url{orf.at/corona/stories/3157533/}

# Even Better News: Exponential PCR Testing?

![](originalData/orf_20200329_tests.png){height=60%}

\vspace{-1cm}
\tiny

\url{orf.at/corona/stories/3157533/}


# The Unknown.

## The Viral Ecosystem.






# The Viral Ecosystem: We!

![@Zhou20200203: metagenomic analysis of patient ICU06](originalData/zhou20_fig1ab.png){height=50%}

\footnotesize

Sandeep Chakraborty at pubpeer:
*Prevotella* (gastric tract bacterium, associated with chronic
inflammatory disease [@Ley2016]) sequences found in unusual
quantities in Wuhan, and SARS:16S reads in Wuhan, Hong Kong and
Wisconsin patient samples; 

\url{pubpeer.com/publications/884D5017B8D48D5BA2466AC323F91B}


# Influenza strains over a life-time

![@Francis2019](originalData/francis19_fig1.png){ height=50% }

\footnotesize

* population-wide prevalence of seasonal influenza strains\newline
$\Rightarrow$ novel strain circulates **until 'enough' are immune** ("40-70%"?),
* **forced co-evolution**: immune system $\Leftrightarrow$ influenza strains

# Influenza strains over a century

![@Francis2019](originalData/francis19_fig3.png){ height=50% }

\footnotesize

* population-wide prevalence of seasonal influenza strains\newline
$\Rightarrow$ novel strain circulates **until 'enough' are immune** ("40-70%"?),
* **forced co-evolution**: immune system $\Leftrightarrow$ influenza strains






# Population Immunity, Vaccination & Therapy

\scriptsize

Vaccination helps!
... but does it?

* The *Antigenic Distance Hypothesis*: 
    - \scriptsize a **negative interference on vaccine efficiency
    from prior season's influenza vaccine?** [@Skowronski2017],
* The *Antigenic Sin*: first encountered virus has
life-long potentially negative influence on response to novel strains.
* Serial immunity, coevolution: immune system $\Leftrightarrow$ virus strains?
* A universal influenza vaccination is yet to be found.

@Francis2019

\vspace{.5cm}
\centering

The **dark matter of biology**:\newline
how many and which other viruses are around?

#  Immunity & Therapy

![](originalData/pinto20_fig2abc.png){width=75%}
![](originalData/pinto20_fig1f.png){width=22%}

\scriptsize
\vspace{1cm}

@Pinto2020: monoclonal antibody S309 from a SARS-CoV survivor\newline
binds to and neutralizes SARS-CoV-2 Spike protein

#  Immunity & Therapy

\scriptsize

* T-cell response to respiratory coronaviruses:
*Interestingly,  the  acute  phase  of  SARS  in humans is associated with a severe reduction in the number of T cells in the blood.* [@Channappanavar2014; @Liu2017]
* Chinese:
    - \scriptsize  oral liquid *Shuanghuanglian* and,
    - capsule *Lianhuaqingwen*  effective against viruses and bacteria [@Wang2020c]
    - *Maxingshigan-Yinqiaosan*  reduced time to fever resolution in patients with H1N1 [@Wang2011]
* Western: *remdesivir* & chloroquin  [@Wang20200204]
* antiviral effects of chloroquine:
    - \scriptsize inhibits SARS-CoV-2 *in vitro* [@Wang20200204]
    - effective influenza treatment in animal model [@Yan2013]
    - effective against HIV and influenza [Savarino2006]
* Ivermectin (FDA-approved for parasitic infections, "widely available") [@Caly2020] 
* Blood cholesterol protects against viruses and was low in Chinese patients [@Hu2020], see \url{https://www.bmj.com/content/368/bmj.m1182/rr-10}

# The Unknown.

## SARS-CoV-2 Genome Structure & Function

# Innate & Adaptive Immune Response to Viruses

![\tiny \url{www.youtube.com/watch?v=TWOzja74aW8}](originalData/reiter20_immunityslide.png)

# Stealth Viruses: Interferon Antagonism by RNA Viruses

![](originalData/nan19_fig1.png){height=90%}
![](originalData/nan19_fig2.png){height=40%}

\footnotesize

@Nan2014: interferon signaling and antagonism.

* Virus-infected cells give alarm (*please, kill me!*): interferons IFN
* Respiratory tract RNA viruses have diverse\newline
mechanisms of **IFN antagonism**
    - inhibitory binding
    - proteolytic degradation
    - inhibition of phosphorylation
    - de-ubiquitination


# Stealth Viruses: Interferon Antagonism by SARS-CoV

\footnotesize

![](originalData/baezsantos15_fig3.jpg){height=50%}

@BaezSantos2015 (Fig. 3)

* Coronaviruses use their *papain-like protease* (PLpro) in IFN antagonism,
* SARS-specific orf3a/b may contribute in the nucleus  [@Zhou2012]!


# SARS-CoV: 5' End - Replication & Processing

\footnotesize

![](originalData/baezsantos15_fig1.jpg){height=50%}

@BaezSantos2015 (Fig. 1)

* Coronaviruses use their *papain-like protease* (PLpro) in IFN antagonism,
* SARS-specific orf3a/b may contribute in the nucleus  [@Zhou2012]!


# SARS-CoV: 3' End - Docking & Specificity

\footnotesize

![](originalData/fehr15_fig1.jpg){width=75%}

@Fehr2015: Coronaviruses: an overview of their replication and
pathogenesis.

# SARS-CoV-2: Mutations in 3' End

\footnotesize

![](originalData/chan20_fig1.png){width=70%}

@Chan2020: genomic characterization of 2019-nCoV (now: SARS-CoV-2),
highly similar to SARS-CoV from 2003 outbreak in China & Canada.


# SARS-CoV-2: Genome Structure & Life Cycle

\footnotesize

* RNA+ virus: direct translation of genome,
* Papain-like protease: virus maturation & interferon antagonism
[@BaezSantos2015],
* Spike glycoproteins S1 & S2:
**cell docking to ACE2** receptor [@Walls2020],
* E protein: **cell entry via ACE2** receptor [@Schoeman2019]
    - also induces ER membrane curvature (check?),
* orf3a/b: nuclear IFN antagonism? [@Zhou2012].

@Chan2020: mutations in spike protein S1, orf8p and a \textbf{novel
protein sequence in orf3b}

\footnotesize

```
> orf3b novel insert, with alpha-helix prediction
MAYCWRCTSCCFSERFQNHNPQKEMATSTLQGCSLCLQLAVVVCNSLLTPFARCCWP
        hhhh hhhhhh   hhh hhhhhhhhhhhhhhhhhhhhh
```

# SARS-CoV-2: *Flatten the Curve*.

\scriptsize

* avoid cold & dry air
    - \scriptsize low T and low humidity are key factors in
    influenza transmission, and prelim. evidence
    (and common sense) suggests the same for SARS-CoV(-1)
    - use humidifiers, preferably cold evaporation (Venta)
    or with certified sterilization (eg. Philipps)
* wash hands & face regularly
    - \scriptsize offer more public facilities,
    - enforce at airports by indicator liquid?
* watch yourself: any/which symptoms?
    - \scriptsize stay at home (except perhaps mild rhinovirus:\newline
    it may protect against influenza!).
* watch RKI reports, beware the flu season
    - \scriptsize before/during flu season: avoid large groups,
    - be careful visiting the elderly & infants.
* personal non-expert advice:
    - \scriptsize drink lots of water (wash them down),
    - sterilize your throat, daily: gin tonic (chinin!) & whisky, 
    - breathe through nose (filter the air!),
    - wash your nose with physiological salt solution.

# SARS-CoV-2: *Flatten the Curve* & Fight Back.


\centering

**protect infants, the elderly and immuno-compromised,\newline let the
healthy population fight the virus(es)\newline by combined immuno-power\newline
until we find a vaccine**


\raggedbottom

# References{.allowframebreaks}
\tiny

