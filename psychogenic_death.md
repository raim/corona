
TODO: collect references on psychogenic death ("giving-up") and
potential impact on COVID19 death rates (hospitals, care homes).

# https://www.sciencedirect.com/science/article/abs/pii/S0306987718306145?via%3Dihub

‘Give-up-itis’ revisited: Neuropathology of extremis
Author links open overlay panel

# https://qz.com/1407287/giving-up-on-life-can-lead-to-actual-death-in-less-than-a-month/

Giving up on life can lead to actual death in less than a month


# https://bigthink.com/mind-brain/can-you-die-from-giving-up

"Psychogenic death is real," said Dr. Leach. "It isn't suicide, it isn't linked to depression, but the act of giving up on life and dying usually within days, is a very real condition often linked to severe trauma."

Dr. Leach identified five stages of progressing psychological decline leading to death:

Social withdrawal, Apathy, Aboulia, Psychic akinesia, Psychogenic death

# https://www.sciencedirect.com/science/article/abs/pii/S1888989120300343

Reactive psychoses in the context of the COVID-19 pandemic: Clinical perspectives from a case series

All of the episodes were directly triggered by stress derived from the COVID-19 pandemic and half of the patients presented severe suicidal behavior at admission.




# https://onlinelibrary.wiley.com/doi/full/10.1111/epi.16635

COVID‐19 outbreak: The impact of stress on seizures in patients with epilepsy

A minority of PWE experienced seizure exacerbation during the outbreak of COVID‐19. Stress, uncontrolled seizures, and inappropriate change in AED regimen were associated with increased seizures. Based on these findings, stress might be an independent precipitant for triggering seizures in some PWE.
