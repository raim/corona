---
title: "SARS-CoV-2 Literature & Data Collection"
author: "Rainer Machn&eacute;"
output: pdf_document
bibliography: corona.bib
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
---

# SARS-CoV-2 Pandemic 2020

Two alternative hypotheses are discussed by virologists,
epidemiologists and medical doctors in Germany:

* H1 (Team Drosten/Kekul&eacute;): a dangerous new virus strain causes a pandemic with high
mortality, requiring immediate and drastic measures, or
* H0 (Team Wodarg/Mölling): nothing special is happening, we are undergoing
through the normal seasonal rise in respiratory diseases, caused by
multiple viruses; we are just measuring one specific virus strain
involved this year, due to "observation bias" and a "social media or 
press epidemic" [@Moelling20200314].

Currently, the biggest unknown is the wide variation of SARS-CoV-2-associated
mortality rates between countries; specifically, much higher in Wuhan
and Italy (2.5-6%) and much lower in Germany (0.2%), and notably
intermediate but <1% on cruiseships where all passengers were tested, and
passengers tend to be older.


\newpage
## *Flatten the Curve*: Flu Season Self-Reported Cases & Total Mortality

![RKI Influenza Report 2018, Abb. 43](originalData/rki2018_abb43_small.png)
![RKI Grippeweb, 2010, KW 10](originalData/grippeweb_2020_10.png)

The Robert-Koch Institut, Berlin, provides yearly reports
on their influenza surveillance, including total mortality rates
from Berlin and Hessen. The RKI *grippeweb* is a German-wide
self-reporting system at https://grippeweb.rki.de/.

* @RKI2019, Abb. 43 shows peaks in 2016/2017/2018 winters and 2018 summer
in Berlin and Hessen, additional peak winter and summer 2015/2016 in
Hessen
* @RKI2018, Tabelle 3 shows that less than 50% of tested
cases during peak influenza season 2016/2017 were one of the four tested
influenza strains
* RKI mortality curves are part of a European monitoring system,
http://www.euromomo.eu and @Nielsen2019 analyzed specifically
the excess mortality in 2017/2018 season, and suggest that influenza B
impact should be reconsidered

### Summer Peaks?

* Increased summer peak after high winter peak:
    - Heat makes mice more susceptible to influenza A (or B?) [@Moriyama2019],
    - Decreased flu deaths in winter where followed by increased
death rates in summer [@Rocklov2009]


\newpage
## Multiple Viruses during Flu Season (in Glasgow)


![@Nickbakhsh2019: *Temporal patterns of viral respiratory
infections detected among patients in Glasgow, United Kingdom, 2005 to
2013*: *RV = rhinoviruses (A–C); IAV = influenza A virus (H1N1 and
H3N2); IBV = influenza B virus; RSV = respiratory syncytial virus; CoV
= human coronaviruses (229E, NL63, HKU1); AdV = human adenoviruses;
MPV = human metapneumovirus; PIV3 = parainfluenza 3 virus; PIV1 =
parainfluenza 1 virus; PIV4 = parainfluenza 4 virus; PIV2 =
parainfluenza 2 virus*](originalData/nickbakhsh19_fig1.jpg)

* @Nickbakhsh2019 report seasonal waves of 11 viruses, tested from 2005 to 2013 in Glasgow,
* -> cross-interaction between viruses & consequences for influenza vaccination?
(wogard, jefferson)
* @Kloepfer2020 report that influenza may inhibits rhinovirus infection,
* MPV infects infants and older adults,
* RSV infects infants, only mild symptoms in adults,
* PIV1 & PIV3: *human respiroviruses*, PIV2 & PIV4: *human rubulaviruses*, cause mumps,
* AdV: *human orthopneumoviruses*, from mild respiratory disease in chilrden, to multi-organ failure with weakened immune system.


\newpage

## SARS-CoV-2 Genome Structure & Phylogeny

![@BaezSantos2015 (Fig. 1) genome structure and protein processing of SARS-CoV.](originalData/baezsantos15_fig1.jpg)

* @Rota2003 first described a novel virus, now known as SARS-CoV and
@Drosten2003 suggested primers for clinical diagnosis
* @Brian2005 and @Fehr2015 review the genome structure of corona viruses, incl. SARS-CoV
* Interactive phylogenies at
    - https://nextstrain.org/groups/blab/sars-like-cov
    - https://www.gisaid.org/epiflu-applications/next-hcov-19-app/
* Shi Zhengli et al. found in 2005 that the SARS virus originated from bats
(https://en.wikipedia.org/wiki/Shi_Zhengli): @Li2005, @Menachery2015
* @Zhou20200203 (last author Shi Zhengli) published the first full report of
the new virus; a metagenomic analysis of  bronchoalveolar lavage fluid
(BALF) from patient ICU06 showed various
other viruses (Saccharomyces cerevisiae killer virus M1, Glypta
fumiferanae ichnovirus segments C9 & C10, Dulcamara mottle virus,
Proteus phage VB_PmiS-Isfahan, Hyposoter fugitivus ichnovirus)
* @Schoeman2019 review the "E" envelope protein's role and entry via the
ACE2 receptor
* @Walls2020 analyze differences in the spike glycoproteins S1 and S2 between
SARS-CoV and SARS-CoV-2
* @BaezSantos2015 review the papain-like protease and its multiple role in
viral protein maturation and interferon antagonism
* @Nan2014 review the diverse ways how RNA viruses antagonise interferon
signaling
* @Chan2020:
    - mutations in spike protein S1 and orf8p, 
    - unknown insert with 4 short alpha-helix predictions in orf3b,
    - orf3a/b: C-terminal NLS, interferon antagonist, induces both apoptosis
and necrosis.


```
> orf3b novel insert, with alpha-helix prediction
MAYCWRCTSCCFSERFQNHNPQKEMATSTLQGCSLCLQLAVVVCNSLLTPFARCCWP
        ---- ------   --- ---------------------
```

\newpage
## Coronavirus Life Cycle

![@BaezSantos2015 (Fig. 3) on the multiple activities of the papain-like protease, incl. inhibition of interferon signaling.](originalData/baezsantos15_fig3.jpg)

* @Brian2005 review replication mechanisms of corona viruses, incl. SARS
* @Walls2020 compare the spike glycoprotein structures of SARS-CoV and SARS-CoV-2
* @Schoeman2019 review current knowledge on the envelope protein E and
its multiple roles during the virus life cycle
* @BaezSantos2015 review the papain-like protease which processes
virus proteins but is also involved in interferon signaling antagonism
* @Nan2014 review the interferon response to viral infection and how RNA
viruses 

\newpage
## Current Literature

* latest curated literature information at NCBI: https://www.ncbi.nlm.nih.gov/research/coronavirus/


## SARS-CoV-2 Prevalence & Mortality

Case fatality rates given in brackets: **(dead/corona+/tested)**,
where the total tested number is rarely known. A reliable
count may come from cruiseship infections, where commonly all
passengers and personel were tested and tracked.

* @Vetter20200209 report 2% mortality in Hubei, but <=0.05% outside 
* @Heneghan20200311: 
    - China 3.9% (3136/80761/?)
    - China, >80y ~15%
    - Italy 6.2% (631/10149/>42000), average age 81y
    - South Korea 0.77% (58/7513/?), wider testing of milder cases
* RKI, 2020 03 15, 19:30 (https://www.rki.de/DE/Content/InfAZ/N/Neuartiges_Coronavirus/Fallzahlen.html)
    - Germany 0.2% (12/4838/?)
* The Local 2020 03 11 (https://www.thelocal.fr/20200311/coronavirus-death-rates-why-do-they-vary-so-much-between-countries):
    - France 1.8%-3% (>30/>1000/?)
    - Spain ~2.3% (47/>2000/?)
* Cruiseships:
    - Diamond Princess  1% (7/696/3618) [@wiki_princess_20200315]

TODO: review zhou20b.pdf and kucharski20.pdf

### Co-infection?

Sandeep Chakraborty noticed that several patient samples
had abnormally high numbers of Prevotella bacterium reads, and
even reads with both 16S rRNA and corona sequences,
https://pubpeer.com/publications/884D5017B8D48D5BA2466AC323F91B

### Low incidence of smokers and COPD patients in a Wuhan cohort

* @Zhang20200214 found only 1.4% (2 current and 7 past) smokers and
1.4% with COPD in 140 patients from Wuhan, while the prevalences in
China were 13.7% of people >=40y with COPD "in a recent study [22]",
and 27.3% of adults are current cigarette smokers "(data in 2018)
[35]"
* @Goldstein20200224 points out studies on elevated levels
of CD4+ cells in smokers
* But smokers may have a more severe progression of the disease [@Liu2020]
(p=0.018)

## PCR Test Accuracy

* @Zhuang20200305 report an 80% false-positive rate of a current
corona virus "active nucleic acid test"; but the article is in Chinese
and currently (2020 03 15) not available via the provided URL for
the English abstract at PubMed.

* "All four RT-PCR detection tests for Wuhan coronavirus will be +ve for SARS, leading to false positives – there is a much better 500bp biomarker" https://sanchakblog.wordpress.com/2020/02/02/all-four-rt-pcr-detection-tests-for-wuhan-coronavirus-will-be-ve-for-sars-leading-to-false-alarms-there-is-a-much-better-500bp-biomarker/

## Therapies & Patents

* @Wang20200204 reported that *remdesivir and chloroquine are
highly effective in the control of 2019-nCoV infection in vitro*

## Immunity & Vaccination

* @Skowronski2017 found the  "Antigenic Distance Hypothesis", that vaccination
against one strain weakens the effectiveness of vaccination
against a new strain, confirmed in a study of A(H3N2) epidemics in Canada,
2010-2011 to 2014-2015
* @Francis2019 review the sequential adaptation of the immune
system to evolving strains of influenza, incl. a short
review of the ADH


\newpage
## Understanding Mortality Bias?

Current mortality rates are derived without baseline by division of
diseased people X by total cases P tested positive. I.e. there is no
broad testing of the virus prevalence in the general population
(healthy+diseased). This can cause various biases, depending on the
testing policies in individual countries and hospitals. For example,
an Italian health expert mentioned [TG3 in RAI3, 20200311] general
post-mortem testing as a potential reason for high mortality, since
Corona+ people may have died from other causes.


* Actual case numbers
    - $N$ ... total population (dead+alive)
    - $X_{tot}$ ... total dead people
    - $X_c$ ... corona+ dead people, who died from corona (diseased)
    - $X_h$ ... corona+ dead people, who died from other causes ("healthy")
    - $X_0$ ... corona- dead people
    - $Y_c$ ... corona+ survivors, diseased
    - $Y_h$ ... corona+ survivors, healthy
    - $Y_0$ ... corona- survivors
* Reported (tested) case numbers
    - $R_l$ ... corona+ survivors
    - $R_x$ ... corona+ dead, who died from corona (diseased)
    - $R_h$ ... corona+ dead, who died from other causes
    - $R_0$ ... corona-, tested
* Sums
    - total population: $N = X_c + X_h + X_0 + Y_c + Y_h + Y_0$
    - total mortality: $M = (X_c + X_h + X_0)/N$
    - total corona cases: $N_c = X_c + X_h + Y_c + Y_h$ 
    - actual corona prevalence: $P = N_c/N$
    - actual mortality from corona: $M_c = X_c/(X_c + Y_c + Y_h)$
    - reported mortality from corona: $M_r = (R_x + R_h)/(R_x + R_h + R_l)$
    - reported prevalence: $P_r = (R_l + R_x + R_h)/R_tot$
    - total tested cases: $R_{tot} = R_l + R_x + R_h + R_0$


To understand the vast differences in reported mortality
rates (from 0.01% in Germany to >6% in Italy), we
need to understand how well the tested cases R
reflect the actual cases N_c and prevalence P.


\newpage
## The Classic SEIR Model

TODO: equations
interactive model with COVID-19
parameters https://alhill.shinyapps.io/COVID19seir/


\newpage
# References

