# Spitzwegerich

https://de.wikipedia.org/wiki/Spitzwegerich#Wichtige_Pflanzeninhaltsstoffe_und_medizinische_Wirkung

Der Spitzwegerich (Plantago lanceolata), auch Spießkraut, Lungenblattl oder Schlangenzunge genannt, ist eine Pflanzenart, die zur Familie der Wegerichgewächse (Plantaginaceae) gehört.

Der Spitzwegerich enthält Iridoidglycoside wie Aucubin, Catalpol, Asperulosid, Schleimstoffe, Gerbstoffe, Kieselsäure, Saponin. Er ist reizmildernd und leicht hustenlösend.[9] Er wird gegen Katarrhe der Luftwege und entzündliche Veränderungen der Mund- und Rachenschleimhaut eingesetzt.

Die Wirksamkeit der Droge ist hier sowohl auf die einhüllende Wirkung der Schleimstoffe als auch auf die adstringierende Wirkung der Gerbstoffe sowie auf die antibakterielle und damit entzündungshemmende Wirkung der Abbauprodukte der Iridoide (Aucubigenin entsteht durch Hydrolyse mittels Beta-Glucosidasen aus Aucubin) zurückzuführen.

# Thymian

e Thymiane (Thymus, von altgriechisch θύμος thýmos für „Mut“, „Kraft“, „Gemüt“) oder Quendel[1] sind eine Pflanzengattung innerhalb der Familie der Lippenblütengewächse (Lamiaceae). 

Der wirksamkeitsbestimmende Inhaltsstoff des Echten Thymians ist das ätherische Öl (1,0–2,5 %). Dieses besteht vorwiegend aus den Monoterpenen Thymol (25–50 %) und Carvacrol (3–10 %) sowie p-Cymen, Borneol und Linalool. Das ätherische Öl hat eine sekretolytische, sekretomotorische und bronchospasmolytische Wirkung. Darüber hinaus ergibt sich aufgrund des Thymols und Carvacrols über eine Hemmung der Cyclooxygenase ein entzündungshemmender Effekt.[8]

# Eibischwurzel

Der Echte Eibisch (Althaea officinalis), auch Arznei-Eibisch genannt, gehört zur Familie der Malvengewächse (Malvaceae). 

Die Schleimstoffe wirken einhüllend, reizmildernd und lindernd[6]; im Tierversuch konnten auch entzündungshemmende und immunstabilisierende Wirkungen nachgewiesen werden. 

Inhaltsstoffe des Echten Eibisch sind Schleimstoffe (in den Wurzeln bis zu 25 %) besonders mit Galacturonorhamnanen und Arabinogalactanen; Pektine und in der Wurzel auch Stärke. 

# Zimt

Eugenol

# Gewuerznelken - Syzygium aromaticum

Eugenol

# Knoblauch -  Allium sativum I

 Allicin ist Ausgangsstoff für mehrere andere schwefelhaltige Verbindungen, die insbesondere beim Erhitzen von Knoblauch entstehen. Dazu gehören Diallyldisulfid, Diallylthiosulfonat und vor allem auch Ajoen, das die Eigenschaft hat, die Aggregation von Thrombozyten zu verhindern, und somit antithrombotisch wirkt.[5]

# Islaendisch Moos

Cetraria islandica – auch Isländisches Moos, Islandmoos, Irisches Moos (nicht zu verwechseln mit Irisch Moos), Lichen Islandicus, Blutlungenmoos, Fiebermoos, Hirschhornflechte oder Graupen (österreichisch) – ist eine polsterförmig wachsende Strauchflechte. 

Die erste bekannte Beschreibung findet sich unter der Bezeichnung Muscus islandicus catharticus in einem Arzneimittelverzeichnis, der Kopenhagener Taxe von 1672.[1] In der Phytotherapie findet der ganze oder zerkleinerte Thallus Verwendung (Lichen islandicus).[2] Die Droge enthält Schleimstoffe und bitter schmeckende Flechtensäuren.[2] Der Ausschuss für pflanzliche Arzneimittel der EU hat im November 2014 Lichen islandicus als traditionelles pflanzliches Arzneimittel eingestuft. Die zugelassenen Anwendungsgebiete umfassen die Linderung von trockenem Husten und Entzündungen im Mund- und Rachenraum sowie temporäre Appetitlosigkeit.[3] Als Hustentee wird Lichen islandicus pur oder gemischt eingesetzt.[4]

Als therapeutisch wirksame Bestandteile enthält Isländisches Moos Bitterstoffe, Flechtensäuren (z. B. Fumarprotocetrarsäure[5]), Iod, Schleimstoffe (Lichenin) und die Vitamine A, B1 und B12.[6] Es wirkt reizlindernd und stärkend auf die Schleimhäute im Mund und Rachen,[6] auch bei Entzündungen der Magen- und Darmschleimhaut wird es verwendet. Weiter wirkt es gegen Brechreiz, ist appetitsteigernd, belebend und kräftigend (tonisierend). Den Flechtensäuren wird leicht antibakterielle Wirkung nachgesagt.[6][7]

# Zataria multiflora: 

lokal, vom Iran bis zum westlichen Himalaja

# Glycyrrhiza glabra: Suessholzwurzel

-> many references on direct antiviral activity of glycyrrhizin

# 

