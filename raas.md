# @Young2020: The renin-angiotensin aldosterone system (RAAS)

* classic endocrine negative feedback loop for the physiological
  control of extracellular fluid volume and blood pressure
* one kidney 1 clip model of hypertension developed by Goldblatt
  demonstrated the fundamental importance of blood flow to the kidney
  for the control of blood pressure homeostasis and further supported
  the concept that factors released from the kidney counter inadequate
  tissue perfusion (Goldblatt et al. 1934).
* renin -> AngI -ACE-> AngII -> alodosterone (electrocortin),
* local tissue RAAS in many peripheral organs, which is proposed to
  have autocrine, paracrine, and intracrine effects that differ from
  the those of the circulating RAAS (Danser 1996, Leung 2004). 
* a vasodilator arm to the RAAS has also been identified that opposes
  the pressor and remodelling actions of ANG II. The predominant
  mediator in the **‘protective’ arm of the RAAS is ACE2**,
* the incidence of severe, and often fatal, COVID-19 complications in
  response to infection is greater for men and for older age groups,
  especially those with pre-existing cardiovascular disease (Reynolds
  et al. 2020). Obesity and ethnicity are other significant risk
  factors for poor outcome. Obesity increases the risk of severe
  COVID-19 by two- to three-fold, and patients of an African, Asian
  and minority ethnicity are also more severely
  affected. Interestingly, although there has been extremely low
  COVID-19 mortality in children, a rare complication of COVID-19 has
  emerged in children who has clinical features similar to Kawasaki
  disease (Verdoni et al. 2020). This pro-inflammatory condition
  mainly affects the kidney and may be related to the
  hyperinflammatory state induced in severe COVID-19 in adults.
* RAAS:
    1. Loss of blood volume and poor perfusion of the kidneys is
    detected by the juxtaglomerular apparatus, which responds by
    releasing renin.
	2. In the circulation, renin cleaves the precursor polypeptide
       angiotensinogen (produced in the liver) to the decapeptide ANG
       I (the N-terminal 10 amino acids of angiotensinogen). ACE is
       expressed predominantly in epithelial cells and performs the
       next step to remove a dipeptide from the C-terminus to produce
       the octapeptide ANG II (Coates 2003).
	3. ANG II binds to angiotensin receptors type 1 and 2 (AT1R and
       AT2R, respectively) located in the zona glomerulosa of the
       adrenal to increase the production of the steroid hormone
       aldosterone. Aldosterone synthesis and release is also
       regulated by ACTH and serum potassium levels and together with
       ANG II, aldosterone serves as a critical physiological
       regulator of sodium resorption and hence fluid levels via
       actions at the epithelial MR
* ACE gene encodes two isoforms; one is expressed broadly in
  epithelial cells, capillaries of the lung, kidney and several other
  tissues, while a truncated germinal form is expressed only in sperm
* ACE is also part of the kinin-kallikrein system (where it is
  referred to as kinase II) and degrades the vasodilator bradykinin,
  and other vasoactive peptides. Other roles include its glycosidase
  activity which releases glycosylphosphatidylinositol (GPI)-anchored
  proteins from the plasma membrane (Zhao et al. 2015). Of note, ACE
  and its isozyme ACE2 are the products of distinct genes located on
  chromosome 17 and the X chromosome, respectively.
* many tissues express a local ‘RAAS’ comprising angiotensinogen, the
  (pro)renin receptor and renin-independent mechanisms of ANG peptide
  generation from ANG(1–12). Members of the ‘depressor’ arm of
  ACE2/ANG(1–7)/MAS receptor (discussed further subsequently) and ANG
  IV/insulin-regulated aminopeptidase pathways (IRAP) have also been
  identified in a wide range of tissues, including lung, heart and
  vasculature, CNS, kidney and macrophages among others (Azushima et
  al. 2020). Although this remains an active area of research,
  therapeutic regulation of local RAAS components has offered new
  options for cardiovascular and other diseases (Danser 2003, Pugliese
  et al. 2020).
* ACE2: the ‘alternate RAAS’
* As an aside, it is intriguing that ACE2 has recently been identified
  as the most highly upregulated gene in hearts of patients who died
  with hypertrophic cardiomyopathy (Bos et al. 2020). Although first
  characterised in heart, kidney and testis – sites where ACE has
  established functions – multiple and diverse roles for ACE2 have now
  been described across many tissues, including the lung, gut and
  inflammatory cells.
* Its role as the catalytic enzyme that produces ANG(1–7) and
  ANG(1–9), that in turn activate the MAS receptor, are now the focus
  of efforts to control cardiovascular disease via novel
  pharmacological avenues
* MAS receptor, also known as MAS1 oncogene, is a G-protein coupled
  receptor, which opposes the negative effects of the RAAS when
  activated by ANG(1–7), that is, inappropriate ANG II, AT1R and MR
  activation
* blood vessels of human and several other species generate
  ANG(1–7). The MAS receptor is expressed in both the endothelial and
  vascular smooth muscle cells (VSMCs) and is thus a target for newer
  pharmacotherapies (Povlsen et al. 2020). The ACE2/ANG(1–7)/ANG(1–9)
  pathway is recognised as critical and protective in the pathogenesis
  of heart failure with reduced or preserved ejection fraction in
  association with myocardial infarction, hypertension, lung disease
  and diabetes,
* SARS-CoV-2 differs from SARS-CoV by a number of defined amino acids
  which increase affinity of the new virus for the ACE2 receptor (Lan
  et al. 2020). The expression of ACE2 on type 2 pneumocytes provides
  SARS-CoV-2 with an infection route deep within airways and is likely
  critical in the development of severe COVID-19. ACE2 is also
  expressed on macrophages, perivascular pericytes, endothelial cells
  and cardiomyocytes providing a plausible link between immunity,
  inflammation, ACE2 and cardiovascular disease and use of the ACE2
  cellular internalisation machinery to enter the cell.
* binding of SARS-CoV-2 to ACE2 is primed by protease cleavage of
  spike proteins. This is catalysed by the type II transmembrane
  serine protease (TMPRSS2). A number of other proteases also perform
  this step and are co-expressed with ACE2 in some tissues; furin
  protease TMPRSS, cathepsin L and human airway trypsin-like protease
  (HAT) all facilitate viral uptake into host cell endosomes (Hoffmann
  et al. 2020).
* Once the SARS-CoV-2 bound ACE2 is internalised by the cell, ACE2 is
  markedly downregulated. This potentially removes the critical
  protective pathway of the RAAS, allowing unrestrained activity of
  the tissue renin/ACE/ANG II pathway and promotion of ‘macrophage
  activation syndrome’ and the multiorgan hyperinflammatory response
  (cytokine storm) (Banu et al. 2020).
* In women, oestrogen (E2) signalling associates with higher levels of
  ACE2, possibly relevant to the sex difference in the development of
  severe COVID-19. Enhanced activity of the ACE2 pathway most likely
  contributes to the reduced incidence of cardiovascular disease in
  premenopausal women.
* Wuhan early in the pandemic established cardiovascular disease, and
  hypertension in particular, as important risk factors for developing
  severe forms of COVID-19. Obesity and diabetes have now emerged as
  other prominent risk factors. Many patients with cardiovascular
  disease and/or hypertension are prescribed ACEi, ARB and MRA
  therapies to control their blood pressure and to protect the heart
  and kidneys from adverse remodelling processes. Because these drugs
  taken alone or in combination can elevate ACE2 expression, concerns
  were expressed that these important anti-hypertensive drugs could
  increase susceptibility to viral infection and/or viral load (the
  number of viral particles) in people taking them.
* early data from a large UK population study showed a higher
  incidence of self-reported COVID-19 symptoms in ACEi users. However,
  there was no increase in COVID-19 symptoms in a subset of patients
  with confirmed viral infection,
* To date, evidence does not support increased susceptibility of
  hypertensive patients or of patients with heart disease to
  SARS-CoV-2 infection. However, older patients, and more often males,
  with these conditions are more likely to develop severe symptoms and
  die. Very recent reports suggest that those patients who continue
  taking ACEi/ARB medication have a lower risk of all-cause
  mortality. As yet, there is no evidence that they alter the severity
  of COVID-19 disease as noted in the WHO briefing on 7 May 2020.
*  However, the increase in ACE2 expression as a result of ACEi or ARB
   therapy is likely to be beneficial in countering the
   SARS-CoV-2-mediated reduction in ACE2 expression, thereby reducing
   ALI/ARDS. Thus, the potential detrimental consequences of
   discontinuing RAAS blocking therapies upon pre-existing disease may
   be substantially greater than any modest effects on susceptibility
   to infection (Guzik et al. 2020). 
* Troponin levels are elevated in >10% of patients hospitalized for
  COVID-19, indicating heart failure and myocardial damage; levels are
  higher in critically ill patients and in those with pre-existing
  heart disease (Tersalvi et al. 2020). Enhanced myocardial injury may
  result from the interaction between pathways associated with severe
  infections and amplified inflammatory response (Tersalvi et
  al. 2020). SARS-CoV-2 infection downregulates ACE2 in the lung; a
  similar mechanism in cardiomyocytes may enable the deleterious
  hyperinflammatory response of COVID-19, leading to cardiomyocyte
  death (Tersalvi et al. 2020). The role of ACE2 regulation in these
  disease processes induced by the novel coronavirus is the subject of
  current investigation.
* In summary then, the current consensus is that the presence of
  hypertension and other cardiovascular diseases do not increase the
  risk of infection by SARS-CoV-2, but patients with pre-existing
  heart and vascular disease, including hypertension and diabetes, are
  at increased risk of a severe course of disease in the event of
  COVID-19 pneumonia and death (Pranata et al. 2020). It should also
  be noted that hypotension is a common feature of critical illness
  and septic shock (Alexandre et al. 2020, Mancia et al. 2020,
  Reynolds et al. 2020); at any point during infection and subsequent
  disease course, this hypotension may be exacerbated by
  antihypertensive medications by virtue of their established
  mechanisms of action (Park et al. 2020). This may be mediated by a
  shift in the ACE/ACE2 balance following downregulation of ACE2
  tissue activity by COVID-19, resulting in unopposed ACE. This, in
  turn, can induce aldosterone synthesis and subsequently
  hypokalaemia. MRA, as potassium-sparing diuretics, may have value
  for correcting this hypokalaemia (Alexandre et al. 2020).
* TODO: continue ...
