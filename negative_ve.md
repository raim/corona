

<!-- pandoc negative_ve.md --pdf-engine=xelatex -o negative_ve.pdf -->


* AB: antibody
* VE: vaccine efficieny
* VE-I: VE against infection
* VE-H: VE against hospitalization
* VE-D: VE against death

## OQ, Ideas and Speculations

* Negative VE-I at still strong VE-H: this may reflect dose-dependent 
antibody-mediated enhancement as a function of the immune system: 
enhance infection to learn about the new strain;

![](ade_as_a_function_v3.jpeg)

### TODO

* digitize VE plots

## notes for ...

* Data unclear, UKSHA offers various scenarios, and extremely challenging 
to handle confounding factors, i.e. to distinguish causal from correlational
relations.

* Too much data: it's a mess, and will likely occupy several academic
generationw, meanwhile "pubmed conspiracies",

* Vaccine passports, mandatory vaccination vs outlook on repeated vaccination 
against current strains,




# Negative VE & Waning

* @Subramanian2021: *Increases in COVID‐19 are unrelated to levels of
  vaccination across 68 countries and 2947 counties in the United
  States*
* @Koren2021pre: Israel, airport study, in August, month of booster campagne,
  incidence was 2x higher in vaccinated,
* @Nordstroem2021pre: Sweden, Tab. 2: -19% VE (-97 -- 28) for AZ, >120 d 
  after AZ vacc, Fig.2: negative over all vaccince from > 240 d after
  vacc.; notably this data is not shown in @Nordstroem2021,
* @Helmsdal2021pre: faroer insel, healthcare worker party, all
  vaccinated and boostered, 21 of 33 had Covid-19,
* @Goldberg2021pre: Fig.3, compare infection rates recovered vs. vaccinated,
  and vacc->rec vs. rec->vacc,
* @Buchan2022pre: Ontario, CA; strongly negative VE for omicron,
  getting less negative over time, i.e., as antibodies wane,
    - study was updated later after public reports, see https://www.cbc.ca/news/health/covid-19-vaccine-study-omicron-anti-vaxxers-1.6315890
* @Hansen2021pre: Denmark, neg. VE for both Pfizer and Moderna,
  91-150 d after vacc.,
* @Sheikh2021pre: Tab.4: strongly neg. VE in some groups, TODO:
  check group definitions,
* UK weekly vaccine reports, 2021-2022,
* TODO: RKI report end of 2021, 
* @Eggink2021pre: TODO - increased risk only against Delta or also
  against unvacc.: "we observed an increased risk of S-gene target
  failure, predictive of the Omicron variant, in fully vaccinated
  (odds ratio: 5.0; 95% confidence interval: 4.0-6.1) and previously
  infected individuals (OR: 4.9: 95% CI: 3.1-7.7) compared with
  infected naïve individuals.",
* @Chaguzza2022pre: TODO - "We found that the positivity rate among
  unvaccinated persons was higher for Delta (5.2%) than Omicron
  (4.5%). We found similar results in persons who received a single
  vaccine dose. Conversely, our results show that Omicron had higher
  positivity rates than Delta among those who received two doses
  within five months (Omicron = 4.7% vs. Delta = 2.6%), two doses more
  than five months ago (4.2% vs. 2.9%), and three vaccine doses (2.2%
  vs. 0.9%)."
* @Brandal2021: TODO - outbreak among vaccinated at x-mas party, "Most
  of the cases (n = 79; 98%) and non-cases (n = 27; 93%) were fully
  vaccinated with a median time since receiving the last vaccine dose
  of 79 days for cases and 87 days for non-cases (no statistically
  significant difference, Wilcoxon rank-sum p = 0.48)."
* TODO: older reports on negative VE for flu vaccine (or negative
  effects of flu vaccine against OTHER respiratory viruses collected
  here: https://alethonews.com/2021/11/15/negative-vaccine-effectiveness-isnt-a-new-phenomenon-it-turned-up-in-the-swine-flu-vaccine/


# Repated Vaccinations

* @Horndler2021pre: SARS-CoV-2, reduced breath after repeated vacc.
* @Ramsay2019: "Although VE was lower against H3N2 and B for
  individuals vaccinated in both seasons compared to those vaccinated
  in the current season only, it should be noted that past vaccination
  history cannot be altered and this comparison disregards
  susceptibility to influenza during the prior season among those
  vaccinated in the current season only. "
* @Okoli2021: "The evidence suggests that influenza VE declines with
  vaccination PM (program maturation)."

# Negative VE After Vaccination

* @Chemaitelly2021: Qatar, Fig.2: negative VE-I, -4.9% in the first
  two weeks, TODO: differences to preprint?,
* @Emborg2021pre: Denmark, Tab.2-2: negative VE-I in several tested
  cohorts, in the frist two weeks,
* @Gram2021: Denmark, TODO: is this the published version of
  @Emborg2021pre?, Fig. 3: 1 dose AZ, 0% VE with symmetric error bars
  0-13 d after vacc., Tab.2: negative VE after 1 dose AZ, 0-13 d and
  again all > 84 d,
* @Thompson2021: Fig3: negative VE-H (!) 0-13 d after vacc.,

## ADE, ADI, VAERD

Note: the following studies do not necessarily refer to
vaccination-induced AB, mostly infection-induced.

* @Liu2021b: non-canoncial ADE, where AB directly facilitates
  Spike:ADE2 interaction
* @Tseng2012: vaccine candidate against SARS-CoV-1 lead to pulmonary
  immunopathology upon infection, *Caution in proceeding to
  application of a SARS-CoV vaccine in humans is indicated.*
* @Hoepel2021: ADI, *High titers and low fucosylation of early human
  anti-SARS-CoV-2 IgG promote inflammation by alveolar macrophages*
* @Wang2021d: *ACE2 can act as the secondary receptor in the
  FcγR-dependent ADE of SARS-CoV-2 infection*
* @Khurana2013: *Vaccine-Induced Anti-HA2 Antibodies Promote Virus
  Fusion and Enhance Influenza Virus Respiratory Disease*, "The
  authors vaccinated pigs with whole inactivated H1N2 influenza virus"
* ADE and SARS-CoV-2 Review: @Lee2020,
* Wu2020c_pre: ADE in recovered patients (NOTE: cited by Lauterbach,
  Oct. 2020 on facebook),

## Vaccination Risks

* @Sharff2021pre: The true incidence of myopericarditis is markedly
  higher than the incidence reported to US advisory committees. The
  VSD should validate its search algorithm to improve its sensitivity
  for myopericarditis.
* @ChanChung2021: Eosinophilic granulomatosis with polyangiitis after
  COVID-19 vaccination
* @Patone2021: *Risks of myocarditis, pericarditis, and cardiac
  arrhythmias associated with COVID-19 vaccination or SARS-CoV-2
  infection*, *Subgroup analyses by age showed the increased risk of
  myocarditis associated with the two mRNA vaccines was present only
  in those younger than 40.*
